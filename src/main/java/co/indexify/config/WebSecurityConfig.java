package co.indexify.config;

import co.indexify.controller.rest.*;
import co.indexify.controller.rest.advice.ReverseFavouriteProductController;
import co.indexify.cors.CustomCorsFilter;
import co.indexify.domain.model.Organization;
import co.indexify.domain.model.Privilege;
import co.indexify.queryparam.ExtraQueryParamFilter;
import co.indexify.security.SecurityContextExtendedPersistenceFilter;
import co.indexify.security.ajax.AjaxUsernameProcessingFilter;
import co.indexify.security.RestAccessDeniedHandler;
import co.indexify.security.RestAuthenticationEntryPoint;
import co.indexify.security.SkipPathMethodRequestMatcher;
import co.indexify.security.ajax.*;
import co.indexify.security.anonymous.ExtendedAnonymousAuthenticationFilter;
import co.indexify.security.jwt.JwtAuthenticationProvider;
import co.indexify.security.jwt.JwtTokenAuthenticationProcessingFilter;
import co.indexify.service.IUserService;
import co.indexify.token.jwt.extractor.TokenExtractor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;
import org.springframework.security.web.savedrequest.NullRequestCache;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;


@Configuration
//@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    public static final String AUTHENTICATION_HEADER_NAME = "Authorization";
    public static final String AUTHENTICATION_URL = "/api/auth/login";
    public static final String REFRESH_TOKEN_URL = "/api/auth/token";
    public static final String AUTHENTICATION_USERNAME_URL = "/api/auth/username";


    public static final String AUTHENTICATION_SOCIAL_URL = "/api/auth/social";


    public static final String API_ROOT_URL = "/api/**";
    public static final String PATH_ROOT = "/";
    public static final String PATH_INDEX = "/index";


    @Autowired
    private RestAuthenticationEntryPoint authenticationEntryPoint;
    @Autowired
    private AjaxAwareAuthenticationSuccessHandler ajaxSuccessHandler;
    @Autowired
    private AjaxAwareAuthenticationFailureHandler ajaxFailureHandler;
    @Autowired
    private AjaxAuthenticationProvider ajaxAuthenticationProvider;
    @Autowired
    private JwtAuthenticationProvider jwtAuthenticationProvider;

    @Autowired
    private AjaxUsernameAuthenticationSuccessHandler ajaxUsernameSuccessHandler;

    @Autowired
    private AjaxUsernameAuthenticationProvider ajaxUsernameAuthenticationProvider;

    @Autowired
    private TokenExtractor tokenExtractor;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private IUserService userService;

    @Autowired
    private RestAccessDeniedHandler accessDeniedHandler;

    @Autowired
    private AuthenticationDetailsSource<HttpServletRequest, ?> authenticationDetailsSource;


    protected AjaxLoginProcessingFilter buildAjaxLoginProcessingFilter(String loginEntryPoint) throws Exception {
        AjaxLoginProcessingFilter filter = new AjaxLoginProcessingFilter(loginEntryPoint, ajaxSuccessHandler, ajaxFailureHandler, objectMapper);
        filter.setAuthenticationManager(this.authenticationManager);
        filter.setAuthenticationDetailsSource(authenticationDetailsSource);
        return filter;
    }



    protected AjaxSocialProcessingFilter buildAjaxSocialProcessingFilter(String loginEntryPoint) throws Exception {
        AjaxSocialProcessingFilter filter = new AjaxSocialProcessingFilter(loginEntryPoint, ajaxSuccessHandler, ajaxFailureHandler, objectMapper);
        filter.setAuthenticationManager(this.authenticationManager);
        filter.setAuthenticationDetailsSource(authenticationDetailsSource);
        return filter;
    }





    protected JwtTokenAuthenticationProcessingFilter buildExtendedJwtTokenAuthenticationProcessingFilter(List<RequestMatcher> pathsToSkip, String pattern) throws Exception {
        SkipPathMethodRequestMatcher matcher = new SkipPathMethodRequestMatcher(pathsToSkip, pattern);
        JwtTokenAuthenticationProcessingFilter filter
                = new JwtTokenAuthenticationProcessingFilter(ajaxFailureHandler, tokenExtractor, matcher);
        filter.setAuthenticationManager(this.authenticationManager);
        filter.setAuthenticationDetailsSource(authenticationDetailsSource);
        return filter;
    }


    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(ajaxUsernameAuthenticationProvider);
       // auth.authenticationProvider(ajaxAuthenticationProvider);
        auth.authenticationProvider(ajaxSocialAuthenticationProvider);
        auth.authenticationProvider(jwtAuthenticationProvider);
    }


    @Autowired
    private AjaxSocialAuthenticationProvider ajaxSocialAuthenticationProvider;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        List<String> permitAllEndpointList = Arrays.asList(
                AUTHENTICATION_URL,
                REFRESH_TOKEN_URL,
                RegistrationController.PATH_USER_REGISTRATION,
                RegistrationController.PATH_USER_REGISTRATION_CONFIRM,
                RegistrationController.PATH_USER_REGISTRATION_EMAIL_RESEND,
                RegistrationController.PATH_FORGET_PASSWORD,
                RegistrationController.PATH_CHANGE_PASSWORD,
                RepositoryRestMvcConfig.PATH_COUNTRIES_GET
        );

        RequestMatcher[] permitAllEndpointRequestMatchers = {
                new AntPathRequestMatcher(AUTHENTICATION_URL, HttpMethod.POST.name()),
                new AntPathRequestMatcher(REFRESH_TOKEN_URL, HttpMethod.GET.name()),
                new AntPathRequestMatcher(RegistrationController.PATH_USER_REGISTRATION, HttpMethod.POST.name()),
                new AntPathRequestMatcher(RegistrationController.PATH_USER_REGISTRATION_CONFIRM, HttpMethod.GET.name()),
                new AntPathRequestMatcher(RegistrationController.PATH_USER_REGISTRATION_EMAIL_RESEND, HttpMethod.GET.name()),
                new AntPathRequestMatcher(RegistrationController.PATH_FORGET_PASSWORD, HttpMethod.GET.name()),
                new AntPathRequestMatcher(RegistrationController.PATH_CHANGE_PASSWORD, HttpMethod.GET.name()),
                new AntPathRequestMatcher(RepositoryRestMvcConfig.PATH_COUNTRIES_GET, HttpMethod.GET.name()),
                new AntPathRequestMatcher(PATH_INDEX, HttpMethod.GET.name()),
                new AntPathRequestMatcher(PATH_ROOT, HttpMethod.GET.name()),
                new AntPathRequestMatcher(AUTHENTICATION_USERNAME_URL, HttpMethod.POST.name()),
                new AntPathRequestMatcher(AUTHENTICATION_SOCIAL_URL, HttpMethod.POST.name()),

        };

        http
                .csrf().disable() // We don't need CSRF for JWT based authentication
                .exceptionHandling()
                .authenticationEntryPoint(this.authenticationEntryPoint)
                .accessDeniedHandler(accessDeniedHandler)
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

                .and()
                .anonymous().authenticationFilter(buildAnonymousAuthenticationFilter())
                .and()
                .requestCache()
                .requestCache(new NullRequestCache())
                .and()
                .antMatcher(API_ROOT_URL)
                .authorizeRequests()
                .requestMatchers(permitAllEndpointRequestMatchers).permitAll()
                .antMatchers(MeController.PATH_USER_ME).hasAuthority(Privilege.PRIV_USER_ME_GET)
                .antMatchers(HttpMethod.POST, RegistrationController.PATH_SAVE_PASSWORD).hasAuthority(Privilege.PRIV_USER_PASSWORD_RESET)
                .antMatchers(HttpMethod.POST, OrganizationController.PATH_ORGANIZATION_CREATE).hasAuthority(Privilege.PRIV_ORG_CREATE)
                .antMatchers(HttpMethod.POST, NodeController.PATH_CREATE_TREE).hasAuthority(Privilege.PRIV_NODE_TREE_CREATE)
                .antMatchers(HttpMethod.POST, ProductController.PATH_PRODUCT_CREATE).hasAuthority(Privilege.PRIV_PRODUCT_CREATE)
                .antMatchers(HttpMethod.PUT, ProductController.PATH_PRODUCT_UPDATE_SECURITY).hasAuthority(Privilege.PRIV_PRODUCT_UPDATE)
                .antMatchers(HttpMethod.POST, ProductController.PATH_PRODUCT_NODE_LINK).hasAuthority(Privilege.PRIV_PRODUCT_NODE_LINK)
                .antMatchers(HttpMethod.POST, ResourceController.PATH_RESOURSE_CREATE).hasAuthority(Privilege.PRIV_RESOURCE_CREATE)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_NODES_GET).hasAuthority(Privilege.PRIV_NODE_TREE_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_PRODUCTS_GET).hasAuthority(Privilege.PRIV_PRODUCTS_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_LEAF_CHILD_PRODUCTS_GET).hasAuthority(Privilege.PRIV_LEAF_CHILD_PRODUCTS_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_PRODUCTS_SEARCH_BY_NAME_GET).hasAuthority(Privilege.PRIV_PRODUCTS_SEARCH_BY_NAME_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_ROOT_NODES_GET).hasAuthority(Privilege.PRIV_ROOT_NODES_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_NODE_CHILD_NODES_GET).hasAuthority(Privilege.PRIV_NODE_CHILD_NODES_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_LEAF_NODES_GET).hasAuthority(Privilege.PRIV_LEAF_NODES_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_PRODUCTS_NOT_LINKED).hasAuthority(Privilege.PRIV_PRODUCTS_NOT_LINKED_GET)
                .antMatchers(HttpMethod.POST, UserScanController.PATH_USER_SCAN_CREATE).hasAuthority(Privilege.PRIV_USER_SCAN_CREATE)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_USER_SCANS_GET).hasAuthority(Privilege.PRIV_USER_SCANS_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_ORGANIZATION_GET).hasAuthority(Privilege.PRIV_ORGANIZATION_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_ORGANIZATION_SCANS_GET).hasAuthority(Privilege.PRIV_ORGANIZATION_SCANS_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_USER_GET).hasAuthority(Privilege.PRIV_USER_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_USER_SCANS_ORDER_BY_CREATED_GET).hasAuthority(Privilege.PRIV_USER_SCANS_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_ORGANIZATION_SCANS_ORDER_BY_CREATED_GET).hasAuthority(Privilege.PRIV_ORGANIZATION_SCANS_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_ORGANIZATION_SCANS_SEARCH_BY_NAME_GET).hasAuthority(Privilege.PRIV_ORGANIZATION_SCANS_SEARCH_BY_NAME_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_USER_SCANS_SEARCH_BY_NAME_GET).hasAuthority(Privilege.PRIV_USER_SCANS_SEARCH_BY_NAME_GET)
                .antMatchers(HttpMethod.POST, FavouriteProductController.PATH_FAVOURITE_PRODUCT_CREATE).hasAuthority(Privilege.PRIV_FAVOURITE_PRODUCT_CREATE)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_FAVOURITE_PRODUCTS_GET).hasAuthority(Privilege.PRIV_FAVOURITE_PRODUCTS_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_FAVOURITE_PRODUCTS_BY_ORGANIZATION_GET).hasAuthority(Privilege.PRIV_FAVOURITE_PRODUCTS_BY_ORGANIZATION_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_PRODUCT_GET).hasAuthority(Privilege.PRIV_PRODUCT_GET)
                .antMatchers(HttpMethod.GET, FavouriteProductController.PATH_FAVOURITE_PRODUCT_ORGANIZATIONS_ID_NAME_GET).hasAuthority(Privilege.PRIV_FAVOURITE_PRODUCT_ORGANIZATIONS_ID_NAME_GET)
                .antMatchers(HttpMethod.DELETE, FavouriteProductController.PATH_FAVOURITE_PRODUCT_DELETE_SECURITY).hasAuthority(Privilege.PRIV_FAVOURITE_PRODUCT_DELETE)
                .antMatchers(HttpMethod.DELETE, UserScanController.PATH_USER_SCAN_DELETE_SECURITY).hasAuthority(Privilege.PRIV_USER_SCAN_DELETE)
                .antMatchers(HttpMethod.DELETE, OrganizationScanController.PATH_ORGANIZATION_SCAN_DELETE_SECURITY).hasAuthority(Privilege.PRIV_ORGANIZATION_SCAN_DELETE)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_USERS_BY_TAG_GET).hasAuthority(Privilege.PRIV_USERS_BY_TAG_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_ORGANIZATIONS_BY_TAG_GET).hasAuthority(Privilege.PRIV_ORGANIZATIONS_BY_TAG_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_USER_VISITORS_GET).hasAuthority(Privilege.PRIV_USER_VISITORS_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_USER_VISITORS_ORDER_BY_CREATED_GET).hasAuthority(Privilege.PRIV_USER_VISITORS_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_USER_VISITORS_SEARCH_BY_NAME_GET).hasAuthority(Privilege.PRIV_USER_VISITORS_SEARCH_BY_NAME_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_USER_VISITORS_BY_TAG_ORDER_BY_CREATED_GET).hasAuthority(Privilege.PRIV_USER_VISITORS_BY_TAG_ORDER_BY_CREATED_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_USER_VISITORS_BY_TAG_SEARCH_BY_NAME_GET).hasAuthority(Privilege.PRIV_USER_VISITORS_BY_TAG_SEARCH_BY_NAME_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_ORGANIZATION_SCANS_BY_TAG_ORDER_BY_CREATED_GET).hasAuthority(Privilege.PRIV_ORGANIZATION_SCANS_BY_TAG_ORDER_BY_CREATED_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_ORGANIZATION_SCANS_BY_TAG_SEARCH_BY_NAME_GET).hasAuthority(Privilege.PRIV_ORGANIZATION_SCANS_BY_TAG_SEARCH_BY_NAME_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_USER_SCANS_BY_TAG_ORDER_BY_CREATED_GET).hasAuthority(Privilege.PRIV_USER_SCANS_BY_TAG_ORDER_BY_CREATED_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_USER_SCANS_BY_TAG_SEARCH_BY_NAME_GET).hasAuthority(Privilege.PRIV_USER_SCANS_BY_TAG_SEARCH_BY_NAME_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_ORGANIZATIONS_BY_TAG_SEARCH_BY_DISPLAY_NAME_GET).hasAuthority(Privilege.PRIV_ORGANIZATIONS_BY_TAG_SEARCH_BY_DISPLAY_NAME_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_USERS_BY_TAG_SEARCH_BY_NAME_GET).hasAuthority(Privilege.PRIV_USERS_BY_TAG_SEARCH_BY_NAME_GET)
                .antMatchers(HttpMethod.DELETE, UserVisitController.PATH_USER_VISITOR_DELETE_SECURITY).hasAuthority(Privilege.PRIV_USER_VISITOR_DELETE)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_USER_SCANS_USERS_BY_TAG_ORDER_BY_CREATED_GET).hasAuthority(Privilege.PRIV_USER_SCANS_USERS_BY_TAG_ORDER_BY_CREATED_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_USER_SCANS_USERS_BY_TAG_SEARCH_BY_NAME_GET).hasAuthority(Privilege.PRIV_USER_SCANS_USERS_BY_TAG_SEARCH_BY_NAME_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_USER_SCANS_ORGANIZATIONS_BY_TAG_ORDER_BY_CREATED_GET).hasAuthority(Privilege.PRIV_USER_SCANS_ORGANIZATIONS_BY_TAG_ORDER_BY_CREATED_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_USER_SCANS_ORGANIZATIONS_BY_TAG_SEARCH_BY_NAME_GET).hasAuthority(Privilege.PRIV_USER_SCANS_ORGANIZATIONS_BY_TAG_SEARCH_BY_NAME_GET)
                .antMatchers(HttpMethod.POST, LoginController.PATH_AUTHENTICATION_PASSWORD).hasAuthority(Privilege.PRIV_USER_FOUND)
                .antMatchers(HttpMethod.POST, LoginController.PATH_AUTHENTICATION_OTP).hasAuthority(Privilege.PRIV_USER_FOUND)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_RESOURCES_GET).hasAuthority(Privilege.PRIV_RESOURCES_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_RESOURCES_BY_TYPE_GET).hasAuthority(Privilege.PRIV_RESOURCES_GET)
                .antMatchers(HttpMethod.PUT, OrganizationController.PATH_ORGANIZATION_CATALOGUE_UPDATE).hasAuthority(Privilege.PRIV_ORGANIZATION_CATALOGUE_UPDATE)
                .antMatchers(HttpMethod.PUT, UserController.PATH_USER_CATALOGUE_STAUS_UPDATE).hasAuthority(Privilege.PRIV_USER_CATALOGUE_STATUS_UPDATE)
                .antMatchers(HttpMethod.PUT, OrganizationController.PATH_ORGANIZATION_CATALOGUE_ADD).hasAuthority(Privilege.PRIV_ORGANIZATION_CATALOGUE_ADD)
                .antMatchers(HttpMethod.POST, EmailController.PATH_EMAILS).hasAuthority(Privilege.PRIV_SEND_EMAIL)
                .antMatchers(HttpMethod.GET, ReverseFavouriteProductController.PATH_REVERSE_FAVOURITE_PRODUCT_ORGANIZATIONS_ID_NAME_GET).hasAuthority(Privilege.PRIV_REVERSE_FAVOURITE_PRODUCT_ORGANIZATIONS_ID_NAME_GET)
                .antMatchers(HttpMethod.DELETE, ReverseFavouriteProductController.PATH_REVERSE_FAVOURITE_PRODUCT_DELETE_SECURITY).hasAuthority(Privilege.PRIV_REVERSE_FAVOURITE_PRODUCT_DELETE)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_REVERSE_FAVOURITE_PRODUCTS_BY_ORGANIZATION_GET).hasAuthority(Privilege.PRIV_REVERSE_FAVOURITE_PRODUCTS_BY_ORGANIZATION_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_REVERSE_FAVOURITE_PRODUCTS_GET).hasAuthority(Privilege.PRIV_REVERSE_FAVOURITE_PRODUCTS_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_USER_SCANS_USERS_ORDER_BY_CREATED_GET).hasAuthority(Privilege.PRIV_USER_SCANS_USERS_ORDER_BY_CREATED_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_USER_SCANS_ORGANIZATIONS_ORDER_BY_CREATED_GET).hasAuthority(Privilege.PRIV_USER_SCANS_ORGANIZATIONS_ORDER_BY_CREATED_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_USER_SCANS_USERS_BY_SEARCH_BY_NAME_GET).hasAuthority(Privilege.PRIV_USER_SCANS_USERS_BY_SEARCH_BY_NAME_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_USER_SCANS_ORGANIZATIONS_SEARCH_BY_NAME_GET).hasAuthority(Privilege.PRIV_USER_SCANS_ORGANIZATIONS_SEARCH_BY_NAME_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_EVENTS_ORDER_BY_NAME_GET).hasAuthority(Privilege.PRIV_EVENTS_ORDER_BY_NAME_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_EVENTS_ORDER_BY_START_GET).hasAuthority(Privilege.PRIV_EVENTS_ORDER_BY_START_GET)
                .antMatchers(HttpMethod.PUT, UserController.PATH_USER_TAG_ADD).hasAuthority(Privilege.PRIV_USER_TAG_ADD)
                .antMatchers(HttpMethod.PUT, NodeController.PATH_LEAF_DELETE_SECURITY).hasAuthority(Privilege.PRIV_LEAF_DELETE)
                .antMatchers(HttpMethod.POST, NodeController.PATH_CREATE_NODE).hasAuthority(Privilege.PRIV_CREATE_NODE)
                .antMatchers(HttpMethod.PUT, NodeController.PATH_UPDATE_NODE_SECURITY).hasAuthority(Privilege.PRIV_UPDATE_NODE)
                .antMatchers(HttpMethod.DELETE, NodeController.PATH_NODE_DELETE_SECURITY).hasAuthority(Privilege.PRIV_NODE_DELETE)
                .antMatchers(HttpMethod.PUT, UserController.PATH_USER_DEVICE_ADD).hasAuthority(Privilege.PRIV_USER_DEVICE_ADD)
                .antMatchers(HttpMethod.PUT, UserController.PATH_USER_DEVICE_REMOVE).hasAuthority(Privilege.PRIV_USER_DEVICE_REMOVE)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_RESOURCES_BY_TYPE_AND_TAG_GET).hasAuthority(Privilege.PRIV_RESOURCES_BY_TYPE_AND_TAG_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_ORGANIZATIONS_BY_TAG_SEARCH_BY_DISPLAY_NAME_ORDER_BY_DISPLAY_NAME_GET).hasAuthority(Privilege.PRIV_ORGANIZATIONS_BY_TAG_SEARCH_BY_DISPLAY_NAME_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_ORGANIZATIONS_BY_TAG_ORDER_BY_DISPLAY_NAME_GET).hasAuthority(Privilege.PRIV_ORGANIZATIONS_BY_TAG_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_ORGANIZATIONS_BY_TAG_SEARCH_BY_DESCRIPTION_GET).hasAuthority(Privilege.PRIV_ORGANIZATIONS_BY_TAG_SEARCH_BY_DESCRIPTION_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_NOTIFICATION_GET).hasAuthority(Privilege.PRIV_NOTIFICATION_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_NOTIFICATION_ORDER_BY_CREATED_GET).hasAuthority(Privilege.PRIV_NOTIFICATION_GET)
                .antMatchers(HttpMethod.PUT, UserController.PATH_USER_UPADATE).hasAuthority(Privilege.PRIV_USER_UPADATE)
                .antMatchers(HttpMethod.PUT, OrganizationController.PATH_ORGANIZATION_UPDATE).hasAuthority(Privilege.PRIV_ORGANIZATION_UPDATE)
                .antMatchers(HttpMethod.PUT, ResourceController.PATH_RESOURSE_TAG_ADD).hasAnyAuthority(Privilege.PRIV_RESOURSE_TAG_ADD)
                .antMatchers(HttpMethod.PUT, ResourceController.PATH_RESOURSE_TAG_REMOVE).hasAnyAuthority(Privilege.PRIV_RESOURSE_TAG_REMOVE)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_ORGANIZATION_EXTENSION_BY_KEY_GET).hasAuthority(Privilege.PRIV_ORGANIZATION_EXTENSION_BY_KEY_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_ORGANIZATION_EXTENSION_BY_TAG_GET).hasAuthority(Privilege.PRIV_ORGANIZATION_EXTENSION_BY_TAG_GET)
                .antMatchers(HttpMethod.POST, ProductController.PATH_PRODUCT_CREATE_CSV).hasAuthority(Privilege.PRIV_PRODUCT_CREATE_CSV)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_ENQUIRIES_GET).hasAuthority(Privilege.PRIV_ENQUIRIES_GET)
                .antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_REVERSE_ENQUIRIES_GET).hasAuthority(Privilege.PRIV_REVERSE_ENQUIRIES_GET)
                .antMatchers(HttpMethod.POST, EnquiryController.PATH_ENQUIRY_CREATE).hasAuthority(Privilege.PRIV_ENQUIRY_CREATE)
                //.antMatchers(HttpMethod.GET, RepositoryRestMvcConfig.PATH_TEST_GET).hasAuthority(Privilege.PRIV_TEST_GET)
                .and()
                .addFilterAfter(buildSecurityContextExtendedPersistenceFilter(), SecurityContextPersistenceFilter.class)
                .addFilterBefore(new CustomCorsFilter(), UsernamePasswordAuthenticationFilter.class)
                .addFilterAfter(buildAjaxUsernameProcessingFilter(AUTHENTICATION_USERNAME_URL), UsernamePasswordAuthenticationFilter.class)
                .addFilterAfter(buildAjaxSocialProcessingFilter(AUTHENTICATION_SOCIAL_URL), UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(buildAjaxLoginProcessingFilter(AUTHENTICATION_URL), UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(buildExtendedJwtTokenAuthenticationProcessingFilter(Arrays.asList(permitAllEndpointRequestMatchers), API_ROOT_URL), UsernamePasswordAuthenticationFilter.class)
                .addFilterAfter(buildExtraQueryParamFilter(userService), AnonymousAuthenticationFilter.class);
    }

    protected ExtraQueryParamFilter buildExtraQueryParamFilter(IUserService userService) {
        return new ExtraQueryParamFilter(userService);
    }

    protected AnonymousAuthenticationFilter buildAnonymousAuthenticationFilter(){
        return new ExtendedAnonymousAuthenticationFilter(UUID.randomUUID().toString(),this.authenticationDetailsSource);
    }



    protected AjaxUsernameProcessingFilter buildAjaxUsernameProcessingFilter(String loginEntryPoint) throws Exception {
        AjaxUsernameProcessingFilter filter = new AjaxUsernameProcessingFilter(loginEntryPoint, ajaxUsernameSuccessHandler, ajaxFailureHandler, objectMapper);
        filter.setAuthenticationManager(this.authenticationManager);
        filter.setAuthenticationDetailsSource(authenticationDetailsSource);
        return filter;
    }


    protected SecurityContextExtendedPersistenceFilter buildSecurityContextExtendedPersistenceFilter(){
        return new SecurityContextExtendedPersistenceFilter();
    }




}
