package co.indexify.config;

import co.indexify.domain.model.*;
import co.indexify.domain.model.Resource;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;


@Configuration
public class RepositoryRestMvcConfig extends RepositoryRestConfigurerAdapter {

    public static final String PATH_TEST_GET = "/api/v1/**";
    //public static final String PATH_NODES_GET = "/api/v1/nodes/search/findByOrganizationId";
    public static final String PATH_NODES_GET = "/api/v1/nodes";
    //public static final String PATH_PRODUCTS_GET = "/api/v1/products/search/findByOrganizationId";
    public static final String PATH_PRODUCTS_GET = "/api/v1/products";

    public static final String PATH_COUNTRIES_GET = "/api/v1/countries";
    //public static final String PATH_LEAF_CHILD_PRODUCTS_GET ="/api/v1/products/search/findByOrganizationIdAndLeafId";
    public static final String PATH_LEAF_CHILD_PRODUCTS_GET ="/api/v1/products/search/findByLeafId";

    //public static final String PATH_PRODUCTS_SEARCH_BY_NAME_GET="/api/v1/products/search/findByOrganizationIdAndNameContainingIgnoreCase";
    public static final String PATH_PRODUCTS_SEARCH_BY_NAME_GET="/api/v1/products/search/findByNameContainingIgnoreCase";
    //public static final String PATH_ROOT_NODES_GET="/api/v1/nodes/search/findByOrganizationIdAndParentIsNull";
    public static final String PATH_ROOT_NODES_GET="/api/v1/nodes/search/findByParentIsNull";

    //public static final String PATH_NODE_CHILD_NODES_GET="/api/v1/nodes/search/findByOrganizationIdAndParentId";
    public static final String PATH_NODE_CHILD_NODES_GET="/api/v1/nodes/search/findByParentId";

   // public static final String PATH_LEAF_NODES_GET="/api/v1/nodes/search/findByOrganizationIdAndLeafTrue";
    public static final String PATH_LEAF_NODES_GET="/api/v1/nodes/search/findByLeafTrue";

    //public static final String PATH_PRODUCTS_NOT_LINKED="/api/v1/products/search/findByOrganizationIdAndLeafIsNull";
    public static final String PATH_PRODUCTS_NOT_LINKED="/api/v1/products/search/findByLeafIsNull";

    public static final String PATH_USER_SCANS_GET = "/api/v1/userScans";

    public static final String PATH_USER_SCANS_ORDER_BY_CREATED_GET = "/api/v1/userScans/search/findByOrderByCreatedDesc";

    public static final String PATH_ORGANIZATION_GET = "/api/v1/organizations/search/getById";

    public static final String PATH_ORGANIZATION_SCANS_GET = "/api/v1/organizationScans";

    public static final String PATH_ORGANIZATION_SCANS_ORDER_BY_CREATED_GET = "/api/v1/organizationScans/search/findByOrderByCreatedDesc";


    public static final String PATH_USER_GET = "/api/v1/users/search/getById";

    public static final String PATH_USER_SCANS_SEARCH_BY_NAME_GET = "/api/v1/userScans/search/findByNameContainingIgnoreCaseOrderByCreatedDesc";

    public static final String PATH_ORGANIZATION_SCANS_SEARCH_BY_NAME_GET = "/api/v1/organizationScans/search/findByNameContainingIgnoreCaseOrderByCreatedDesc";

    public static final String PATH_FAVOURITE_PRODUCTS_GET = "/api/v1/favouriteProducts";

    public static final String PATH_FAVOURITE_PRODUCTS_BY_ORGANIZATION_GET = "/api/v1/favouriteProducts/search/findByOrganizationId";

    public static final String PATH_PRODUCT_GET = "/api/v1/products/search/getById";

    public static final String PATH_USERS_BY_TAG_GET = "/api/v1/users/search/findByTagsAndPublicProfileTrue";

    public static final String PATH_ORGANIZATIONS_BY_TAG_GET = "/api/v1/organizations/search/findByTagsAndPublicProfileTrue";

    public static final String PATH_USER_VISITORS_GET = "/api/v1/userVisitors";

    public static final String PATH_USER_VISITORS_ORDER_BY_CREATED_GET = "/api/v1/userVisitors/search/findByOrderByCreatedDesc";

    public static final String PATH_USER_VISITORS_SEARCH_BY_NAME_GET = "/api/v1/userVisitors/search/findByNameContainingIgnoreCaseOrderByCreatedDesc";

    public static final String PATH_USER_SCANS_BY_TAG_ORDER_BY_CREATED_GET = "/api/v1/userScans/search/findByTagsOrderByCreatedDesc";

    public static final String PATH_USER_SCANS_BY_TAG_SEARCH_BY_NAME_GET = "/api/v1/userScans/search/findByTagsAndNameContainingIgnoreCaseOrderByCreatedDesc";

    public static final String PATH_USER_VISITORS_BY_TAG_ORDER_BY_CREATED_GET = "/api/v1/userVisitors/search/findByTagsOrderByCreatedDesc";

    public static final String PATH_USER_VISITORS_BY_TAG_SEARCH_BY_NAME_GET = "/api/v1/userVisitors/search/findByTagsAndNameContainingIgnoreCaseOrderByCreatedDesc";

    public static final String PATH_ORGANIZATION_SCANS_BY_TAG_ORDER_BY_CREATED_GET = "/api/v1/organizationScans/search/findByTagsOrderByCreatedDesc";

    public static final String PATH_ORGANIZATION_SCANS_BY_TAG_SEARCH_BY_NAME_GET = "/api/v1/organizationScans/search/findByTagsAndNameContainingIgnoreCaseOrderByCreatedDesc";

    public static final String PATH_ORGANIZATIONS_BY_TAG_SEARCH_BY_DISPLAY_NAME_GET = "/api/v1/organizations/search/findByTagsAndDisplayNameContainingIgnoreCaseAndPublicProfileTrue";

    public static final String PATH_USERS_BY_TAG_SEARCH_BY_NAME_GET = "/api/v1/users/search/findByTagsAndNameContainingIgnoreCaseAndPublicProfileTrue";


    public static final String PATH_USER_SCANS_USERS_BY_TAG_ORDER_BY_CREATED_GET = "/api/v1/userScans/search/findByTagsAndOrganizationIsNullOrderByCreatedDesc";

    public static final String PATH_USER_SCANS_USERS_BY_TAG_SEARCH_BY_NAME_GET = "/api/v1/userScans/search/findByTagsAndNameContainingIgnoreCaseAndOrganizationIsNullOrderByCreatedDesc";

    public static final String PATH_USER_SCANS_ORGANIZATIONS_BY_TAG_ORDER_BY_CREATED_GET = "/api/v1/userScans/search/findByTagsAndUserIsNullOrderByCreatedDesc";

    public static final String PATH_USER_SCANS_ORGANIZATIONS_BY_TAG_SEARCH_BY_NAME_GET = "/api/v1/userScans/search/findByTagsAndNameContainingIgnoreCaseAndUserIsNullOrderByCreatedDesc";
    public static final String PATH_RESOURCES_GET = "/api/v1/resources";
    public static final String PATH_RESOURCES_BY_TYPE_GET = "/api/v1/resources/findByType";


    //Now

    public static final String PATH_USER_SCANS_USERS_BY_SEARCH_BY_NAME_GET = "/api/v1/userScans/search/findByNameContainingIgnoreCaseAndOrganizationIsNullOrderByCreatedDesc";

    public static final String PATH_USER_SCANS_ORGANIZATIONS_ORDER_BY_CREATED_GET = "/api/v1/userScans/search/findByUserIsNullOrderByCreatedDesc";

    public static final String PATH_USER_SCANS_USERS_ORDER_BY_CREATED_GET = "/api/v1/userScans/search/findByOrganizationIsNullOrderByCreatedDesc";


    public static final String PATH_USER_SCANS_ORGANIZATIONS_SEARCH_BY_NAME_GET = "/api/v1/userScans/search/findByNameContainingIgnoreCaseAndUserIsNullOrderByCreatedDesc";


    public static final String PATH_EVENTS_ORDER_BY_NAME_GET = "/api/v1/events/search/findByOrderByName";
    public static final String PATH_EVENTS_ORDER_BY_START_GET = "/api/v1/events/search/findByOrderByStartDesc";

    public static final String PATH_RESOURCES_BY_TYPE_AND_TAG_GET = "/api/v1/resources/search/findByTagsAndType";

    public static final String PATH_ORGANIZATIONS_BY_TAG_SEARCH_BY_DISPLAY_NAME_ORDER_BY_DISPLAY_NAME_GET = "/api/v1/organizations/search/findByTagsAndDisplayNameContainingIgnoreCaseAndPublicProfileTrueOrderByDisplayNameAsc";


    public static final String PATH_ORGANIZATIONS_BY_TAG_ORDER_BY_DISPLAY_NAME_GET = "/api/v1/organizations/search/findByTagsAndPublicProfileTrueOrderByDisplayNameAsc";
    public static final String PATH_ORGANIZATIONS_BY_TAG_SEARCH_BY_DESCRIPTION_GET = "/api/v1/organizations/search/findByTagsAndDescriptionContainingIgnoreCaseAndPublicProfileTrue";



    public static final String PATH_NOTIFICATION_ORDER_BY_CREATED_GET = "/api/v1/notifications/search/findByOrderByCreatedDesc";

    public static final String PATH_NOTIFICATION_GET = "/api/v1/notifications";

    public static final String  PATH_ORGANIZATION_EXTENSION_BY_TAG_GET = "/api/v1/organizationExtensions/search/findByTags";
    public static final String  PATH_ORGANIZATION_EXTENSION_BY_KEY_GET = "/api/v1/organizationExtensions/search/findByKey";


    public static final String PATH_ENQUIRIES_GET = "/api/v1/enquiries";

    public static final String PATH_REVERSE_ENQUIRIES_GET = "/api/v1/reverseEnquiries";


    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        super.configureRepositoryRestConfiguration(config);
        config.exposeIdsFor(
                Node.class,
                Product.class,
                Organization.class,
                UserScan.class,
                Resource.class,
                OrganizationScan.class,
                FavouriteProduct.class,
                User.class,
                UserVisitor.class,
                ReverseFavouriteProduct.class,
                Event.class,
                Notification.class,
                OrganizationExtension.class,
                Enquiry.class,
                ReverseEnquiry.class
        );
    }

    /*
    @Bean
    HttpMethodBasedInterceptor httpMethodBasedInterceptor(){
        return new HttpMethodBasedInterceptor();
    }


    @Bean
    public MappedInterceptor myMappedInterceptor(HttpMethodBasedInterceptor httpMethodBasedInterceptor) {
        return new MappedInterceptor(new String[]{"/api/v1/**"}, httpMethodBasedInterceptor);
    } */



    public static final String PATH_REVERSE_FAVOURITE_PRODUCTS_GET = "/api/v1/reverseFavouriteProducts";

    public static final String PATH_REVERSE_FAVOURITE_PRODUCTS_BY_ORGANIZATION_GET = "/api/v1/reverseFavouriteProducts/search/findByOrganizationId";








}


//findByTagsAndDisplayNameContainingIgnoreCaseAndPublicProfileTrueOrderByDisplayName