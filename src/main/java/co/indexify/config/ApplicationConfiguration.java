package co.indexify.config;

import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.SetSMSAttributesRequest;
import com.maxmind.db.CHMCache;
import com.maxmind.geoip2.DatabaseReader;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ApplicationEventMulticaster;
import org.springframework.context.event.SimpleApplicationEventMulticaster;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.data.repository.query.SecurityEvaluationContextExtension;
import org.springframework.web.client.RestTemplate;

/**
 * Created by ankur on 03-02-2017.
 */
@Configuration
@EnableScheduling
public class ApplicationConfiguration {
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }


    @Bean
    public ApplicationEventMulticaster applicationEventMulticaster(){
        return new SimpleApplicationEventMulticaster();
    }


    @Bean
    public AmazonS3 s3(){
        return  AmazonS3ClientBuilder.standard()
                .withCredentials(new ClasspathPropertiesFileCredentialsProvider())
                .withRegion(Regions.AP_SOUTH_1).build();
    }


    @Bean
    public AmazonSNS sns(){
        AmazonSNS sns =  AmazonSNSClientBuilder.standard()
                .withCredentials(new ClasspathPropertiesFileCredentialsProvider())
                .withRegion(Regions.AP_SOUTHEAST_1).build();
        SetSMSAttributesRequest setRequest = new SetSMSAttributesRequest()
                .addAttributesEntry("DefaultSenderID", "INDEXIFY")
                .addAttributesEntry("MonthlySpendLimit", "500")
                .addAttributesEntry("DeliveryStatusIAMRole",
                        "arn:aws:iam::853829766482:role/SNSSuccessFeedback")
                .addAttributesEntry("DeliveryStatusSuccessSamplingRate", "100")
                .addAttributesEntry("DefaultSMSType", "Transactional")
                .addAttributesEntry("UsageReportS3Bucket", "indexifysmsreport");

        sns.setSMSAttributes(setRequest);
        return sns;
    }


    @Bean
    public TaskExecutor getTaskExecutor() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(1);
        threadPoolTaskExecutor.setMaxPoolSize(1);
        return threadPoolTaskExecutor;

    }

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Bean
    public HttpClient httpClient(){
        return HttpClientBuilder.create().build();
    }

    @Bean
    public DatabaseReader databaseReader() throws Exception{
        ClassPathResource resource =  new ClassPathResource("GeoLite2-City.mmdb");
        DatabaseReader reader = new DatabaseReader.Builder(resource.getInputStream()).withCache(new CHMCache()).build();
        return reader;
    }

    @Bean
    public SecurityEvaluationContextExtension securityEvaluationContextExtension() {
        return new SecurityEvaluationContextExtension();
    }





}
