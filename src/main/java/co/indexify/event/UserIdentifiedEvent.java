package co.indexify.event;

import co.indexify.security.CustomUserDetails;
import org.springframework.context.ApplicationEvent;

import java.util.Locale;

public class UserIdentifiedEvent extends ApplicationEvent {
    final private Locale locale;

    public UserIdentifiedEvent(final CustomUserDetails userDetails,final Locale locale) {
        super(userDetails);
        this.locale = locale;
    }


    public CustomUserDetails getUserDetails(){
        return (CustomUserDetails)getSource();
    }

    public Locale getLocale() {
        return locale;
    }
}
