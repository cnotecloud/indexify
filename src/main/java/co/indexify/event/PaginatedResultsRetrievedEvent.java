package co.indexify.event;

import org.springframework.context.ApplicationEvent;
import org.springframework.data.domain.Page;

import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

public final class PaginatedResultsRetrievedEvent<T extends Serializable> extends ApplicationEvent {
    private final HttpServletResponse response;
    public PaginatedResultsRetrievedEvent(Page<T> page, final HttpServletResponse response) {
        super(page);
        this.response = response;

    }
    public final HttpServletResponse getResponse() {
        return response;
    }

    @SuppressWarnings("unchecked")
    public Page<T> getPage(){
        return (Page<T>)super.getSource();
    }


}
