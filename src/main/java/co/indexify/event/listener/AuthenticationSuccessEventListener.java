package co.indexify.event.listener;

import co.indexify.domain.model.DomainContext;
import co.indexify.domain.model.Privilege;
import co.indexify.domain.model.User;
import co.indexify.service.IOrganizationService;
import co.indexify.service.IUserService;
import co.indexify.util.SecurityUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AuthenticationSuccessEventListener implements ApplicationListener<AuthenticationSuccessEvent>{
    final private IUserService userService;

    public AuthenticationSuccessEventListener(IUserService userService) {
        this.userService = userService;
    }



    @Override
    @Async
    public void onApplicationEvent(AuthenticationSuccessEvent event) {
        Optional<DomainContext> context = SecurityUtil.getDomainContext(event.getAuthentication());
        if(context.isPresent()) {
            Optional<User> user = SecurityUtil.getMe(event.getAuthentication());
            user.ifPresent(u -> {
                if(!u.getEnabled()){
                    u.setEnabled(true);
                    userService.update(user.get());
                }
            });
        }
    }

}
