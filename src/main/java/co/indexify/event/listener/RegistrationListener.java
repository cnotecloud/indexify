package co.indexify.event.listener;

import co.indexify.domain.model.User;
import co.indexify.event.RegistrationCompleteEvent;
import co.indexify.service.IEmailService;
import co.indexify.service.ISmsService;
import co.indexify.service.IUserService;
import co.indexify.service.impl.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class RegistrationListener implements ApplicationListener<RegistrationCompleteEvent> {
    @Autowired
    private IUserService service;


    @Autowired
    private Environment env;


    @Autowired
    private IEmailService emailService;

    @Autowired
    private ISmsService smsService;


    @Override
    public void onApplicationEvent(final RegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(final RegistrationCompleteEvent event) {
        final User user = event.getUser();
        final String token = UUID.randomUUID().toString();
        service.createVerificationTokenForUser(user, token);
        emailService.sendForRegistration(user, event.getTag(), event.getLocale());

    }

}
