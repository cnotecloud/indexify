package co.indexify.event.listener;

import co.indexify.domain.model.User;
import co.indexify.event.UserIdentifiedEvent;
import co.indexify.service.IEmailService;
import co.indexify.service.ISmsService;
import co.indexify.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class UserIdentifiedEventListener implements ApplicationListener<UserIdentifiedEvent> {

    @Autowired
    private ISmsService smsService;

    @Autowired
    private IEmailService emailService;

    @Autowired
    private IUserService userService;


    @Override
    @Async
    public void onApplicationEvent(UserIdentifiedEvent userIdentifiedEvent) {
        if(userIdentifiedEvent.getUserDetails()!= null && userIdentifiedEvent.getUserDetails().getUser()!= null){
            User user = userIdentifiedEvent.getUserDetails().getUser();
            userService.createOto(user);
            smsService.sendLoginOtp(user, userIdentifiedEvent.getLocale());
            emailService.sendForLoginOtp(user, userIdentifiedEvent.getLocale());
        }
    }
}
