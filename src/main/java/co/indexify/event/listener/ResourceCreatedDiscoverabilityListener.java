package co.indexify.event.listener;

import co.indexify.event.ResourceCreatedEvent;
import org.springframework.context.ApplicationListener;

import com.google.common.base.Preconditions;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import java.net.URI;

@Component
class ResourceCreatedDiscoverabilityListener implements ApplicationListener<ResourceCreatedEvent> {

    @Override
    public void onApplicationEvent(final ResourceCreatedEvent resourceCreatedEvent) {
        Preconditions.checkNotNull(resourceCreatedEvent);

        final HttpServletResponse response = resourceCreatedEvent.getResponse();
        final String id = resourceCreatedEvent.getId();

        addLinkHeaderOnResourceCreation(response, id);
    }
    void addLinkHeaderOnResourceCreation(final HttpServletResponse response, final String id) {
        final URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path(String.format("{%s}", PATH_PARAM_ID)).buildAndExpand(PATH_PARAM_ID).toUri();
        response.setHeader(HttpHeaders.LOCATION, uri.toASCIIString());
    }

    public static final String PATH_PARAM_ID = "id";


}