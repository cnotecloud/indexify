package co.indexify.event;

import org.springframework.context.ApplicationEvent;

import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

public class ResourceCreatedEvent<T extends Serializable> extends ApplicationEvent{
    private final HttpServletResponse response;
    private String id;

    public ResourceCreatedEvent(final Serializable source, final HttpServletResponse response, final String id) {
        super(source);
        this.response = response;
        this.id = id;
    }
    public HttpServletResponse getResponse() {
        return response;
    }

    @SuppressWarnings("unchecked")
    public T getResource(){
        return (T)getSource();
    }

    public String getId() {
        return id;
    }
}
