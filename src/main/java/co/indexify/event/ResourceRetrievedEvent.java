package co.indexify.event;

import co.indexify.domain.model.Domain;
import org.springframework.context.ApplicationEvent;

import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

public class ResourceRetrievedEvent<T extends Domain> extends ApplicationEvent {
    private final HttpServletResponse response;

    public ResourceRetrievedEvent(final Serializable source, final HttpServletResponse response) {
        super(source);

        this.response = response;
    }
    public HttpServletResponse getResponse() {
        return response;
    }

    @SuppressWarnings("unchecked")
    public T getResource(){
        return (T)getSource();
    }



}
