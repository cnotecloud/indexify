package co.indexify.event;

import co.indexify.domain.model.User;
import org.springframework.context.ApplicationEvent;

import java.util.Locale;


@SuppressWarnings("serial")
public class RegistrationCompleteEvent extends ApplicationEvent {

    private final Locale locale;
    private final User user;
    private final String tag;

    public RegistrationCompleteEvent(final User user, final String tag, final Locale locale) {
        super(user);
        this.user = user;
        this.locale = locale;
        this.tag = tag;
    }

    public Locale getLocale() {
        return locale;
    }

    public User getUser() {
        return user;
    }

    public String getTag() {
        return tag;
    }
}

