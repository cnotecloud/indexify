package co.indexify.service;

import co.indexify.domain.model.OrganizationScan;
import co.indexify.domain.model.ReverseEnquiry;

public interface IReverseEnquiryService extends IDomainService<ReverseEnquiry, String> {
    ReverseEnquiry create(ReverseEnquiry reverseEnquiry);
}
