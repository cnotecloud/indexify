package co.indexify.service;

import co.indexify.domain.model.Product;
import co.indexify.domain.model.User;

import java.util.Locale;

/**
 * Created by ankur on 04-10-2016.
 */
public interface IPushNotificationService {
    void sendNotificationForOrganizationScan(User user, User other, Locale locale);
    void sendNotificationForFavouriteProduct(User user, User other, Product product, Locale locale);
}
