package co.indexify.service;

import co.indexify.domain.model.FavouriteProduct;
import co.indexify.domain.model.User;
import co.indexify.dto.FavouriteProductDto;
import co.indexify.dto.MethodSecurityDto;
import co.indexify.dto.OrganizationIdNameDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Locale;

public interface IFavouriteProductService extends IDomainService<FavouriteProduct, String> {
    Page<OrganizationIdNameDto> getOrganizationIdName(Pageable pageable);
    FavouriteProduct create(FavouriteProductDto favouriteProductDto, User user, Locale locale);
}
