package co.indexify.service;

import co.indexify.domain.model.Product;
import co.indexify.domain.model.User;
import co.indexify.dto.ProductCreateCsvDto;
import co.indexify.dto.ProductCreateDto;
import co.indexify.dto.ProductNodeLinkDto;
import co.indexify.dto.ProductNodeUnLinkDto;
import co.indexify.dto.ProductUpdateDto;

import java.util.Optional;

import javax.validation.Valid;

public interface IProductService extends IDomainService<Product, String> {
    Product create(ProductCreateDto dto, User user);
    Product update(String id, ProductUpdateDto dto, User user);
    void linkToNode(ProductNodeLinkDto dto, User user);
    void unassignLeaf(String leafId);

    void createByCsv(ProductCreateCsvDto dto, User user);
	void unlinkToNode(@Valid ProductNodeUnLinkDto productNodeLinkDto, User user);
}
