package co.indexify.service;

import co.indexify.domain.model.Country;

import java.util.Optional;

public interface ICountryService extends IDomainService<Country, String> {
    Optional<Country> findByIso2(String iso2);
}
