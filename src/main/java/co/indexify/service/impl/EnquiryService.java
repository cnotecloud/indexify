package co.indexify.service.impl;

import co.indexify.domain.model.*;
import co.indexify.domain.repository.IEnquiryRepository;
import co.indexify.domain.repository.IUserScanRepository;
import co.indexify.dto.EnquiryDto;
import co.indexify.dto.ResponseCode;
import co.indexify.dto.UserScanDto;
import co.indexify.exception.NotFoundException;
import co.indexify.service.*;
import co.indexify.util.SecurityUtil;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import java.util.Locale;
import java.util.Optional;


@Service
public class EnquiryService extends AbstractDomainService<Enquiry, String> implements IEnquiryService {

    @Autowired
    private IEnquiryRepository enquiryRepository;

    @Autowired
    private IReverseEnquiryService reverseEnquiryService;

    @Autowired
    private IOrganizationService organizationService;


    @Autowired
    private IEmailService emailService;

    @Autowired
    private IUserService userService;


    @Autowired
    private IPushNotificationService notificationService;


    @Override
    public void createNew(EnquiryDto dto, User user, Locale locale) {

        Optional<Organization> organization = organizationService.findById(NumberUtils.toLong(SecurityUtil.getRequestedOrganization()));

        if(organization.isPresent()){
            Enquiry enquiry = new Enquiry();
            enquiry.setSubject(dto.getSubject());
            enquiry.setBody(dto.getBody());
            enquiry.setOrganization(organization.get());
            enquiry.setOrganizationName(organization.get().getDisplayName());
            enquiryRepository.save(enquiry);
            ReverseEnquiry reverseEnquiry = new ReverseEnquiry();
            reverseEnquiry.setBody(dto.getBody());
            reverseEnquiry.setSubject(dto.getSubject());
            reverseEnquiry.setCreatedByName(user.getName());
            reverseEnquiryService.create(reverseEnquiry);
            emailService.sendForEnquiry(user, organization.get(), userService.findByOrganizationId(organization.get().getId()).findFirst().orElse(null), enquiry,locale);

        }






    }


    @Override
    protected MongoRepository<Enquiry, String> getDao() {
        return enquiryRepository;
    }
}
