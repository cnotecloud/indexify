package co.indexify.service.impl;

import co.indexify.aws.sns.SNSMobilePush;
import co.indexify.domain.model.*;
import co.indexify.service.IPushNotificationService;
import co.indexify.service.IPlatformService;
import co.indexify.service.IUserService;
import com.amazonaws.services.sns.model.EndpointDisabledException;
import com.amazonaws.services.sns.model.InvalidParameterException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

import static co.indexify.domain.model.Platform.*;

/**
 * Created by ankur on 04-10-2016.
 */
@Service
public class AmazonPushNotificationService implements IPushNotificationService {

    private static final String MESSAGE_FAVOURITE_PRODUCT_TITLE = "product.favourite.title";
    private static final String MESSAGE_ORGANIZATION_SCAN_TITLE = "organization.scan.title";


    private static final String MESSAGE_FAVOURITE_PRODUCT_BODY = "product.favourite.body";
    private static final String MESSAGE_ORGANIZATION_SCAN_BODY = "organization.scan.body";


    private static final Logger logger = LoggerFactory.getLogger(AmazonPushNotificationService.class);


    @Autowired
    private NotificationService notificationService;




    @Autowired
    private SNSMobilePush snsMobilePush;

    @Autowired
    private IPlatformService platformService;

    @Autowired
    private IUserService userService;

    @Autowired
    private TaskExecutor taskExecutor;


    @Autowired
    private MessageSource messageSource;


    public void sendNotificationToUser(User user, Map<String, String> message) {
        Set<Device> devices = user.getDevices();
        if (!CollectionUtils.isEmpty(devices)) {
            devices.forEach(device -> {
                taskExecutor.execute(() -> {
                    sendNotificationToDevice(user, device, message);
                });

            });
        }

    }

    @Override
    @Async
    public void sendNotificationForOrganizationScan(User user, User other, Locale locale) {
        Map<String, String> message = new HashMap<>();
        String title = messageSource.getMessage(MESSAGE_ORGANIZATION_SCAN_TITLE, null, "", locale);
        message.put("title", title);
        String body = messageSource.getMessage(MESSAGE_ORGANIZATION_SCAN_BODY, new Object[]{user.getName()}, "", locale);
        message.put("body", body);
        message.put("tag", "organization_scan");
        sendNotificationToUser(other, message);
        Notification notification =  new Notification();
        notification.setBody(body);
        notification.setTitle(title);
        notificationService.create(notification, other.getId());
    }


    @Override
    @Async
    public void sendNotificationForFavouriteProduct(User user, User other, Product product, Locale locale) {
        Map<String, String> message = new HashMap<>();
        String title = messageSource.getMessage(MESSAGE_FAVOURITE_PRODUCT_TITLE, null, "", locale);
        message.put("title", title);
        String body = messageSource.getMessage(MESSAGE_FAVOURITE_PRODUCT_BODY, new Object[]{user.getName()}, "", locale);
        message.put("body", body);
        message.put("tag", "favourite_product");
        Notification notification =  new Notification();
        notification.setBody(body);
        notification.setTitle(title);
        notificationService.create(notification, other.getId());
        sendNotificationToUser(other, message);
    }


    private void sendNotificationToDevice(User user, Device device, Map<String, String> message) {
        String platformType = device.getPlatform();
        Optional<Platform> platform = platformService.findByType(platformType);
        message.put("token", device.getId());
        platform.ifPresent(p -> {
            try {
                switch (platform.get().getType()) {
                    case GCM:
                        snsMobilePush.androidAppNotification(p.getPrincipal(), p.getApplication(), device.getId(), message);
                        break;
                    case APNS_SANDBOX:
                        snsMobilePush.appleSandboxAppNotification(p.getPrincipal(), p.getCredential(), p.getApplication(), device.getId(), message);
                        break;
                    case APNS:
                        snsMobilePush.appleAppNotification(p.getPrincipal(), p.getCredential(), p.getApplication(), device.getId(), message);
                        break;
                }
            } catch (EndpointDisabledException endpointDisabledException) {
                logger.debug("Message not sent: {}", endpointDisabledException.getMessage());
                endpointDisabledException.printStackTrace();
                userService.removeDevice(user, device);
            } catch (InvalidParameterException invalidParameterException) {
                userService.removeDevice(user, device);
                logger.debug("Message not sent: {}", invalidParameterException.getMessage());
                invalidParameterException.printStackTrace();
            }
        });


    }

}

