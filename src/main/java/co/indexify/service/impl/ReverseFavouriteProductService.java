package co.indexify.service.impl;

import co.indexify.domain.model.Organization;
import co.indexify.domain.model.ReverseFavouriteProduct;
import co.indexify.domain.repository.IReverseFavouriteProductRepository;
import co.indexify.dto.FavouriteProductDto;
import co.indexify.dto.OrganizationIdNameDto;
import co.indexify.service.IOrganizationService;
import co.indexify.service.IProductService;
import co.indexify.service.IReverseFavouriteProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;


@Service
public class ReverseFavouriteProductService extends AbstractDomainService<ReverseFavouriteProduct, String> implements IReverseFavouriteProductService{

    @Autowired
    private IReverseFavouriteProductRepository reverseFavouriteProductRepository;

    @Autowired
    private IProductService productService;

    @Autowired
    private IOrganizationService organizationService;

    @Override
    public ReverseFavouriteProduct create(ReverseFavouriteProduct reverseFavouriteProduct, Long organizationId) {
       return reverseFavouriteProductRepository.persist(reverseFavouriteProduct, organizationId);
    }


    @Override
    public Page<OrganizationIdNameDto> getOrganizationIdName(Pageable pageable){
        return reverseFavouriteProductRepository.getOrganizationIdName(pageable);

    }

    @Override
    protected MongoRepository<ReverseFavouriteProduct, String> getDao() {
        return reverseFavouriteProductRepository;
    }
}
