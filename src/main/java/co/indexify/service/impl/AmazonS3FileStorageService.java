package co.indexify.service.impl;

import co.indexify.domain.model.Resource;
import co.indexify.exception.FileStorageServiceException;
import co.indexify.service.IFileStorageService;
import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import org.apache.http.client.HttpClient;
import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class AmazonS3FileStorageService implements IFileStorageService {
    public static final String MESSAGE_UPLOAD_ERROR_S3 =  "Problem in uploading file to S3.";

    @Value("${aws.s3.bucket}")
    private String bucket;
    @Autowired
    private AmazonS3 s3;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private HttpClient httpClient;

    @Override
    public String uploadFileToS3(String directory, Resource.ResourceType type, MultipartFile file){
        String key = String.format("%s/%s/%s", directory, type.getSubDirectory(), file.getOriginalFilename());
        try {
            ObjectMetadata objectMetadata = new ObjectMetadata();
            Tika tika =  new Tika();
            String mime = tika.detect(file.getOriginalFilename());
            objectMetadata.setContentType(mime);
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, key, file.getInputStream(), objectMetadata);
            putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead);
            PutObjectResult putObjectResult = s3.putObject(putObjectRequest);
            return ((AmazonS3Client) s3).getResourceUrl(bucket, key);
        } catch (AmazonClientException | IOException ex) {
            throw new FileStorageServiceException(MESSAGE_UPLOAD_ERROR_S3);
        }
    }


    /*

    @Override
    public void doesBucketObjectExists(String bucket, String dirctory){
        try {
            boolean flag = s3.doesObjectExist(bucket, dirctory + "/");
            if (!flag) {
                throw new ServiceException("S3 Bucket or Directory does not exist.");
            }
        } catch (AmazonClientException ex) {
            throw new ServiceRuntimeException("Problem in uploading file to S3.");
        }
    }

    @Override
    public File downloadFileFromBucketObject(String bucket, String directory, String fileName){
        try{
            GetObjectRequest getObjectRequest = new GetObjectRequest(bucket, directory + "/" + fileName);
            S3Object object = s3.getObject(getObjectRequest);
            InputStream is = object.getObjectContent();
            int indexOfDot = fileName.indexOf(".");
            String prefix = fileName.substring(0, indexOfDot);
            String suffix = fileName.substring(indexOfDot + 1);
            File file = File.createTempFile(prefix, suffix);
            file.deleteOnExit();
            return file;
        }catch (AmazonClientException | IOException ex){
            throw new ServiceRuntimeException("Problem in downloading file from S3.");
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public File downloadFileFromBucketObjectUsigUrl(String httpUrl){
        try{
            HttpGet request = new HttpGet(httpUrl);
            HttpResponse response = httpClient.execute(request);
            String fileName = FilenameUtils.getName(httpUrl);
            fileName = URLDecoder.decode(fileName,Charsets.UTF_8.toString());
            int indexOfDot = fileName.indexOf(".");
            String prefix = fileName.substring(0, indexOfDot);
            String suffix = fileName.substring(indexOfDot);
            File file = File.createTempFile(prefix, suffix);
            FileOutputStream fos = new FileOutputStream(file);
            response.getEntity().writeTo(fos);
            fos.close();
            file.deleteOnExit();
            return file;

        } catch (ClientProtocolException ex){
            throw new ServiceRuntimeException("Problem in downloading file from S3.");
        }catch (IOException ex){
            throw new ServiceRuntimeException("Problem in downloading file from S3.");
        }

    } */

}
