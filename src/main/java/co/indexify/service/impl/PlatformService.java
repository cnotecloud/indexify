package co.indexify.service.impl;

import co.indexify.domain.model.Platform;
import co.indexify.domain.repository.IPlatformRepository;
import co.indexify.service.IPlatformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PlatformService extends AbstractDomainService<Platform, String> implements IPlatformService {

    @Autowired
    private IPlatformRepository platformRepository;

    @Override
    protected MongoRepository<Platform, String> getDao() {
        return platformRepository;
    }

    @Override
    public Optional<Platform> findByType(String type) {
        return platformRepository.findByType(type);
    }
}
