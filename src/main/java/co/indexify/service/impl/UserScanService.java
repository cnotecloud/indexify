package co.indexify.service.impl;

import co.indexify.domain.model.*;
import co.indexify.domain.repository.IUserScanRepository;
import co.indexify.dto.ResponseCode;
import co.indexify.dto.UserScanDto;
import co.indexify.exception.*;
import co.indexify.service.*;
import co.indexify.util.SecurityUtil;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import java.util.Locale;
import java.util.Optional;


@Service
public class UserScanService extends AbstractDomainService<UserScan, String> implements IUserScanService {

    @Autowired
    private IUserScanRepository userScanRepository;

    @Autowired
    private IOrganizationService organizationService;

    @Autowired
    private IOrganizationScanService organizationScanService;

    @Autowired
    private IUserService userService;


    @Autowired
    private IUserVisitorService userVisitorService;

    @Autowired
    private IEmailService emailService;


    @Autowired
    private IPushNotificationService notificationService;


    @Override
    public void createNew(UserScanDto dto, User user, Locale locale) {
        if(dto.getUserId() != null){
            Optional<User> other = userService.findById(dto.getUserId());
            if(other.isPresent()){
                if(user.getId() != other.get().getId()){
                    UserScan userScan = new UserScan();
                    if(dto.getTag() != null){
                        userScan.addTag(dto.getTag());
                    }
                    userScan.setUser(other.get());
                    userScan.setName(other.get().getName());
                    create(userScan);
                    UserVisitor userVisitor = new UserVisitor();
                    if(dto.getTag()!= null){
                        userVisitor.addTag(dto.getTag());
                    }
                    userVisitor.setName(user.getName());
                    userVisitorService.create(userVisitor, other.get().getId());
                }
            }else {
                throw new NotFoundException(User.class.getSimpleName(), ResponseCode.USER_NOT_FOUND);
            }
        }
        else {
            Optional<Organization> organization = organizationService.findById(NumberUtils.toLong(SecurityUtil.getRequestedOrganization()));
            if (organization.isPresent()) {
                UserScan userScan = new UserScan();
                if (dto.getTag() != null) {
                    userScan.addTag(dto.getTag());
                }
                userScan.setOrganization(organization.get());
                userScan.setName(organization.get().getDisplayName());
                create(userScan);
                OrganizationScan organizationScan = new OrganizationScan();
                organizationScan.setName(user.getName());
                if (dto.getTag() != null) {
                    organizationScan.addTag(dto.getTag());
                }
                organizationScanService.create(organizationScan);
                Optional<User> other = userService.findByOrganizationId(organization.get().getId()).findFirst();
                emailService.sendForOrganizationScan(user, organization.get(), other.orElse(null), locale);
                other.ifPresent(o -> {
                    notificationService.sendNotificationForOrganizationScan(user, o, locale);
                });
            } else {
                throw new NotFoundException(Organization.class.getSimpleName(), ResponseCode.ORGANIZATION_NOT_FOUND);
            }
        }
    }


    @Override
    protected MongoRepository<UserScan, String> getDao() {
        return userScanRepository;
    }
}
