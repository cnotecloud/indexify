package co.indexify.service.impl;

import co.indexify.domain.model.*;
import co.indexify.domain.repository.IFavouriteProductRepository;
import co.indexify.dto.FavouriteProductDto;
import co.indexify.dto.OrganizationIdNameDto;
import co.indexify.dto.ResponseCode;
import co.indexify.exception.NotFoundException;
import co.indexify.service.*;
import co.indexify.util.SecurityUtil;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import java.util.Locale;
import java.util.Optional;


@Service
public class FavouriteProductService extends AbstractDomainService<FavouriteProduct, String> implements IFavouriteProductService {

    @Autowired
    private IFavouriteProductRepository favouriteProductRepository;

    @Autowired
    private IProductService productService;

    @Autowired
    private IOrganizationService organizationService;

    @Autowired
    private IReverseFavouriteProductService reverseFavouriteProductService;


    @Autowired
    private IPushNotificationService notificationService;


    @Autowired
    private IEmailService emailService;

    @Autowired
    private IUserService userService;

    @Override
    public FavouriteProduct create(FavouriteProductDto favouriteProductDto, User user, Locale locale) {
        Optional<Organization> organization = organizationService.findById(NumberUtils.toLong(SecurityUtil.getOrganization()));
        if (!organization.isPresent())
            throw new NotFoundException(Organization.class.getSimpleName(), ResponseCode.ORGANIZATION_NOT_FOUND);

        Product product = productService.findById(favouriteProductDto.getProductId()).orElse(null);
        if (product == null) {
            throw new NotFoundException(Product.class.getSimpleName(), ResponseCode.PRODUCT_NOT_FOUND);
        }

        ReverseFavouriteProduct reverseFavouriteProduct = new ReverseFavouriteProduct();
        if (user.getOrganization() != null) {
            reverseFavouriteProduct.setOrganizationName(user.getOrganization().getDisplayName());
            reverseFavouriteProduct.setOrganization(user.getOrganization());
        }
        reverseFavouriteProduct.setProduct(product);
        reverseFavouriteProduct.setProductName(product.getName());
        reverseFavouriteProduct.setUserName(user.getName());
        reverseFavouriteProductService.create(reverseFavouriteProduct, organization.get().getId());
        Optional<User> other = userService.findByOrganizationId(organization.get().getId()).findFirst();
        emailService.sendForFavouriteProductScan(user, organization.get(), other.orElse(null), product, locale);
        other.ifPresent(o -> notificationService.sendNotificationForFavouriteProduct(user,o , product, locale));
        FavouriteProduct favouriteProduct = new FavouriteProduct();
        favouriteProduct.setOrganizationName(organization.get().getDisplayName());
        favouriteProduct.setOrganization(organization.get());
        favouriteProduct.setProductName(product.getName());
        favouriteProduct.setProduct(product);
        return create(favouriteProduct);
    }


    @Override
    public Page<OrganizationIdNameDto> getOrganizationIdName(Pageable pageable) {
        return favouriteProductRepository.getOrganizationIdName(pageable);

    }

    @Override
    protected MongoRepository<FavouriteProduct, String> getDao() {
        return favouriteProductRepository;
    }
}
