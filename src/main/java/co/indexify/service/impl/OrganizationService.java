package co.indexify.service.impl;

import co.indexify.domain.model.*;
import co.indexify.domain.model.Resource;
import co.indexify.domain.repository.IOrganizationRepository;
import co.indexify.dto.*;
import co.indexify.exception.ExistException;
import co.indexify.service.IFileStorageService;
import co.indexify.service.IOrganizationService;
import co.indexify.service.IResourceService;
import co.indexify.service.IUserService;
import com.google.common.collect.Sets;
import org.apache.commons.text.RandomStringGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.Collections;

@Service("organizationService")
public class OrganizationService extends AbstractDomainService<Organization, Long> implements IOrganizationService {

    @Autowired
    private IOrganizationRepository organizationRepository;


    @Autowired
    private IFileStorageService fileStorageService;

    @Autowired
    private IUserService userService;

    @Autowired
    private IResourceService resourceService;

    public static final String IMAGE_TAG = "Logo";

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Override
    public void addTag(Organization organization, String tag) {
        organization.addTag(tag);
        update(organization);

    }

    @Override
    public void updateCatalogue(Organization organization, CalalogueDto dto) {
        organization.setCatalogue(dto.getCatalogue());
        update(organization);
    }

    @Override
    public void addCatalogue(Organization organization, @Valid CalalogueDto dto) {
        organization.addCatalogue(dto.getCatalogue());
        update(organization);
    }

    @Override
    public void update(Organization organization, OrganizationUpdateDto organizationUpdateDto) {
        organization.setDisplayName(organizationUpdateDto.getDisplayName());
        organization.setDescription(organizationUpdateDto.getDescription());
        organization.setLocation(Location.fromDto(organizationUpdateDto.getLocation()));
        organization.setWebsite(organizationUpdateDto.getWebsite());
        organization.setEmails(Collections.singleton(new Email(organizationUpdateDto.getEmail())));
        organization.setContacts(Collections.singleton(new Contact(organizationUpdateDto.getContact())));
        organization.setImgUrl(organizationUpdateDto.getImgUrl());
        update(organization);
    }


    @Override
    @SuppressWarnings("deprecation")
    public Organization registerNew(OrganizationCreateDto organizationCreateDto, User user) {
        if (user.getOrganization() == null) {
            Organization organization = Organization.fromDto(organizationCreateDto);
            Resource resource = null;
            if (organizationCreateDto.getImage() != null) {
                ResourceDto resourceDto = new ResourceDto()
                        .assignFile(organizationCreateDto.getImage())
                        .image()
                        .tag("logo")
                        .organization(organization);
                resource = resourceService.upload(resourceDto);
                organization.setImgUrl(resource.getUrl());
            }
            organization = organizationRepository.persist(organization);
            user.setOrganization(organization);
            userService.update(user);
            if(resource != null)
                resource = resourceService.create(resource);
            User ecalalogue = new User();
            ecalalogue.setFirstName("Indexify");
            ecalalogue.setLastName("Ecatalogue");
            String username = new RandomStringGenerator.Builder().withinRange('a', 'z').build().generate(15);
            ecalalogue.setUsername(username);
            ecalalogue.setEmail(new Email(String.format("%s@cnote.co",username), Email.EMAIL_PRIMARY));
            ecalalogue.setPassword(passwordEncoder.encode(username));
            ecalalogue.setRoles(Sets.newHashSet(Role.ROLE_ECATALOGUE));
            ecalalogue.setEnabled(true);
            ecalalogue.setOrganization(organization);
            userService.create(ecalalogue);
            return organization;

        }
        else
            throw new ExistException(Organization.class.getSimpleName(), ResponseCode.ORGANIZATION_EXIST);

    }



    @Override
    public Organization create(Organization organization) {
        return organizationRepository.persist(organization);
    }

    @Override
    protected MongoRepository<Organization, Long> getDao() {
        return organizationRepository;
    }

}
