package co.indexify.service.impl;

import co.indexify.domain.model.User;
import co.indexify.service.ISmsService;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Service
public class AmazonSmsService implements ISmsService {

    private static final Logger logger = LoggerFactory.getLogger(AmazonSmsService.class);


    @Autowired
    private AmazonSNS sns;

    @Autowired
    private MessageSource messageSource;



    public void send(String contact, String text) {
        Map<String, MessageAttributeValue> smsAttributes =
                new HashMap<String, MessageAttributeValue>();
        //smsAttributes.put("AWS.SNS.SMS.SenderID",new MessageAttributeValue().withStringValue("INDEXIFY").withDataType("String"));
        smsAttributes.put("AWS.SNS.SMS.SMSType",new MessageAttributeValue().withStringValue("Transactional").withDataType("String"));
        smsAttributes.put("AWS.SNS.SMS.MaxPrice", new MessageAttributeValue().withStringValue("0.50").withDataType("Number"));
        smsAttributes.put("AWS.SNS.SMS.SenderID", new MessageAttributeValue()
                .withStringValue("INDEXIFY") //The sender ID shown on the device.
                .withDataType("String"));


        PublishResult result = sns.publish(new PublishRequest()
                .withMessage(text)
                .withPhoneNumber(contact)
                .withMessageAttributes(smsAttributes));
        logger.debug("{}", result);
    }

    @Override
    @Async
    public void sendLoginOtp(User user, Locale locale) {
        if(user!= null && user.getContact()!= null && user.getContact().getValue()!= null){
            String message = messageSource.getMessage("message.loginOtp", new Object[]{user.getOtp().getValue()}, locale);
            send(user.getContact().getValue(), message);
        }
    }



}
