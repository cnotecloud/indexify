package co.indexify.service.impl;

import co.indexify.domain.model.OrganizationScan;
import co.indexify.domain.repository.IOrganizationScanRepository;
import co.indexify.dto.MethodSecurityDto;
import co.indexify.service.IOrganizationScanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
public class OrganizationScanService extends AbstractDomainService<OrganizationScan, String> implements IOrganizationScanService {

    @Autowired
    private IOrganizationScanRepository organizationScanRepository;


    @Override
    public OrganizationScan create(OrganizationScan organizationScan) {
       return organizationScanRepository.persist(organizationScan);
    }

    @Override
    protected MongoRepository<OrganizationScan, String> getDao() {
        return organizationScanRepository;
    }

}
