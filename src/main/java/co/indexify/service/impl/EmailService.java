package co.indexify.service.impl;

import co.indexify.domain.model.*;
import co.indexify.domain.repository.IUserRepository;
import co.indexify.dto.DataSourceDto;
import co.indexify.dto.EmailDto;
import co.indexify.dto.EmailPostDto;
import co.indexify.dto.EmailsDto;
import co.indexify.service.IEmailService;
import co.indexify.service.IEmailTemplateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.core.task.TaskExecutor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.mail.internet.MimeMessage;
import java.util.*;

/**
 * Created by ankur on 29-05-2017.
 */
@Service
public class EmailService implements IEmailService {

    private static final Logger logger = LoggerFactory.getLogger(EmailService.class);


    //Messages
    private static final String MESSAGE_REGISTRATION_SUBJECT = "user.registration.mail.subject";
    private static final String MESSAGE_FORGET_PASSWORD_SUBJECT = "user.registration.forgetPassword.subject";
    private static final String MESSAGE_FAVOURITE_PRODUCT_SUBJECT = "product.favourite.subject";
    private static final String MESSAGE_ORGANIZATION_SCAN_SUBJECT = "organization.scan.subject";
    public static final String MESSAGE_CNOTE_SUBJECT = "emailSent.cnote.subject";
    public static final String MESSAGE_EVENT_BODY_LINE = "emailSent.cnote.event.bodyLine";
    public static final String MESSAGE_DEFAULT_EVENT_BODY_LINE = "emailSent.cnote.defaultEvent.bodyLine";
    public static final String MESSAGE_SENT_EMAIL_SUBJECT = "emailSent.email.subject";
    public static final String MESSAGE_CREATE_USER_SUBJECT = "emailSent.createUser.subject";
    public static final String MESSAGE_LOGIN_OTP_SUBJECCT = "loginOtp.mail.subject";
    private static final String MESSAGE_ENQUIRY_SUBJECT = "enquiry.mail.subject";
    //Messages

    //Autowiring
    @Resource(name = "mailSender")
    private JavaMailSender javaMailSender;
    @Autowired
    private Environment environment;
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private IEmailTemplateService emailTemplateService;
    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private TaskExecutor taskExecutor;
    //Autowiring


    //Property
    public static final String PROPERTY_SUPPORT_EMAIL_FORMAT = "support.email.format";
    public static final String PROPERTY_SUPPORT_EMAIL = "support.email";
    public static final String PROPERTY_SUPPORT_EMAIL_ORGANIZATION = "support.email.organization";
    //Property


    private MimeMessage convertEmailDtoToMimeMessage(JavaMailSender sender, EmailDto emailDto) throws Exception {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
        //messageHelper.setFrom();
        messageHelper.setFrom(emailDto.getFrom());
        messageHelper.setTo(emailDto.getTo());
        messageHelper.setSubject(emailDto.getSubject());
        messageHelper.setText(emailDto.getBody(), true);
        if (emailDto.getDataSources() != null && !emailDto.getDataSources().isEmpty())
            for (DataSourceDto dataSource : emailDto.getDataSources()) {
                messageHelper.addAttachment(dataSource.getOrignalFileName(), dataSource.getDataSource());
            }
        if (emailDto.getReplyTo() != null)
            messageHelper.setReplyTo(emailDto.getReplyTo());
        if (emailDto.getCcs() != null && !emailDto.getCcs().isEmpty())
            for (String cc : emailDto.getCcs())
                messageHelper.addCc(cc);
        return message;
    }


    private void sendHtmlMail(EmailsDto emailsDto) {
        taskExecutor.execute(() -> {
            List<EmailDto> emailDtos = emailsDto.getEmails();
            if (emailDtos != null && !emailDtos.isEmpty())
                for (EmailDto emailDto : emailDtos) {
                    try {
                        MimeMessage message = convertEmailDtoToMimeMessage(emailsDto.getSender(), emailDto);
                        emailsDto.getSender().send(message);
                    } catch (Exception ex) {
                        logger.debug("Problem in sending email to {}: {}", emailDto.getTo(), ex.getMessage());
                    }
                }

        });


    }


    private JavaMailSender getMailSender(SmtpCredentials smtpCredentials) {
        JavaMailSender sender = null;
        if (smtpCredentials != null) {
            JavaMailSenderImpl selfJavaMailSender = new JavaMailSenderImpl();
            selfJavaMailSender.setHost(environment.getProperty("spring.mail.host"));
            selfJavaMailSender.setPort(environment.getProperty("spring.mail.port", int.class));
            selfJavaMailSender.setUsername(smtpCredentials.getUsername());
            selfJavaMailSender.setPassword(smtpCredentials.getPassword());
            Properties properties = new Properties();
            properties.put("mail.smtp.auth", environment.getProperty("spring.mail.properties.mail.smtp.auth", boolean.class));
            properties.put("mail.debug", environment.getProperty("spring.mail.properties.mail.debug", boolean.class));
            properties.put("mail.smtp.socketFactory.fallback", environment.getProperty("spring.mail.properties.mail.smtp.socketFactory.fallback", boolean.class));
            properties.put("mail.smtp.starttls.enable", environment.getProperty("spring.mail.properties.mail.smtp.starttls.enable", boolean.class));
            selfJavaMailSender.setJavaMailProperties(properties);
            sender = selfJavaMailSender;
        } else {
            sender = javaMailSender;
        }
        return sender;
    }

    private String getFrom(SmtpCredentials smtpCredentials, String orgnanizationName) {
        if (smtpCredentials != null && !StringUtils.isEmpty(orgnanizationName))
            return String.format(environment.getProperty(PROPERTY_SUPPORT_EMAIL_FORMAT, ""), orgnanizationName, smtpCredentials.getEmail());
        else if (orgnanizationName != null)
            return String.format(environment.getProperty(PROPERTY_SUPPORT_EMAIL_FORMAT, ""), orgnanizationName, environment.getProperty(PROPERTY_SUPPORT_EMAIL, ""));
        else
            return String.format(environment.getProperty(PROPERTY_SUPPORT_EMAIL_FORMAT, ""), environment.getProperty(PROPERTY_SUPPORT_EMAIL_ORGANIZATION, ""), environment.getProperty(PROPERTY_SUPPORT_EMAIL, ""));

    }


    @Override
    @Async
    public void sendForRegistration(User user, String tag, Locale locale) {
        String createRegistrationHtml = null;
        try {
            createRegistrationHtml = emailTemplateService.createRegistrationHtml(user, tag);
        } catch (RuntimeException ex) {
            logger.debug("Problem in making create user email html: {}", ex.getMessage());
        }

        String createNewUserHtml = null;
        try {
            createNewUserHtml = emailTemplateService.createNewUserHtml(user);
        } catch (RuntimeException ex) {
            logger.debug("Problem in making create user email html: {}", ex.getMessage());
        }


        if (!StringUtils.isEmpty(createRegistrationHtml) && !StringUtils.isEmpty(createNewUserHtml)) {
            String email = user.getEmail().getValue();
            String subject = messageSource.getMessage(MESSAGE_REGISTRATION_SUBJECT, null,"", locale);
            JavaMailSender sender = getMailSender(null);
            String from = getFrom(null, user.getOrganization() != null ? user.getOrganization().getDisplayName() : null);
            String emailSales = environment.getProperty("sales.email", "");
            if (!StringUtils.isEmpty(email) && !StringUtils.isEmpty(emailSales)) {
                EmailDto emailDtoUser = new EmailDto(subject, email, from, createRegistrationHtml, null, null, null);
                EmailDto emailDtoSales = new EmailDto(subject, emailSales, from, createNewUserHtml, null, null, email);

                sendHtmlMail(new EmailsDto(Arrays.asList(emailDtoUser, emailDtoSales), sender));
            }
            if (sender != javaMailSender) {
                sender = null;
                Runtime.getRuntime().gc();
            }
        }
    }

    @Override
    @Async
    public void sendForForgetPassword(User user, Locale locale) {
        String createForgetPasswordHtml = null;
        try {
            createForgetPasswordHtml = emailTemplateService.createForgetPasswordHtml(user);
        } catch (RuntimeException ex) {
            logger.debug("Problem in making create user email html: {}", ex.getMessage());
        }
        if (!StringUtils.isEmpty(createForgetPasswordHtml)) {
            String subject = messageSource.getMessage(MESSAGE_FORGET_PASSWORD_SUBJECT, null, locale);
            sendToUser(user, subject, createForgetPasswordHtml);
        }
    }

    private void sendToUser(User user, String subject, String template) {
        String email = user.getEmail().getValue();
        JavaMailSender sender = getMailSender(null);
        String from = getFrom(null, null);
        if (!StringUtils.isEmpty(email)) {
            EmailDto emailDto = new EmailDto(subject, email, from, template, null, null, null);
            sendHtmlMail(new EmailsDto(Collections.singletonList(emailDto), sender));
        }
        if (sender != javaMailSender) {
            sender = null;
            Runtime.getRuntime().gc();
        }

    }


    private void sendToUserAndOrganization(User user, Organization organization, String subject, String template) {
        String targetUserEmail = user.getEmail().getValue();
        Optional<String> targetOrganizationEmail = organization.getEmails().stream().map(Email::getValue).findFirst();
        JavaMailSender sender = getMailSender(null);
        String from = getFrom(null, null);
        List<String> ccs = null;
        if (targetOrganizationEmail.isPresent()) {
            ccs = Collections.singletonList(targetOrganizationEmail.get());
        }
        if(targetUserEmail != null){
            EmailDto emailDto = new EmailDto(subject, targetUserEmail, from, template, null, ccs, null);
            sendHtmlMail(new EmailsDto(Collections.singletonList(emailDto), sender));
        }

        if (sender != javaMailSender) {
            sender = null;
            Runtime.getRuntime().gc();
        }


    }

    @Override
    @Async
    public void sendForLoginOtp(User user, Locale locale) {
        String createLoginOtpHtml = null;
        try {
            createLoginOtpHtml = emailTemplateService.createLoginOtpHtml(user);
        } catch (RuntimeException ex) {
            logger.debug("Problem in making create user email html: {}", ex.getMessage());
        }
        if (createLoginOtpHtml != null) {
            String subject = messageSource.getMessage(MESSAGE_LOGIN_OTP_SUBJECCT, null, locale);
            sendToUser(user, subject, createLoginOtpHtml);
        }
    }


    @Override
    @Async
    public void sendForOrganizationScan(User user, Organization targetOrganization, User targetUser, Locale locale) {
        String createOrganizationScanTemplate = null;
        try {
                createOrganizationScanTemplate = emailTemplateService.createOrganizationScanHtml(user, targetOrganization);
        } catch (RuntimeException ex) {
            logger.debug("Problem in making create user email html: {}", ex.getMessage());
        }
        if (!StringUtils.isEmpty(createOrganizationScanTemplate)) {
            String subject = messageSource.getMessage(MESSAGE_ORGANIZATION_SCAN_SUBJECT, null, "", locale);
            sendToUserAndOrganization(targetUser, targetOrganization, subject, createOrganizationScanTemplate);
        }
    }


    @Override
    @Async
    public void sendForFavouriteProductScan(User user, Organization targetOrganization, User targetUser, Product product, Locale locale) {
        String createFavouriteProductHtml = null;
        try {
                createFavouriteProductHtml = emailTemplateService.createFavouriteProductScanHtml(user, targetOrganization, product);
        } catch (RuntimeException ex) {
            logger.debug("Problem in making create user email html: {}", ex.getMessage());
        }
        if (!StringUtils.isEmpty(createFavouriteProductHtml)) {
            String subject = messageSource.getMessage(MESSAGE_FAVOURITE_PRODUCT_SUBJECT, null, "", locale);
            sendToUserAndOrganization(targetUser, targetOrganization, subject, createFavouriteProductHtml);
        }
    }

    @Override
    public void sendForEnquiry(User user, Organization targetOrganization, User targetUser, Enquiry enquiry, Locale locale) {
        String createEnquiryHtml = null;
        try {
            createEnquiryHtml = emailTemplateService.createEnquiryHtml(user, targetOrganization, enquiry);
        } catch (RuntimeException ex) {
            logger.debug("Problem in making create user email html: {}", ex.getMessage());
        }
        if (!StringUtils.isEmpty(createEnquiryHtml)) {
            String subject = messageSource.getMessage(MESSAGE_ENQUIRY_SUBJECT, null, "", locale);
            if(enquiry != null)
                subject = StringUtils.isEmpty(enquiry.getSubject()) ? subject : enquiry.getSubject();
            sendToUserAndOrganization(targetUser, targetOrganization, subject, createEnquiryHtml);
        }
    }


    @Override
    @Async
    public void send(EmailPostDto emailPostDto) {
        EmailDto emailDto = new EmailDto(emailPostDto.getSubject(), emailPostDto.getTo(), getFrom(null, null), emailPostDto.getBody(), null, null, emailPostDto.getReplyTo());
        sendHtmlMail(new EmailsDto(Arrays.asList(emailDto), getMailSender(null)));
    }


}
