package co.indexify.service.impl;

import co.indexify.domain.DomainConstants;
import co.indexify.domain.model.*;
import co.indexify.domain.model.PasswordResetToken;
import co.indexify.domain.repository.IRoleRepository;
import co.indexify.domain.repository.IUserRepository;
import co.indexify.dto.CatalogueStatusDto;
import co.indexify.dto.DeviceDto;
import co.indexify.dto.RegistrationDto;
import co.indexify.dto.UserPutDto;
import co.indexify.exception.ExistException;
import co.indexify.security.CustomUserDetails;
import co.indexify.security.CustomUserDetailsService;
import co.indexify.service.IOrganizationService;
import co.indexify.service.ISmsService;
import co.indexify.service.IUserService;
import com.google.common.collect.Sets;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.stream.Stream;

@Service("userService")
public class UserService extends AbstractDomainService<User, Long> implements IUserService {

    @Autowired
    private ISmsService smsService;


    @Autowired
    private IUserRepository userRepository;


    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private IRoleRepository roleRepository;

    @Autowired
    private MessageSource messageSource;

    //@Autowired
    //private SessionRegistry sessionRegistry;

    public static final String TOKEN_INVALID = "invalidToken";
    public static final String TOKEN_EXPIRED = "expired";
    public static final String TOKEN_VALID = "valid";

    public static String QR_PREFIX = "https://chart.googleapis.com/chart?chs=200x200&chld=M%%7C0&cht=qr&chl=";
    public static String APP_NAME = "SpringRegistration";

    // API

    @Override
    public User registerNewUserAccount(final RegistrationDto accountDto) {
        String message = null;
        if (emailExist(accountDto.getEmail())) {
            throw new ExistException("email", accountDto.getEmail());
        }
        if(contacExist(accountDto.getContact())){
            throw new ExistException("contact",accountDto.getContact());
        }
        final User user = new User();

        user.setFirstName(accountDto.getFirstName());
        user.setLastName(accountDto.getLastName());
        user.setPassword(passwordEncoder.encode(accountDto.getPassword()));
        user.setEmail(new Email(accountDto.getEmail(), Email.EMAIL_PRIMARY));
        user.setRoles(Sets.newHashSet(Role.ROLE_ADMIN));
        user.setEnabled(false);
        user.setCompany(accountDto.getCompany());
        if(accountDto.getContact() !=  null){
            user.setContact(new Contact(accountDto.getContact(), Contact.CONTACT_PRIMARY));
        }
        if(accountDto.getTag()!= null)
            user.addTag(accountDto.getTag());
        return create(user);
    }

    private boolean contacExist(String contact) {
        return userRepository.findByContact(contact).isPresent();
    }

    @Override
    public User getUserByVerificationToken(final String verificationToken) {
        Optional<User> opUser = userRepository.findByVerificationToken(verificationToken);
        if (opUser.isPresent()) {
            return opUser.get();
        }
        return null;
    }

    @Override
    public VerificationToken getVerificationToken(final String verificationToken) {
        Optional<User> opUser = userRepository.findByVerificationToken(verificationToken);
        if (opUser.isPresent()) {
            return opUser.get().getVerificationToken();
        }
        return null;
    }


    @Override
    public User create(final User user) {
        return userRepository.persist(user);
    }







    @Override
    public void createOto(final User user) {
        String token = String.valueOf(new RandomDataGenerator().nextLong(100000, 999999));
        final Otp otp = new Otp(token,user);
        userRepository.save(user);
    }




    @Override
    public void createVerificationTokenForUser(final User user, final String token) {
        final VerificationToken myToken = new VerificationToken(token, user);
        userRepository.save(user);
    }

    @Override
    public User generateNewVerificationToken(final String existingVerificationToken) {
        User user = getUserByVerificationToken(existingVerificationToken);
        if(user != null){
            VerificationToken verificationToken = user.getVerificationToken();
            if(verificationToken != null){
                verificationToken.updateToken(UUID.randomUUID().toString());
                userRepository.save(user);
                return user;
            }
            return null;
        }
        return null;
    }

    @Override
    public void createPasswordResetTokenForUser(final User user, final String token) {
        final PasswordResetToken passwordResetToken = new PasswordResetToken(token, user);
        userRepository.save(user);
    }

    @Override
    public Optional<User> findUserByEmail(final String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public PasswordResetToken getPasswordResetToken(final String passwordResetToken) {
        Optional<User> opUser = userRepository.findByPasswordResetToken(passwordResetToken);
        if(opUser.isPresent())
            return opUser.get().getPasswordResetToken();
        return null;
    }

    @Override
    public Optional<User> getUserByPasswordResetToken(final String token) {
        return userRepository.findByPasswordResetToken(token);
    }


    @Override
    protected MongoRepository<User, Long> getDao() {
        return userRepository;
    }

    @Override
    public void changeUserPassword(final User user, final String password) {
        user.setPassword(passwordEncoder.encode(password));
        userRepository.save(user);
    }

    @Override
    public boolean checkIfValidOldPassword(final User user, final String oldPassword) {
        return passwordEncoder.matches(oldPassword, user.getPassword());
    }

    @Override
    public VerificationToken.VerificationTokenStatus validateVerificationToken(String token) {
        final User user = getUserByVerificationToken(token);
        if (user == null) {
            return VerificationToken.VerificationTokenStatus.INVALID;
        }
        final VerificationToken verificationToken = user.getVerificationToken();
        if (verificationToken == null) {
            return VerificationToken.VerificationTokenStatus.INVALID;
        }

        final Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            // deleteVerificationToken(user);
            return VerificationToken.VerificationTokenStatus.EXPIRED;
        }

        user.setEnabled(true);
        // tokenRepository.delete(verificationToken);
        userRepository.save(user);
        return VerificationToken.VerificationTokenStatus.VALID;
    }

    public void deleteVerificationToken(User user){
        user.setVerificationToken(null);
        userRepository.save(user);
    }


    @Override
    public String generateQRUrl(User user) throws UnsupportedEncodingException {
        // return QR_PREFIX + URLEncoder.encode(String.format("otpauth://totp/%s:%s?secret=%s&issuer=%s", APP_NAME, user.getEmail(), user.getSecret(), APP_NAME), "UTF-8");
        return null;
    }

    @Override
    public User updateUser2FA(boolean use2FA) {
        final Authentication curAuth = SecurityContextHolder.getContext().getAuthentication();
        User currentUser = (User) curAuth.getPrincipal();
        currentUser.setUsing2FA(use2FA);
        currentUser = userRepository.save(currentUser);
        final Authentication auth = new UsernamePasswordAuthenticationToken(currentUser, currentUser.getPassword(), curAuth.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);
        return currentUser;
    }

    private boolean emailExist(final String email) {
        return userRepository.findByEmail(email).isPresent();
    }

    @Override
    public List<String> getUsersFromSessionRegistry() {
        // return sessionRegistry.getAllPrincipals().stream().filter((u) -> !sessionRegistry.getAllSessions(u, false).isEmpty()).map(Object::toString).collect(Collectors.toList());
        return null;
    }


    @Override
    public PasswordResetToken.PasswordResetTokenStatus validatePasswordResetToken(String id, String token) {
        final Optional<User> userOpt = userRepository.findByPasswordResetToken(token);
        if(userOpt.isPresent()){
            if(!userOpt.get().getId().equals(id)){
                return PasswordResetToken.PasswordResetTokenStatus.INVALID;
            }
            final Calendar cal = Calendar.getInstance();
            if ((userOpt.get().getPasswordResetToken().getExpiryDate()
                    .getTime() - cal.getTime()
                    .getTime()) <= 0) {
                return PasswordResetToken.PasswordResetTokenStatus.EXPIRED;
            }
            return PasswordResetToken.PasswordResetTokenStatus.VALID;
        }
        else{
            return PasswordResetToken.PasswordResetTokenStatus.INVALID;
        }
    }


    @Override
    public Otp.OtpStatus validateLoginOtp(User user, String otp) {
        if(user!= null && user.getOtp()!= null){
            if(user.getOtp().getValue()!= null && !user.getOtp().getValue().equals(otp)){
                return Otp.OtpStatus.INVALID;
            }
            final Calendar cal = Calendar.getInstance();
            if ((user.getOtp().getExpiryDate()
                    .getTime() - cal.getTime()
                    .getTime()) <= 0) {
                return Otp.OtpStatus.INVALID;
            }
            return Otp.OtpStatus.VALID;
        }
        else{
            return Otp.OtpStatus.INVALID;
        }
    }


    @Override
    public PasswordResetToken.PasswordResetTokenStatus validatePasswordResetToken(CustomUserDetails userDetails, String id, String token) {
        User user = userDetails.getUser();
        if(user!= null){
            if(user.getId().equals(id)){
                return PasswordResetToken.PasswordResetTokenStatus.INVALID;
            }
            final Calendar cal = Calendar.getInstance();
            if ((user.getPasswordResetToken().getExpiryDate()
                    .getTime() - cal.getTime()
                    .getTime()) <= 0) {
                return PasswordResetToken.PasswordResetTokenStatus.EXPIRED;
            }
            return PasswordResetToken.PasswordResetTokenStatus.VALID;
        }
        else{
            return PasswordResetToken.PasswordResetTokenStatus.INVALID;
        }
    }


    @Autowired
    private CustomUserDetailsService userDetailsService;




    @Override
    public Optional<User> findByCandidateKey(String candidateKey) {
        return userRepository.findByCandidateKey(candidateKey);
    }

    @Override
    public Optional<User> findByPasswordResetToken(String token) {
        return userRepository.findByPasswordResetToken(token);
    }

    @Override
    public void deletePasswordResetAllExpiredSince(Date now) {
        userRepository.deletePasswordResetAllExpiredSince(now);
    }

    @Override
    public void deleteVerificationTokenVerified() {
        userRepository.deleteVerificationTokenVerified();
    }

    @Override
    public Optional<User> findByContact(String contact) {
        return userRepository.findByContact(contact);
    }

    @Override
    public void updateCatalogueStatus(User user, CatalogueStatusDto dto) {
        CatalogueStatus catalogueStatus = new CatalogueStatus(dto);
        user.setCatalogueStatus(catalogueStatus);
        update(user);
    }

    @Override
    public void addDevice(User user, DeviceDto dto) {
        Device device = new Device(dto);
        user.addDevice(device);
        update(user);
    }

    @Override
    public void removeDevice(User user, DeviceDto dto) {
        Device device = new Device(dto);
        removeDevice(user, device);
    }


    @Override
    public void removeDevice(User user, Device device) {
        user.removeDevice(device);
        update(user);
    }

    @Override
    public void update(User user, UserPutDto dto) {
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
       // user.setContact(new Contact(dto.getContact()));
       // user.setEmail(new Email(dto.getEmail()));
        user.setLocation(Location.fromDto(dto.getLocation()));
        update(user);
    }


    private RuntimeException translateDuplicateKeyException(DuplicateKeyException ex, RegistrationDto userDto){
        if(ex.getMessage().contains(DomainConstants.USER_EMAIL_INDEX_NAME))
            return new ExistException("email", userDto.getEmail());
        else if(ex.getMessage().contains(DomainConstants.USER_CONTACT_INDEX_NAME))
            return new ExistException("contact", userDto.getContact());
        else if(ex.getMessage().contains(DomainConstants.USER_USERNAME_INDEX_NAME))
            return new ExistException("username", DomainConstants.USER_USERNAME_INDEX_NAME);
        return ex;
    }

    @Autowired
    private IOrganizationService organizationService;


    @Override
    public void addTag(User user, String tag) {
        user.addTag(tag);
        update(user);
        /*
        if(user.getOrganization() != null){
            organizationService.addTag(user.getOrganization(), tag);
        } */

    }


    @Override
    public Stream<User> findByOrganizationId(Long organizationId) {
        return userRepository.findByOrganizationId(organizationId);
    }
}
