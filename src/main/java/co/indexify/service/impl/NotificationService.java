package co.indexify.service.impl;

import co.indexify.domain.model.Notification;
import co.indexify.domain.repository.INotificationRepository;
import co.indexify.service.INotificationService;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

@Service
public class NotificationService extends AbstractDomainService<Notification, String> implements INotificationService {

    private INotificationRepository notificationRepository;

    public NotificationService(INotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    @Override
    protected MongoRepository<Notification, String> getDao() {
        return notificationRepository;
    }

    @Override
    public void create(Notification notification, Long userId) {
        notificationRepository.persist(notification, userId);
    }
}
