package co.indexify.service.impl;

import co.indexify.domain.model.Role;
import co.indexify.domain.repository.IRoleRepository;
import co.indexify.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

@Service
public class RoleService extends AbstractDomainService<Role, String> implements IRoleService {

    @Autowired
    private IRoleRepository roleRepository;

    @Override
    public Role findByName(String name) {
        return roleRepository.findByName(name);
    }

    @Override
    protected MongoRepository<Role, String> getDao() {
        return roleRepository;
    }
}
