package co.indexify.service.impl;

import co.indexify.csv.dto.ProductCsvDto;
import co.indexify.domain.model.*;
import co.indexify.domain.model.Resource;
import co.indexify.domain.repository.IProductRepository;
import co.indexify.dto.*;
import co.indexify.exception.NotExistException;
import co.indexify.exception.NotFoundException;
import co.indexify.service.*;
import com.opencsv.bean.CsvToBeanBuilder;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ProductService extends AbstractDomainService<Product, String> implements IProductService {

    private static final Logger log =  LoggerFactory.getLogger(ProductService.class);

    @Autowired
    private IProductRepository productRepository;


    @Autowired
    private IUserService userService;


    @Autowired
    private IResourceService resourceService;


    @Autowired
    private INodeService nodeService;


    @Override
    public Product create(ProductCreateDto dto, User user) {
        if(user.getOrganization() != null) {
            Product product = Product.fromCreateDto(dto);
            if (dto.getImages() != null && !dto.getImages().isEmpty()) {
                Resource resource = null;
                for (MultipartFile file : dto.getImages()) {
                    ResourceDto resourceDto = new ResourceDto()
                            .assignFile(file)
                            .tag(dto.getDisplayName())
                            .image()
                            .organization(user.getOrganization());
                    resource = resourceService.create(resourceService.upload(resourceDto));
                    if(resource != null && resource.getUrl()!= null)
                        product.addImageUrls(resource.getUrl());
                }
            }
            
            return create(product);
        }else {
            throw new NotExistException(Organization.class.getSimpleName(), ResponseCode.ORGANIZATION_NOT_EXIST);
        }
    }

    @Override
    public Product update(String id, ProductUpdateDto dto, User user) {
    	
        if (user.getOrganization() != null) {
            Optional<Product> productOpt = findById(id);
            if (productOpt.isPresent()) {
                Product product = productOpt.get();
                product.updateFromUpdateDto(dto);
                if (dto.getImages() != null && !dto.getImages().isEmpty()) {
                    Resource resource = null;
                    List<String> temp = new ArrayList<>();
                    for (MultipartFile file : dto.getImages()) {
                        ResourceDto resourceDto = new ResourceDto()
                                .assignFile(file)
                                .tag(dto.getDisplayName())
                                .image()
                                .organization(user.getOrganization());
                        resource = resourceService.create(resourceService.upload(resourceDto));
                        if(resource != null && resource.getUrl()!= null) {
                        	temp.add(resource.getUrl());
                        }
                            
                    }
                    List<String> newList = Stream.concat(temp.stream(), product.getImgUrls().stream())
                    .collect(Collectors.toList());
                    product.setImgUrls(newList);
                }
                update(product);
                return product;
            } else {
                throw new NotFoundException(Product.class.getSimpleName(), ResponseCode.PRODUCT_NOT_FOUND);
            }
        } else
            throw new NotExistException(Organization.class.getSimpleName(), ResponseCode.ORGANIZATION_NOT_EXIST);

    }

    @Override
    public void linkToNode(ProductNodeLinkDto dto, User user) {
        if (user.getOrganization() != null) {
            for (ProductNodeDto produtNode : dto.getLinks()) {
                Optional<Product> productOpt = findById(produtNode.getId());
                if (productOpt.isPresent()) {
                    Product product = productOpt.get();
                    Optional<Node> nodeOpt = nodeService.findByIdLeaf(user.getOrganization().getId(), produtNode.getLeaf());
                    if (nodeOpt.isPresent()) {
                        Node node = nodeOpt.get();
                        product.setLeaf(node);
                        update(product);
                    }
                }
            }

        } else
            throw new NotExistException(Organization.class.getSimpleName(), ResponseCode.ORGANIZATION_NOT_EXIST);

    }
    
    @Override
    public void unlinkToNode(ProductNodeUnLinkDto dto, User user) {
        if (user.getOrganization() != null) {
            for (ProductNodeDto produtNode : dto.getLinks()) {
                Optional<Product> productOpt = findById(produtNode.getId());
                if (productOpt.isPresent()) {
                    Product product = productOpt.get();
                        product.setLeaf(null);
                        update(product);
                 }
            }

        } else
            throw new NotExistException(Organization.class.getSimpleName(), ResponseCode.ORGANIZATION_NOT_EXIST);

    }

    @Override
    public void unassignLeaf(String leafId) {
        productRepository.unassignLeaf(leafId);
    }

    @Override
    public void createByCsv(ProductCreateCsvDto dto, User user) {

        ResourceDto resourceDto = new ResourceDto()
                .assignFile(dto.getCsv())
                .tag(dto.getTag())
                .image()
                .organization(user.getOrganization());
        Resource resource = resourceService.create(resourceService.upload(resourceDto));
        if(resource != null){
            try {
                File temp = File.createTempFile("temp", "csv");
                dto.getCsv().transferTo(temp);
                FileReader tempReader = new FileReader(temp);
                List<ProductCsvDto> productCsvDtoList = new CsvToBeanBuilder<ProductCsvDto>(tempReader)
                        .withType(ProductCsvDto.class).build().parse();
                if(CollectionUtils.isNotEmpty(productCsvDtoList)){
                    List<Product> products = new ArrayList<>();
                    productCsvDtoList.forEach(productDto -> {
                        Product product = new Product();
                        product.setName(productDto.getName());
                        product.setControls(productDto.fieldsAsList());
                        products.add(product);
                    });
                    if(CollectionUtils.isNotEmpty(products)){
                        productRepository.saveAll(products);
                    }
                }
                tempReader.close();
                temp.delete();
            }catch (Exception ex){
                ex.printStackTrace();
                throw new RuntimeException(ex);
            }
        }
    }


    @Override
    protected MongoRepository<Product, String> getDao() {
        return productRepository;
    }
}
