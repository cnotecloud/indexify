package co.indexify.service.impl;

import co.indexify.domain.model.Enquiry;
import co.indexify.domain.model.Organization;
import co.indexify.domain.model.Product;
import co.indexify.domain.model.User;
import co.indexify.service.IEmailTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Calendar;

@Service
public class EmailTemplateService implements IEmailTemplateService {

    //Autowiring
    @Autowired private TemplateEngine templateEngine;
    @Autowired private Environment environment;
    //Autowiring

    //Params
    public static final String MODEL_PARAM_TOKEN="token";
    public static final String MODEL_PARAM_LINK = "link";
    public static final String MODEL_PARAM_USER = "user";
    public static final String MODEL_PARAM_NOW = "now";
    public static final String MODEL_PARAM_ORGANIZATION = "organization";
    public static final String MODEL_PARAM_PRODUCT = "product";
    public static final String MODEL_PARAM_ID="id";
    public static final String MODEL_PARAM_OTP = "otp";
    private static final String MODEL_PARAM_ENQUIRY = "enquiry";
    //Params


    //Property
    public static final String PROPERTY_REGISTRATION_URL = "user.registration.confirm.url";
    public static final String PROPERTY_FORGET_PASSWORD_URL="user.registration.forgetPassword.url";
    //Property

    //Templates
    public static final String TEMPLATE_REGISTRATION="registration";
    public static final String TEMPLATE_FORGET_PASSWORD="forget-password";
    public static final String TEMPLATE_CREATE_USER="create-user";
    public static final String TEMPLATE_FAVOURITE_PRODUCT = "favourite-product";
    public static final String TEMPLATE_LOGIN_OTP = "login-otp";
    public static final String TEMPLATE_NEW_USER="new-user";
    private static final String TEMPLATE_ORGANIZATION_SCAN = "organization-scan";
    private static final String TEMPLATE_ENQUIRY = "enquiry";

    //Templates






    @Override
    public String createRegistrationHtml(User user, String tag) {
        Context context = new Context();
        context.setVariable(MODEL_PARAM_TOKEN, user.getVerificationToken().getValue());
        context.setVariable(MODEL_PARAM_LINK, environment.getProperty(PROPERTY_REGISTRATION_URL));
        String emailText = templateEngine.process(TEMPLATE_REGISTRATION, context);
        return emailText;
    }

    @Override
    public String createCreateUserEmailHtml(User user){
        Context context = new Context();
        context.setVariable(MODEL_PARAM_NOW, Calendar.getInstance().getTime());
        context.setVariable(MODEL_PARAM_USER, user);
        String emailText = templateEngine.process(TEMPLATE_CREATE_USER, context);
        return emailText;
    }


    @Override
    public String createFavouriteProductScanHtml(User user, Organization organization, Product product) {
        Context context = new Context();
        context.setVariable(MODEL_PARAM_USER, user);
        context.setVariable(MODEL_PARAM_ORGANIZATION, organization);
        context.setVariable(MODEL_PARAM_PRODUCT, product);
        String emailText = templateEngine.process(TEMPLATE_FAVOURITE_PRODUCT, context);
        return emailText;
    }

    @Override
    public String createForgetPasswordHtml(User user) {
        Context context = new Context();
        context.setVariable(MODEL_PARAM_TOKEN, user.getPasswordResetToken().getValue());
        context.setVariable(MODEL_PARAM_ID, user.getId());
        context.setVariable(MODEL_PARAM_LINK, environment.getProperty(PROPERTY_FORGET_PASSWORD_URL));
        String emailText = templateEngine.process(TEMPLATE_FORGET_PASSWORD, context);
        return emailText;
    }


    @Override
    public String createLoginOtpHtml(User user) {
        Context context = new Context();
        context.setVariable(MODEL_PARAM_OTP, user.getOtp().getValue());
        String emailText = templateEngine.process(TEMPLATE_LOGIN_OTP, context);
        return emailText;
    }


    @Override
    public String createNewUserHtml(User user) {
        Context context = new Context();
        context.setVariable(MODEL_PARAM_USER, user);
        String emailText = templateEngine.process(TEMPLATE_NEW_USER, context);
        return emailText;
    }


    @Override
    public String createOrganizationScanHtml(User user, Organization organization){
        Context context = new Context();
        context.setVariable(MODEL_PARAM_USER, user);
        context.setVariable(MODEL_PARAM_ORGANIZATION, organization);
        String emailText = templateEngine.process(TEMPLATE_ORGANIZATION_SCAN, context);
        return emailText;
    }

    @Override
    public String createEnquiryHtml(User user, Organization organization, Enquiry enquiry) {
        Context context = new Context();
        context.setVariable(MODEL_PARAM_USER, user);
        context.setVariable(MODEL_PARAM_ORGANIZATION, organization);
        context.setVariable(MODEL_PARAM_ENQUIRY, enquiry);
        String emailText = templateEngine.process(TEMPLATE_ENQUIRY, context);
        return emailText;
    }


}
