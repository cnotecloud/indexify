package co.indexify.service.impl;

import co.indexify.domain.model.OrganizationScan;
import co.indexify.domain.model.UserVisitor;
import co.indexify.domain.repository.IOrganizationScanRepository;
import co.indexify.domain.repository.IUserVisitorRepository;
import co.indexify.service.IOrganizationScanService;
import co.indexify.service.IUserVisitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

@Service
public class UserVisitorService extends AbstractDomainService<UserVisitor, String> implements IUserVisitorService {

    @Autowired
    private IUserVisitorRepository userVisitorRepository;

    @Override
    public UserVisitor create(UserVisitor userVisitor, Long userId) {
       return userVisitorRepository.persist(userVisitor, userId);
    }

    @Override
    protected MongoRepository<UserVisitor, String> getDao() {
        return userVisitorRepository;
    }

}
