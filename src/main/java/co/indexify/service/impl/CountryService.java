package co.indexify.service.impl;

import co.indexify.domain.model.Country;
import co.indexify.domain.repository.ICountryRepository;
import co.indexify.service.ICountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CountryService extends AbstractDomainService<Country, String> implements ICountryService {

    @Autowired
    private ICountryRepository countryRepository;

    @Override
    public Optional<Country> findByIso2(String iso2) {
        return countryRepository.findByIso2IgnoreCase(iso2);
    }

    @Override
    protected MongoRepository<Country, String> getDao() {
        return countryRepository;
    }
}
