package co.indexify.service.impl;

import co.indexify.domain.model.Node;
import co.indexify.domain.model.Organization;
import co.indexify.domain.model.User;
import co.indexify.domain.repository.INodeRepository;
import co.indexify.dto.*;
import co.indexify.exception.ExistException;
import co.indexify.exception.NotExistException;
import co.indexify.service.INodeService;
import co.indexify.service.IProductService;
import co.indexify.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class NodeService extends AbstractDomainService<Node, String> implements INodeService {
    @Autowired
    private INodeRepository nodeRepository;

    @Autowired
    private IUserService userService;

    @Autowired
    private IProductService productService;


    @Override
    public void saveTree(NodesDto nodesDto, User user) {
        if (user.getOrganization() != null) {
            long count = nodeRepository.count();
            if (count <= 0) {
                createTree(user, nodesDto.getNodes());
            } else {
                //deleteTree(user);
                throw new ExistException(String.format("%s Tree", Node.class.getSimpleName()), ResponseCode.NODE_TREE_EXIST);
            }
        } else
            throw new NotExistException(Organization.class.getSimpleName(), ResponseCode.ORGANIZATION_NOT_EXIST);

    }

    @Override
    public Node create(Node node) {
        return nodeRepository.persist(node);
    }

    @Override
    protected MongoRepository<Node, String> getDao() {
        return nodeRepository;
    }

    @Override
    public Optional<Node> findByIdLeaf(Long organizationId, String id) {
        return nodeRepository.findByIdAndLeafTrue(id);
    }

    @Override
    public void deleteNode(String id) {
        Optional<Node> node = nodeRepository.findById(id);
        node.ifPresent(this::deleteNode);
    }
    @Override
    public void deleteAllStructure(List<String> nodeListDto) {
    	for(String id:nodeListDto) {
    	deleteNode(id);	
    	}
    }

    private void deleteNode(Node node) {
        if (node!= null) {
            deleteLeaf(node.getId());
            productService.unassignLeaf(node.getId());
            Page<Node> page = null;
            int i = 0;
            do {
                page = nodeRepository.findByParentId(node.getId(), null);
                page.forEach(this::deleteNode);
                i++;
            } while (i < page.getTotalPages());
        }
    }
    
    @Override
    public void deleteLeaf(String id) {
            nodeRepository.deleteById(id);
    }


    @Override
    public void add(NodeDto dto) {
        if (dto.getParent() == null) {
            nodeRepository.save(Node.fromDto(dto));
        } else {
            Optional<Node> parent = nodeRepository.findById(dto.getParent());
            if (parent.isPresent()) {
                if (parent.get().getLeaf()) {
                    productService.unassignLeaf(parent.get().getId());
                    nodeRepository.makeNonLeaf(parent.get());
                }
                nodeRepository.save(Node.fromDto(dto).assignParent(parent.get()));
            }
        }
    }

    @Override
    public void edit(String id, NodeUpdateDto nodeDto) {
    	Optional<Node> node = nodeRepository.findById(id);
        if(nodeDto.getLeaf()) {
        	Page<Node> isNodeExist = nodeRepository.findByParentId(id, null);
        	if(nodeDto.getLeaf()!=node.get().getLeaf()&& isNodeExist != null && isNodeExist.getNumberOfElements()!=0) {
        		throw new ExistException(String.format("%s Tree", Node.class.getSimpleName()), ResponseCode.NODE_TREE_EXIST);
        	}else {
        		if (node.isPresent()) {
                	node.get().setDisplayName(nodeDto.getDisplayName());
                	node.get().setLeaf(nodeDto.getLeaf());
                    if (nodeDto.getImgUrl() != null) {
                        node.get().addImgUrl(nodeDto.getImgUrl());
                    }
                    nodeRepository.save(node.get());
                }
        	}
        }else {
        	productService.unassignLeaf(id);
        	if (node.isPresent()) {
            	node.get().setDisplayName(nodeDto.getDisplayName());
            	node.get().setLeaf(nodeDto.getLeaf());
                if (nodeDto.getImgUrl() != null) {
                    node.get().addImgUrl(nodeDto.getImgUrl());
                }
                nodeRepository.save(node.get());
            }
        }
    }

    private void deleteTree(User user) {

    }


    private void createTree(User user, List<NodeDto> nodes) {
        Map<String, Node> nodesCreated = new HashMap<>();
        Deque<NodeDto> nodesQueue = new ArrayDeque<>();
        nodesQueue.addAll(nodes);
        while (!nodesQueue.isEmpty()) {
            NodeDto dto = nodesQueue.pollFirst();
            if (dto.getParent() == null || nodesCreated.get(dto.getParent()) != null) {
                Node node = null;
                if (dto.getParent() == null) {
                    node = Node.fromDto(dto);
                    node = create(node);
                } else if (nodesCreated.get(dto.getParent()) != null) {
                    node = Node.fromDto(dto).assignParent(nodesCreated.get(dto.getParent()));
                    node = create(node);
                } else {
                    nodesQueue.addLast(dto);
                }
                nodesCreated.put(node.getId(), node);
            } else {
                nodesQueue.addLast(dto);
            }
        }
    }
}
