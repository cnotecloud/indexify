package co.indexify.service.impl;

import co.indexify.domain.model.Country;
import co.indexify.service.ICountryService;
import co.indexify.service.IIpToCountryService;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.CityResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.InetAddress;
import java.util.Optional;


@Service
public class IpToCountryService implements IIpToCountryService {

    @Autowired
    private ICountryService countryService;

    @Autowired
    private DatabaseReader databaseReader;


    @Override
    public Optional<Country> convert(String ip) {
        try {
            InetAddress ipAddress = InetAddress.getByName(ip);
            CityResponse response = databaseReader.city(ipAddress);
            com.maxmind.geoip2.record.Country country = response.getCountry();
            return countryService.findByIso2(response.getCountry().getIsoCode());
        }catch (Exception ex){
            return Optional.empty();
        }
    }
}
