package co.indexify.service.impl;

import co.indexify.domain.model.Organization;
import co.indexify.domain.model.Resource;
import co.indexify.domain.model.User;
import co.indexify.domain.repository.IResourceRepository;
import co.indexify.dto.*;
import co.indexify.exception.NotExistException;
import co.indexify.service.IFileStorageService;
import co.indexify.service.IResourceService;
import co.indexify.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ResourceService extends AbstractDomainService<Resource, String> implements IResourceService {


    @Autowired
    private IFileStorageService fileStorageService;

    @Autowired
    private IResourceRepository resourceRepository;

    @Autowired
    private IUserService userService;

    @Override
    public Resource upload(ResourceDto dto) {
        String fileUrl = fileStorageService.uploadFileToS3(String.valueOf(dto.getOrganization().getId()), dto.getType(), dto.getFile());
        return new Resource(fileUrl, dto.getType(), dto.getTag());
    }

    @Override
    public void addTag(Organization organization, String id, String tag) {
        resourceRepository.findById(id)
                .ifPresent((resource) -> {
                    resource.addTag(tag);
                    update(resource);
                });

    }


    @Override
    public void removeTag(Organization organization, String id, String tag) {
        resourceRepository.findById(id)
                .ifPresent((resource) -> {
                    resource.removeTag(tag);
                    update(resource);
                });

    }


    @Override
    public void upload(ResourcesUploadDto dto, User user) {
        if(user.getOrganization()!= null){
            for(ResourceUploadDto resourcesUploadDto: dto.getResources()){
                ResourceDto resourceDto = ResourceDto.fromUploadDto(resourcesUploadDto)
                        .organization(user.getOrganization());
                create(upload(resourceDto));
            }
        }else
            throw new NotExistException(Organization.class.getSimpleName(), ResponseCode.ORGANIZATION_NOT_EXIST);
    }

    @Override
    protected MongoRepository<Resource, String> getDao() {
        return resourceRepository;
    }
}
