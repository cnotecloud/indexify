package co.indexify.service.impl;

import co.indexify.domain.model.Domain;
import co.indexify.service.IDomainService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public abstract class AbstractDomainService<T extends Domain<ID>, ID extends Serializable> implements IDomainService<T, ID> {
    @Override
    public Optional<T> findById(final ID id) {
        return getDao().findById(id);
    }
    @Override
    public List<T> findAll() {
        return getDao().findAll();
    }

    @Override
    public Page<T> findPaginated(final Pageable pageable) {
        return getDao().findAll(pageable);
    }

    @Override
    public T create(final T entity) {
        return getDao().insert(entity);
    }

    @Override
    public T update(final T entity) {
        return getDao().save(entity);
    }

    @Override
    public void delete(final T entity) {
        getDao().delete(entity);
    }

    @Override
    public void deleteById(ID id) {
        getDao().deleteById(id);
    }

    protected abstract MongoRepository<T, ID> getDao();

}
