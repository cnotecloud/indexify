package co.indexify.service.impl;

import co.indexify.domain.model.OrganizationScan;
import co.indexify.domain.model.ReverseEnquiry;
import co.indexify.domain.repository.IOrganizationScanRepository;
import co.indexify.domain.repository.IReverseEnquiryRepository;
import co.indexify.service.IOrganizationScanService;
import co.indexify.service.IReverseEnquiryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

@Service
public class ReverseEnquiryService extends AbstractDomainService<ReverseEnquiry, String> implements IReverseEnquiryService {

    @Autowired
    private IReverseEnquiryRepository reverseEnquiryRepository;

    @Override
    public ReverseEnquiry create(ReverseEnquiry reverseEnquiry) {
       return reverseEnquiryRepository.persist(reverseEnquiry);
    }



    @Override
    protected MongoRepository<ReverseEnquiry, String> getDao() {
        return reverseEnquiryRepository;
    }

}
