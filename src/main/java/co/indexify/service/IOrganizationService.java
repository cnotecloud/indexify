package co.indexify.service;

import co.indexify.domain.model.Organization;
import co.indexify.domain.model.User;
import co.indexify.dto.CalalogueDto;
import co.indexify.dto.OrganizationCreateDto;
import co.indexify.dto.OrganizationUpdateDto;

import javax.validation.Valid;

public interface IOrganizationService extends IDomainService<Organization, Long> {
    Organization registerNew(OrganizationCreateDto organizationCreateDto, User user);
    void addTag(Organization organization, String tag);
    void updateCatalogue(Organization organization, CalalogueDto dto);

    void addCatalogue(Organization organization, @Valid CalalogueDto dto);

    void update(Organization organization, OrganizationUpdateDto organizationUpdateDto);
}
