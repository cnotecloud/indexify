package co.indexify.service;

import co.indexify.domain.model.Enquiry;
import co.indexify.domain.model.Organization;
import co.indexify.domain.model.Product;
import co.indexify.domain.model.User;

public interface IEmailTemplateService {
    String createRegistrationHtml(User user, String tag);

    String createCreateUserEmailHtml(User user);

    String createFavouriteProductScanHtml(User user, Organization organization, Product product);

    String createForgetPasswordHtml(User user);

    String createLoginOtpHtml(User user);

    String createNewUserHtml(User user);

    String createOrganizationScanHtml(User user, Organization organization);

    String createEnquiryHtml(User user, Organization targetOrganization, Enquiry enquiry);
}
