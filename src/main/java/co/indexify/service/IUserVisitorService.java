package co.indexify.service;

import co.indexify.domain.model.OrganizationScan;
import co.indexify.domain.model.UserVisitor;

public interface IUserVisitorService extends IDomainService<UserVisitor, String> {
    UserVisitor create(UserVisitor userVisitor, Long userId);
}
