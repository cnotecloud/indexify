package co.indexify.service;

import co.indexify.domain.model.Node;
import co.indexify.domain.model.User;
import co.indexify.dto.NodeDto;
import co.indexify.dto.NodeListDto;
import co.indexify.dto.NodeUpdateDto;
import co.indexify.dto.NodesDto;

import java.util.List;
import java.util.Optional;

public interface INodeService extends IDomainService<Node, String> {

    void saveTree(NodesDto nodesDto, User user);

    Optional<Node> findByIdLeaf(Long organizationId, String id);

    void deleteLeaf(String id);

    void add(NodeDto dto);

    void edit(String id, NodeUpdateDto nodeDto);

    void deleteNode(String id);
    void deleteAllStructure(List<String> nodeListDto);
}
