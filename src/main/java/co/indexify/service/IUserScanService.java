package co.indexify.service;

import co.indexify.domain.model.User;
import co.indexify.domain.model.UserScan;
import co.indexify.dto.UserScanDto;

import java.util.Locale;

public interface IUserScanService extends IDomainService<UserScan, String> {
    void createNew(UserScanDto dto, User user, Locale locale);
}
