package co.indexify.service;

import co.indexify.domain.model.Organization;
import co.indexify.domain.model.ReverseFavouriteProduct;
import co.indexify.dto.FavouriteProductDto;
import co.indexify.dto.OrganizationIdNameDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IReverseFavouriteProductService extends IDomainService<ReverseFavouriteProduct, String> {
    Page<OrganizationIdNameDto> getOrganizationIdName(Pageable pageable);
    ReverseFavouriteProduct create(ReverseFavouriteProduct reverseFavouriteProduct, Long organizationId);
}
