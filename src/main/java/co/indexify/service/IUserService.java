package co.indexify.service;

import co.indexify.domain.model.*;
import co.indexify.dto.CatalogueStatusDto;
import co.indexify.dto.DeviceDto;
import co.indexify.dto.RegistrationDto;
import co.indexify.dto.UserPutDto;
import co.indexify.security.CustomUserDetails;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.io.UnsupportedEncodingException;
import java.util.stream.Stream;

public interface IUserService extends IDomainService<User, Long> {

    Otp.OtpStatus validateLoginOtp(User user, String otp);


    Stream<User> findByOrganizationId(Long organizationId);



    void createOto(final User user);

    void addTag(User user, String tag);

    User registerNewUserAccount(RegistrationDto accountDto);

    User getUserByVerificationToken(String verificationToken);


    void createVerificationTokenForUser(User user, String token);

    VerificationToken getVerificationToken(String VerificationToken);

    User generateNewVerificationToken(String token);

    void createPasswordResetTokenForUser(User user, String token);

    Optional<User> findUserByEmail(String email);

    PasswordResetToken getPasswordResetToken(String token);

    Optional<User> getUserByPasswordResetToken(String token);


    void deleteVerificationToken(User user);

    void changeUserPassword(User user, String password);

    boolean checkIfValidOldPassword(User user, String password);

    VerificationToken.VerificationTokenStatus validateVerificationToken(String token);

    String generateQRUrl(User user) throws UnsupportedEncodingException;

    User updateUser2FA(boolean use2FA);

    List<String> getUsersFromSessionRegistry();

    PasswordResetToken.PasswordResetTokenStatus validatePasswordResetToken(String id, String token);

    PasswordResetToken.PasswordResetTokenStatus validatePasswordResetToken(CustomUserDetails userDetails, String id, String token);


    Optional<User> findByCandidateKey(String candiadteKey);

    Optional<User> findByPasswordResetToken(String token);

    void deletePasswordResetAllExpiredSince(Date now);

    void deleteVerificationTokenVerified();


    Optional<User> findByContact(String contact);

    void updateCatalogueStatus(User user, CatalogueStatusDto dto);

    void addDevice(User user, DeviceDto dto);

    void removeDevice(User user, DeviceDto dto);

    void removeDevice(User user, Device device);

    void update(User user, UserPutDto dto);
}
