package co.indexify.service;

import co.indexify.domain.model.Role;

public interface IRoleService extends IDomainService<Role, String> {
    Role findByName(String name);
}
