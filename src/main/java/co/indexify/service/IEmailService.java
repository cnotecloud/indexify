package co.indexify.service;

import co.indexify.annotation.Local;
import co.indexify.domain.model.Enquiry;
import co.indexify.domain.model.Organization;
import co.indexify.domain.model.Product;
import co.indexify.domain.model.User;
import co.indexify.dto.EmailPostDto;

import javax.validation.Valid;
import java.util.Locale;
import java.util.Optional;

/**
 * Created by ankur on 29-05-2017.
 */
public interface IEmailService {
    void sendForRegistration(User user, String tag, Locale locale);
    void sendForForgetPassword(User user, Locale locale);
    void sendForLoginOtp(User user, Locale locale);
    void send(EmailPostDto emailPostDto);
    void sendForOrganizationScan(User user, Organization targetOrganization, User targetUser, Locale locale);
    void sendForFavouriteProductScan(User user, Organization targetOrgannization, User targetUser, Product product, Locale locale);
    void sendForEnquiry(User user, Organization targetOrganization, User targetUser, Enquiry enquiry, Locale locale);

}
