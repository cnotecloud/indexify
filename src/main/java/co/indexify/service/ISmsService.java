package co.indexify.service;

import co.indexify.domain.model.User;

import java.util.Locale;

public interface ISmsService {
    void sendLoginOtp(User user, Locale locale);
}
