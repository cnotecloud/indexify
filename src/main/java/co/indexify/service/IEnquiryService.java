package co.indexify.service;

import co.indexify.domain.model.Enquiry;
import co.indexify.domain.model.User;
import co.indexify.domain.model.UserScan;
import co.indexify.dto.EnquiryDto;
import co.indexify.dto.UserScanDto;

import java.util.Locale;

public interface IEnquiryService extends IDomainService<Enquiry, String> {
    void createNew(EnquiryDto dto, User user, Locale locale);
}
