package co.indexify.service;

import co.indexify.domain.model.Organization;
import co.indexify.domain.model.Resource;
import co.indexify.domain.model.User;
import co.indexify.dto.ResourceDto;
import co.indexify.dto.ResourcesUploadDto;

public interface IResourceService extends IDomainService<Resource, String>  {
    void upload(ResourcesUploadDto dto, User user);
    Resource upload(ResourceDto dto);

    void addTag(Organization organization, String id, String tag);

    void removeTag(Organization organization, String id, String tag);
}
