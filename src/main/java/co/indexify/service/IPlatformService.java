package co.indexify.service;

import co.indexify.domain.model.Node;
import co.indexify.domain.model.Platform;
import co.indexify.domain.model.User;
import co.indexify.dto.NodeDto;
import co.indexify.dto.NodeUpdateDto;
import co.indexify.dto.NodesDto;

import java.util.Optional;

public interface IPlatformService extends IDomainService<Platform, String> {


    Optional<Platform> findByType(String type);
}
