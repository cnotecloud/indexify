package co.indexify.service;

import co.indexify.domain.model.Notification;

public interface INotificationService extends IDomainService<Notification, String> {
    void create(Notification notification, Long userId);
}
