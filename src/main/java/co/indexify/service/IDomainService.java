package co.indexify.service;

import co.indexify.domain.model.Domain;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public interface IDomainService<T extends Domain<ID>, ID extends Serializable> {

    // read - one

    Optional<T> findById(ID id);

    // read - all

    List<T> findAll();

    Page<T> findPaginated(Pageable pageable);

    // write

    T create(T entity);

    T update(T entity);

    void delete(T entity);

    void deleteById(ID id);

}
