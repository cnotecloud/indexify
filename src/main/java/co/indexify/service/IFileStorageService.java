package co.indexify.service;

import co.indexify.domain.model.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface IFileStorageService {
    String uploadFileToS3(String directory, Resource.ResourceType type, MultipartFile file);
    /*void doesBucketObjectExists(String bucket, String dirctory);
    File downloadFileFromBucketObject(String bucket, String directory, String fileName);
    File downloadFileFromBucketObjectUsigUrl(String httpUrl);*/


}
