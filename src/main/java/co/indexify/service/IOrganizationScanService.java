package co.indexify.service;

import co.indexify.domain.model.OrganizationScan;

public interface IOrganizationScanService extends IDomainService<OrganizationScan, String> {
    OrganizationScan create(OrganizationScan organizationScan);
}
