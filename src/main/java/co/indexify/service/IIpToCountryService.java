package co.indexify.service;

import co.indexify.domain.model.Country;

import java.util.Optional;

public interface IIpToCountryService {
    Optional<Country> convert(String ip);
}
