package co.indexify.social;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

public class Linkedin extends ApiBinding {

    private static final String BASE_URL = "https://api.linkedin.com/v1/people/~:(email-address,first-name,phone-numbers,last-name)";


    public Linkedin(String token) {
        super(token, LINKEDIN);
    }



    public LinkedinDto getProfile() {
        String url = UriComponentsBuilder.fromUriString(BASE_URL)
                .queryParam("format", "json")
                .build()
                .encode().toUriString();
        HttpHeaders headers = new HttpHeaders();
//        headers.add("x-li-src", "msdk");
        HttpEntity<LinkedinDto> requestEntity = new HttpEntity<>(headers);
                ResponseEntity<LinkedinDto> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, LinkedinDto.class);
        return responseEntity.getBody();
    }





    public static class LinkedinDto {

        private String firstName;

        private String lastName;

        @JsonProperty("emailAddress")
        private String email;


        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }

}
