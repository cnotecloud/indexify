package co.indexify.social;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.web.util.UriComponentsBuilder;

public class Facebook extends ApiBinding {

    private static final String GRAPH_API_BASE_URL =
            "https://graph.facebook.com/v2.12/";

    public Facebook(String token) {
        super(token, FACEBOOK);
    }

    public FacebookDto getProfile() {
        String url  = UriComponentsBuilder.fromUriString(GRAPH_API_BASE_URL).path("me")
                .queryParam("fields", "first_name,last_name,email,verified")
                .build()
                .encode()
                .toUriString();
        return restTemplate.getForObject(url, FacebookDto.class);
    }


    public static class FacebookDto {

        @JsonProperty("first_name")
        private String firstName;

        @JsonProperty("last_name")
        private String lastName;

        private String email;


        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

    }



}