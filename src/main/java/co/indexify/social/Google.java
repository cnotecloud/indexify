package co.indexify.social;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.web.util.UriComponentsBuilder;

public class Google extends ApiBinding{

    private static final String BASE_URL =
            "https://www.googleapis.com/oauth2/v1/";



    public Google(String token) {
        super(token, GOOGLE);
    }



    public  GoogleDto verifyToken(){

        String url  = UriComponentsBuilder.fromUriString(BASE_URL)
                .path("tokeninfo")
                .queryParam("id_token", token)
                .build()
                .encode()
                .toUriString();

        return restTemplate.getForObject(url, Google.GoogleDto.class);

    }



    public static class GoogleDto{

        @JsonProperty("email_verified")
        boolean verified =  false;

        public boolean isVerified() {
            return verified;
        }

        public void setVerified(boolean verified) {
            this.verified = verified;
        }
    }
}
