package co.indexify.social;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

public abstract class ApiBinding {

    protected RestTemplate restTemplate;

    protected String token;


    public static final String FACEBOOK = "facebook";


    public static final String GOOGLE  = "google";


    public static final String LINKEDIN = "linkedin";



    public ApiBinding(String token, String provider) {
        this.restTemplate = new RestTemplate();
        this.token = token;
        initRestTemplate(provider);


    }

    private void initRestTemplate(String provider) {
        if(StringUtils.isNotEmpty(provider)){
            switch (provider){
                case FACEBOOK:
                    initBearerToken();
                    break;

                case LINKEDIN:
                    initBearerToken();
                    break;

                case GOOGLE:
                default:
                    initDefault();

            }
        }


    }

    private void initDefault() {
        this.restTemplate.getInterceptors().add(getNoTokenInterceptor());
    }


    private void initBearerToken() {
        if (token != null)
            this.restTemplate.getInterceptors()
                .add(getBearerTokenInterceptor());
    }

    private ClientHttpRequestInterceptor
    getBearerTokenInterceptor() {
        ClientHttpRequestInterceptor interceptor =
                new ClientHttpRequestInterceptor() {
                    @Override
                    public ClientHttpResponse intercept(HttpRequest request, byte[] bytes,
                                                        ClientHttpRequestExecution execution) throws IOException {
                        request.getHeaders().add("Authorization", "Bearer " + token);
                        return execution.execute(request, bytes);
                    }
                };
        return interceptor;
    }

    private ClientHttpRequestInterceptor getNoTokenInterceptor() {
        return new ClientHttpRequestInterceptor() {
            @Override
            public ClientHttpResponse intercept(HttpRequest request, byte[] bytes,
                                                ClientHttpRequestExecution execution) throws IOException {
                return execution.execute(request, bytes);
            }
        };
    }

}