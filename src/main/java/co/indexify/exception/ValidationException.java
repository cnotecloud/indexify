package co.indexify.exception;

import co.indexify.dto.ResponseCode;
import org.springframework.validation.BindingResult;

public class ValidationException extends RuntimeException {
    private BindingResult bindingResult;
    private ResponseCode responseCode;
    public ValidationException(BindingResult bindingResult, String message, ResponseCode responseCode) {
        super(message);
        this.bindingResult = bindingResult;
        this.responseCode = responseCode;
    }


    public BindingResult getBindingResult() {
        return bindingResult;
    }


    public ResponseCode getResponseCode() {
        return responseCode;
    }
}
