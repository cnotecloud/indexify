package co.indexify.exception;

import co.indexify.dto.ResponseCode;

public class NotExistException extends RuntimeException {
    private final String entity;
    private final ResponseCode code;
    public NotExistException(String entity, ResponseCode code) {
        this.entity = entity;
        this.code = code;
    }
    public String getEntity() {
        return entity;
    }

    public ResponseCode getCode() {
        return code;
    }
}
