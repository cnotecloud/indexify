package co.indexify.exception;

import co.indexify.controller.rest.advice.RuntimeExceptionRestControllerAdvice;
import co.indexify.dto.ResponseCode;

public class NotFoundException extends RuntimeException {
    private final String entity;
    private final ResponseCode code;
    public NotFoundException(String entity, ResponseCode code) {
        this.entity = entity;
        this.code = code;
    }
    public String getEntity() {
        return entity;
    }

    public ResponseCode getCode() {
        return code;
    }
}
