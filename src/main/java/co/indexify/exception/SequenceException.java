package co.indexify.exception;

public class SequenceException extends RuntimeException {

    public SequenceException(String message) {
        super(message);
    }
}
