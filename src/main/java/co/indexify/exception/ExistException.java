package co.indexify.exception;

import co.indexify.dto.ResponseCode;
import org.springframework.validation.BindingResult;

public class ExistException extends RuntimeException {

    private final String entity;

    private final ResponseCode code;

    private final BindingResult bindingResult;

    private final String value;



    public ExistException(String entity, String value) {
        this(entity, value, null, ResponseCode.VALIDATION, null);
    }

    public ExistException(String entity, ResponseCode code) {
        this(entity, null, null, code, null);
    }

    public ExistException(String entity, String value, String message, ResponseCode code, BindingResult bindingResult) {
        super(message);
        this.entity = entity;
        this.value = value;
        this.code = code;
        this.bindingResult = bindingResult;
    }

    public String getValue() {
        return value;
    }

    public String getEntity() {
        return entity;
    }

    public BindingResult getBindingResult() {
        return bindingResult;
    }

    public ResponseCode getResponseCode() {
        return code;
    }
}
