package co.indexify.security;

import co.indexify.domain.model.DomainContext;
import co.indexify.util.WebUtil;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.web.context.HttpRequestResponseHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class HttpSessionSecurityContextRepositoryExtended extends HttpSessionSecurityContextRepository{
    public SecurityContext loadContext(HttpRequestResponseHolder requestResponseHolder) {
        SecurityContext impl = super.loadContext(requestResponseHolder);
        String requestedOrganization = WebUtil.getRequestedOrganization(requestResponseHolder.getRequest());
        String requestedUser = WebUtil.getRequestedUser(requestResponseHolder.getRequest());
        String ip = WebUtil.getClientIP(requestResponseHolder.getRequest());
        String tag = WebUtil.getTag(requestResponseHolder.getRequest());
        return new SecurityContextImplExtended(impl, new DomainContext(requestResponseHolder.getRequest()));
    }
}
