package co.indexify.security;

import co.indexify.domain.model.DomainContext;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class SecurityContextExtendedPersistenceFilter extends GenericFilterBean {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        SecurityContext context = SecurityContextHolder.getContext();
        DomainContext domainContext = new DomainContext((HttpServletRequest) servletRequest);
        SecurityContextImplExtended contextExtended = new SecurityContextImplExtended(context, domainContext);
        SecurityContextHolder.setContext(contextExtended);
        filterChain.doFilter(servletRequest, servletResponse);
        SecurityContextHolder.setContext(((SecurityContextImplExtended)SecurityContextHolder.getContext()).getSecurityContext());
    }
}
