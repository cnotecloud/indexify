package co.indexify.security.ajax;

import co.indexify.dto.LoginRequestDto;
import co.indexify.exception.AuthMethodNotSupportedException;
import co.indexify.util.WebUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public final class AjaxLoginProcessingFilter extends AbstractAuthenticationProcessingFilter {
    private ObjectMapper objectMapper;
    private static final Logger logger = LoggerFactory.getLogger(AjaxLoginProcessingFilter.class);

    protected final void setDetails(HttpServletRequest request,UsernamePasswordAuthenticationToken authRequest) {
        authRequest.setDetails(this.authenticationDetailsSource.buildDetails(request));
    }


    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        if (HttpMethod.POST.name().equals(request.getMethod()) && WebUtil.isAjax(request)) {
            LoginRequestDto loginRequest = this.objectMapper.readValue(request.getReader(), LoginRequestDto.class);
            if (!StringUtils.isBlank(loginRequest.getUsername()) && !StringUtils.isBlank(loginRequest.getPassword())) {
                UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword());
                this.setDetails(request, authRequest);
                Authentication authentication = this.getAuthenticationManager().authenticate((Authentication)authRequest);
                return authentication;
            } else {
                throw new AuthenticationServiceException("Username or Password not provided");
            }
        } else {
            if (logger.isDebugEnabled()) {
                this.logger.debug("Authentication method not supported. Request method: " + request.getMethod());
            }

            throw new AuthMethodNotSupportedException("Authentication method not supported");
        }
    }

    protected void successfulAuthentication(HttpServletRequest request,HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        this.getSuccessHandler().onAuthenticationSuccess(request, response, authResult);
    }

    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        this.getFailureHandler().onAuthenticationFailure(request, response, failed);
    }

    public AjaxLoginProcessingFilter(String defaultProcessUrl, ObjectMapper objectMapper) {
        super(defaultProcessUrl);
        this.objectMapper = objectMapper;
    }

    public AjaxLoginProcessingFilter(String defaultProcessUrl, AuthenticationSuccessHandler successHandler, AuthenticationFailureHandler failureHandler, ObjectMapper objectMapper) {
        this(defaultProcessUrl, objectMapper);
        this.setAuthenticationFailureHandler(failureHandler);
        this.setAuthenticationSuccessHandler(successHandler);
    }


}
