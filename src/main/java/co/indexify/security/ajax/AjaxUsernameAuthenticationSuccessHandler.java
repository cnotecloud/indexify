package co.indexify.security.ajax;

import co.indexify.dto.UserContext;
import co.indexify.event.UserIdentifiedEvent;
import co.indexify.token.Token;
import co.indexify.token.jwt.TokenFactory;
import co.indexify.util.SecurityUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
public class AjaxUsernameAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    private final ObjectMapper mapper;
    private final TokenFactory tokenFactory;
    private final ApplicationEventPublisher applicationEventPublisher;

    public AjaxUsernameAuthenticationSuccessHandler(ObjectMapper mapper, TokenFactory tokenFactory, ApplicationEventPublisher applicationEventPublisher) {
        this.mapper = mapper;
        this.tokenFactory = tokenFactory;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
            Authentication authentication) throws IOException, ServletException {
        UserContext userContext = (UserContext) authentication.getPrincipal();
        Boolean isOtp = SecurityUtil.getOtp(authentication);
        if(isOtp){
            applicationEventPublisher.publishEvent(new UserIdentifiedEvent(userContext.getUserDetails(), request.getLocale()));
        }
        
        Token accessToken = tokenFactory.createAccessJwtToken(userContext, 3);
        Map<String, String> tokenMap = new HashMap<String, String>();
        tokenMap.put("accessToken", accessToken.getToken());

        response.setStatus(HttpStatus.OK.value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        mapper.writeValue(response.getWriter(), tokenMap);
        clearAuthenticationAttributes(request);
    }

    /**
     * Removes temporary authentication-related data which may have been stored
     * in the session during the authentication process..
     * 
     */
    protected final void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);

        if (session == null) {
            return;
        }

        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }
}
