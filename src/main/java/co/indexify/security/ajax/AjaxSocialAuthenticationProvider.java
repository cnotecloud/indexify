package co.indexify.security.ajax;

import co.indexify.domain.model.DomainContext;
import co.indexify.domain.model.Email;
import co.indexify.domain.model.Role;
import co.indexify.domain.model.User;
import co.indexify.dto.UserContext;
import co.indexify.event.RegistrationCompleteEvent;
import co.indexify.security.CustomUserDetails;
import co.indexify.security.SocialAuthenticationToken;
import co.indexify.service.IUserService;
import co.indexify.social.Facebook;
import co.indexify.social.Google;
import co.indexify.social.Linkedin;
import co.indexify.util.SecurityUtil;
import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.client.RestClientException;

import java.util.Optional;


@Component
public class AjaxSocialAuthenticationProvider implements AuthenticationProvider {
    private final BCryptPasswordEncoder passwordEncoder;
    private final UserDetailsService userDetailsService;
    private final ApplicationEventPublisher applicationEventPublisher;
    private final String MESSAGE_TOKEN_INVALID_CREDENTIAL_TEMP ="Authentication Failed. Provider or Token is incorrect";
    private final String MESSAGE_USER_NO_ROLE_ASSIGNED_TEMP ="Authentication Failed. User is not assigned any role.";






    @Autowired
    public AjaxSocialAuthenticationProvider(final UserDetailsService userDetailsService, final BCryptPasswordEncoder passwordEncoder, ApplicationEventPublisher applicationEventPublisher) {
        this.userDetailsService = userDetailsService;
        this.passwordEncoder = passwordEncoder;
        this.applicationEventPublisher =  applicationEventPublisher;
    }


    @Autowired
    private IUserService userService;



    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Assert.notNull(authentication, "No authentication data provided");

        String accessToken = (String) authentication.getPrincipal();
        String provider = ((SocialAuthenticationToken) authentication).getProvider();

        SocialDto socialDto = new SocialDto();


        if("facebook".equals(provider)){

            Facebook facebook = new Facebook(accessToken);
            try{
                Facebook.FacebookDto dto = facebook.getProfile();
                if(dto != null && dto.getEmail() != null){
                    socialDto.setEmail(dto.getEmail());
                    socialDto.setFirstName(dto.getFirstName());
                    socialDto.setLastName(dto.getLastName());

                }else {
                    throw new BadCredentialsException(MESSAGE_TOKEN_INVALID_CREDENTIAL_TEMP);
                }

            }catch (RestClientException ex){
                throw new BadCredentialsException(MESSAGE_TOKEN_INVALID_CREDENTIAL_TEMP);
            }

        }else if("google".equals(provider)){
            Google google =  new Google(accessToken);
            try{
                Google.GoogleDto dto = google.verifyToken();
                if(dto.isVerified()){

                    try {
                        DecodedJWT jwt = JWT.decode(accessToken);
                        String email = jwt.getClaim("email").asString();
                        String lastName = jwt.getClaim("family_name").asString();
                        String firstName = jwt.getClaim("given_name").asString();

                        socialDto.setEmail(email);
                        socialDto.setFirstName(firstName);
                        socialDto.setLastName(lastName);

                    }catch (JWTDecodeException ex){
                        throw new BadCredentialsException(MESSAGE_TOKEN_INVALID_CREDENTIAL_TEMP);
                    }


                }else {
                    throw new BadCredentialsException(MESSAGE_TOKEN_INVALID_CREDENTIAL_TEMP);
                }


            }catch (RestClientException ex){
                throw new BadCredentialsException(MESSAGE_TOKEN_INVALID_CREDENTIAL_TEMP);
            }
        } else if("linkedin".equals(provider)){
            Linkedin linkedin = new Linkedin(accessToken);
            try{
                Linkedin.LinkedinDto dto = linkedin.getProfile();
                if(dto != null && dto.getEmail() != null){
                    socialDto.setEmail(dto.getEmail());
                    socialDto.setFirstName(dto.getFirstName());
                    socialDto.setLastName(dto.getLastName());
                }else {
                    throw new BadCredentialsException(MESSAGE_TOKEN_INVALID_CREDENTIAL_TEMP);
                }

            }catch (RestClientException ex){
                throw new BadCredentialsException(MESSAGE_TOKEN_INVALID_CREDENTIAL_TEMP);
            }
        }


        else {

            throw new BadCredentialsException(MESSAGE_TOKEN_INVALID_CREDENTIAL_TEMP);

        }




        SecurityContextHolder.getContext().setAuthentication(authentication);

        CustomUserDetails userDetails = null;

        try{
            userDetails = (CustomUserDetails) userDetailsService.loadUserByUsername(socialDto.getEmail());
           /* if(!userDetails.isEnabled()){
                userDetails.getUser().setEnabled(true);
                userService.update(userDetails.getUser());
            } */

        }catch (UsernameNotFoundException ex){
            User user = new User();
            user.setEnabled(true);
            user.setFirstName(socialDto.getFirstName());
            user.setLastName(socialDto.getLastName());
            user.setRoles(Sets.newHashSet(Role.ROLE_ADMIN));
            user.setEmail(new Email(socialDto.getEmail()));
            userService.create(user);
            Optional<DomainContext> domainContext = SecurityUtil.getDomainContext(authentication);
            domainContext.ifPresent(c -> {
                applicationEventPublisher.publishEvent(new RegistrationCompleteEvent(user, null, c.getLocale()));
            });
            userDetails = (CustomUserDetails) userDetailsService.loadUserByUsername(socialDto.getEmail());
        }
        SecurityContextHolder.getContext().setAuthentication(null);

        if (userDetails.getAuthorities() == null && userDetails.getAuthorities().isEmpty()) throw new InsufficientAuthenticationException(MESSAGE_USER_NO_ROLE_ASSIGNED_TEMP);

        UserContext userContext = UserContext.create(userDetails.getUsername(),userDetails.getRoles(), userDetails.getAuthorities(), userDetails);
        return new UsernamePasswordAuthenticationToken(userContext, null, userContext.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return (SocialAuthenticationToken.class.isAssignableFrom(authentication));
    }
}
