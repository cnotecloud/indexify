package co.indexify.security.ajax;

import java.util.Collection;

import co.indexify.dto.UserContext;
import co.indexify.security.CustomUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;


@Component
public class AjaxAuthenticationProvider implements AuthenticationProvider {
    private final BCryptPasswordEncoder encoder;
    private final UserDetailsService userService;
    private final String MESSAGE_USER_DISABLED_TEMP="Authentication failed. User is not enabled.";
    private final String MESSAGE_USER_INVALID_CREDENTIAL_TEMP ="Authentication Failed. Username or Password not valid.";
    private final String MESSAGE_USER_NO_ROLE_ASSIGNED_TEMP ="Authentication Failed. User is not assigned any role.";

    @Autowired
    public AjaxAuthenticationProvider(final UserDetailsService userDetailsService, final BCryptPasswordEncoder encoder) {
        this.userService = userDetailsService;
        this.encoder = encoder;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Assert.notNull(authentication, "No authentication data provided");

        String username = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();
        SecurityContextHolder.getContext().setAuthentication(authentication);
        CustomUserDetails userDetails = (CustomUserDetails) userService.loadUserByUsername(username);
        SecurityContextHolder.getContext().setAuthentication(null);
        String message = null;
        if(!userDetails.isEnabled()){
            throw new DisabledException(MESSAGE_USER_DISABLED_TEMP);
        }

        if (!encoder.matches(password, userDetails.getPassword())) {
            throw new BadCredentialsException(MESSAGE_USER_INVALID_CREDENTIAL_TEMP);
        }


        if (userDetails.getAuthorities() == null && userDetails.getAuthorities().isEmpty()) throw new InsufficientAuthenticationException(MESSAGE_USER_NO_ROLE_ASSIGNED_TEMP);


        UserContext userContext = UserContext.create(userDetails.getUsername(),userDetails.getRoles(), userDetails.getAuthorities(), userDetails);
        return new UsernamePasswordAuthenticationToken(userContext, null, userContext.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }
}
