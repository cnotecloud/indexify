package co.indexify.security.ajax;

import co.indexify.domain.model.Role;
import co.indexify.dto.UserContext;
import co.indexify.event.UserIdentifiedEvent;
import co.indexify.security.CustomUserDetails;
import co.indexify.service.ISmsService;
import co.indexify.service.IUserService;
import co.indexify.util.SecurityUtil;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Collection;


/**
 *
 * <p>
 * Aug 3, 2016
 */
@Component
public class AjaxUsernameAuthenticationProvider implements AuthenticationProvider {
    private final UserDetailsService userDetailsService;
    private final String MESSAGE_USER_DISABLED_TEMP = "Authentication failed. User is not enabled.";
    private final String MESSAGE_USER_INVALID_CREDENTIAL_TEMP = "Authentication Failed. Username or Password not valid.";
    private final String MESSAGE_USER_NO_ROLE_ASSIGNED_TEMP = "Authentication Failed. User is not assigned any role.";

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    private IUserService userService;

    @Autowired
    public AjaxUsernameAuthenticationProvider(final UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Assert.notNull(authentication, "No authentication data provided");
        String username = (String) authentication.getPrincipal();
      //  SecurityContextHolder.getContext().setAuthentication(authentication);
        CustomUserDetails userDetails = (CustomUserDetails) userDetailsService.loadUserByUsername(username);
  //      SecurityContextHolder.getContext().setAuthentication(null);
        Boolean isOtp = SecurityUtil.getOtp(authentication);
        if (isOtp) {
            //
        } else {
            if (!userDetails.isEnabled()) {
                throw new DisabledException(MESSAGE_USER_DISABLED_TEMP);
            }
        }

        UserContext userContext = UserContext.create(userDetails.getUsername(),Sets.newHashSet(Role.ROLE_USER_FOUND), userDetails.getAuthorities(), userDetails);
        //userContext.getUserDetails().getUser().setRoles();
        return new UsernameAuthenticationToken(userContext,userContext.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return (UsernameAuthenticationToken.class.isAssignableFrom(authentication));
    }
}
