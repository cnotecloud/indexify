package co.indexify.security.ajax;

import co.indexify.dto.UsernameLoginDto;
import co.indexify.exception.AuthMethodNotSupportedException;
import co.indexify.util.WebUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AjaxUsernameProcessingFilter extends AbstractAuthenticationProcessingFilter{

    private final ObjectMapper objectMapper;

    public AjaxUsernameProcessingFilter(String defaultProcessUrl, AuthenticationSuccessHandler successHandler, AuthenticationFailureHandler failureHandler, ObjectMapper objectMapper){
        super(defaultProcessUrl);
        setAuthenticationFailureHandler(failureHandler);
        setAuthenticationSuccessHandler(successHandler);
        this.objectMapper = objectMapper;
    }

    protected void setDetails(HttpServletRequest request , UsernameAuthenticationToken authRequest) {
        authRequest.setDetails(this.authenticationDetailsSource.buildDetails(request));
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        if (!HttpMethod.POST.name().equals(request.getMethod()) || !WebUtil.isAjax(request)) {
            if (logger.isDebugEnabled()) {
                logger.debug("Authentication method not supported. Request method: " + request.getMethod());
            }
            throw new AuthMethodNotSupportedException("Authentication method not supported");
        }
        UsernameLoginDto usernameRequest = objectMapper.readValue(request.getReader(), UsernameLoginDto.class);
        if (StringUtils.isBlank(usernameRequest.getUsername())) {
            throw new AuthenticationServiceException("Username is not provided.");
        }
        UsernameAuthenticationToken authRequest = new UsernameAuthenticationToken(usernameRequest.getUsername());
        this.setDetails(request, authRequest);
        return this.getAuthenticationManager().authenticate(authRequest);
    }
    @Override
    public void successfulAuthentication(HttpServletRequest request , HttpServletResponse response , FilterChain chain,
                                         Authentication authResult) throws IOException, ServletException {
        getSuccessHandler().onAuthenticationSuccess(request, response, authResult);
    }

    @Override
     public void unsuccessfulAuthentication(HttpServletRequest request , HttpServletResponse response,
                                            AuthenticationException failed) throws IOException, ServletException {
        getFailureHandler().onAuthenticationFailure(request, response, failed);
    }

}
