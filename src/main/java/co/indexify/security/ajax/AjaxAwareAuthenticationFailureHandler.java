package co.indexify.security.ajax;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.indexify.dto.ResponseCode;
import co.indexify.dto.ResponseDto;
import co.indexify.dto.ResponseMessage;
import co.indexify.exception.AuthMethodNotSupportedException;
import co.indexify.exception.JwtExpiredTokenException;
import co.indexify.exception.UserDisabledException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 *
 *
 * Aug 3, 2016
 */
@Component
public class AjaxAwareAuthenticationFailureHandler implements AuthenticationFailureHandler {
    private final ObjectMapper mapper;

    @Autowired
    private MessageSource messageSource;
    
    @Autowired
    public AjaxAwareAuthenticationFailureHandler(ObjectMapper mapper) {
        this.mapper = mapper;
    }	
    
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException e) throws IOException, ServletException {
		
		response.setStatus(HttpStatus.UNAUTHORIZED.value());
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		String message = null;
		if(e instanceof DisabledException){
			message = messageSource.getMessage(ResponseMessage.MESSAGE_USER_DISABLED, null, request.getLocale());
			mapper.writeValue(response.getWriter(), ResponseDto.of(message, ResponseCode.USER_DISABLED, HttpStatus.UNAUTHORIZED));
		}
		else if (e instanceof BadCredentialsException) {
			mapper.writeValue(response.getWriter(), ResponseDto.of("Invalid username or password", ResponseCode.AUTHENTICATION, HttpStatus.UNAUTHORIZED));
		} else if (e instanceof JwtExpiredTokenException) {
			mapper.writeValue(response.getWriter(), ResponseDto.of("Token has expired", ResponseCode.JWT_TOKEN_EXPIRED, HttpStatus.UNAUTHORIZED));
		} else if (e instanceof AuthMethodNotSupportedException) {
		    mapper.writeValue(response.getWriter(), ResponseDto.of(e.getMessage(), ResponseCode.AUTHENTICATION, HttpStatus.UNAUTHORIZED));
		}

		mapper.writeValue(response.getWriter(), ResponseDto.of("Authentication failed", ResponseCode.AUTHENTICATION, HttpStatus.UNAUTHORIZED));
	}
}
