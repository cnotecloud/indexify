package co.indexify.security;
import co.indexify.domain.model.Country;
import co.indexify.domain.model.Role;
import co.indexify.domain.model.User;
import co.indexify.service.IIpToCountryService;
import co.indexify.service.IRoleService;
import co.indexify.service.IUserService;
import co.indexify.util.SecurityUtil;
import co.indexify.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {
	@Autowired
	private IUserService userService;

	@Autowired
	private IRoleService roleService;
	static final boolean ENABLED = true;
	static final boolean ACCOUNT_NOT_EXPIRED = true;
	static final boolean CREDENTIALS_NOT_EXPIRED = true;
	static final boolean ACCOUNT_NOT_LOCKED = true;

	public static final String MESSAGE_USER_NOT_FOUND = "User not found with username %s";


	@Autowired
	private IIpToCountryService ipToCountryService;



	@Override
	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
		String usernameFromIp = null;
		String usedUsername = null;
		if(!username.startsWith("+")) {
			Optional<String> ip = SecurityUtil.getIp();
			if(ip.isPresent()){
				System.out.println("---------------ip="+ip.get()+"--------------------------");
				Optional<Country> country = ipToCountryService.convert(ip.get());
				if(country.isPresent()){
					usernameFromIp = String.format("+%s%s",country.get().getCode(), username);
				}
			}
		}
		Optional<User> userOpt = userService.findByCandidateKey(username);
		if(!userOpt.isPresent()){
			userOpt = userService.findByContact(usernameFromIp);
			if(!userOpt.isPresent()){
				throw new UsernameNotFoundException(String.format(MESSAGE_USER_NOT_FOUND,username));
			}else{
				usedUsername = usernameFromIp;
			}
		}else {
			usedUsername = username;
		}

		User user = userOpt.get();
		//Set<? extends GrantedAuthority> roles = getRoles(user.getRoles());
		Set<? extends GrantedAuthority> privileges = getAuthorities(user.getRoles());
		Boolean enabled = user.getEnabled();
        enabled = enabled != null ? enabled : true;

        return new CustomUserDetails(user,usedUsername, enabled ,ACCOUNT_NOT_LOCKED, CREDENTIALS_NOT_EXPIRED, ACCOUNT_NOT_EXPIRED, privileges);
	}





	public  Set<GrantedAuthority> getRoles(Set<String> userRoles){
		Set<GrantedAuthority> roles = userRoles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toSet());
		return roles;
	}


	public final Set<? extends GrantedAuthority> getAuthorities(final Set<String> roles) {
		return getGrantedAuthorities(getPrivileges(roles));
	}

	private final Set<String> getPrivileges(final Set<String> userRoles) {
		final Set<String> privilegesAsString = new HashSet<>();
		final Set<String> privileges = new HashSet<>();
		for (final String userRole : userRoles) {
			Role role = roleService.findByName(userRole);
			if(role!= null && role.getPrivileges() != null && !role.getPrivileges().isEmpty())
				privileges.addAll(role.getPrivileges());
		}
		for (final String item : privileges) {
			privilegesAsString.add(item);
		}

		return privilegesAsString;
	}

	private final Set<GrantedAuthority> getGrantedAuthorities(final Set<String> privileges) {
		final Set<GrantedAuthority> authorities = new HashSet<>();
		for (final String privilege : privileges) {
			authorities.add(new SimpleGrantedAuthority(privilege));
		}
		return authorities;
	}





}
