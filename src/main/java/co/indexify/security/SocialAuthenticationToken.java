package co.indexify.security;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;

import java.util.Collection;


public class SocialAuthenticationToken extends AbstractAuthenticationToken {

    private final Object principal;

    private final String provider;


    public String getProvider() {
        return provider;
    }

    public SocialAuthenticationToken(Object principal, String provider) {
        super(null);
        this.principal = principal;
        this.provider = provider;
        setAuthenticated(false);
    }

    public SocialAuthenticationToken(Object principal, String provider, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.principal = principal;
        this.provider = provider;
        super.setAuthenticated(true);
    }

    // ~ Methods
    // ========================================================================================================

    public Object getCredentials() {
        return principal;
    }

    public Object getPrincipal() {
        return this.principal;
    }

    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        if (isAuthenticated) {
            throw new IllegalArgumentException(
                    "Cannot set this token to trusted - use constructor which takes a GrantedAuthority list instead");
        }

        super.setAuthenticated(false);
    }


}
