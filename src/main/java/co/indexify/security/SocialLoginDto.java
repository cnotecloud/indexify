package co.indexify.security;

import java.io.Serializable;

public class SocialLoginDto implements Serializable {

    private String provider;

    private String accessToken;


    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
