package co.indexify.security.anonymous;


import co.indexify.domain.model.Role;
import co.indexify.dto.UserContext;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import javax.servlet.http.HttpServletRequest;

public class ExtendedAnonymousAuthenticationFilter extends AnonymousAuthenticationFilter {

    private AuthenticationDetailsSource<HttpServletRequest, ?> authenticationDetailsSource = new WebAuthenticationDetailsSource();
    private String key;

    public ExtendedAnonymousAuthenticationFilter(String key, AuthenticationDetailsSource<HttpServletRequest, ?> authenticationDetailsSource) {
        super(key, UserContext.ANONYMOUS_USER_CONTEXT, AuthorityUtils.createAuthorityList(Role.ANONYMOUS_ROLE.privileges().toArray()));
        this.authenticationDetailsSource = authenticationDetailsSource;
        this.key = key;
    }

    @Override
    protected Authentication createAuthentication(HttpServletRequest request) {
        AnonymousAuthenticationToken auth = new AnonymousAuthenticationToken(key,
                getPrincipal(), getAuthorities());
        auth.setDetails(authenticationDetailsSource.buildDetails(request));
        return auth;
    }



}