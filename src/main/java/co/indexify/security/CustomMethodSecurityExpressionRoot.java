package co.indexify.security;

import co.indexify.domain.model.Organization;
import co.indexify.dto.UserContext;
import co.indexify.service.IOrganizationService;
import co.indexify.util.SecurityUtil;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;

public final class CustomMethodSecurityExpressionRoot extends SecurityExpressionRoot implements MethodSecurityExpressionOperations {
    private Object filterObject;
    private Object returnObject;
    private final IOrganizationService organizationService;

    public final boolean isMyOrganization() {
        return SecurityUtil.isMyOrganization(this.getMyOrganizationId());
    }

    public final boolean isMe() {
        return SecurityUtil.isMe(this.getMeId());
    }

    public final String getMeId() {
        if (this.getPrincipal() != null && this.getPrincipal() instanceof UserContext) {
            CustomUserDetails userDetails = ((UserContext) this.getPrincipal()).getUserDetails();
            if (userDetails != null && userDetails.getUser() != null && userDetails.getUser().getId() != null)
                return String.valueOf(userDetails.getUser().getId());
            else return null;
        } else return null;
    }

    public final String getMyOrganizationId() {
        if (this.getPrincipal() != null && this.getPrincipal() instanceof UserContext) {
            CustomUserDetails userDetails = ((UserContext) this.getPrincipal()).getUserDetails();
            if (userDetails != null && userDetails.getUser() != null && userDetails.getUser().getOrganization() != null && userDetails.getUser().getOrganization().getId() != null)
                return String.valueOf(userDetails.getUser().getOrganization().getId());
            else return null;
        }else return null;
    }

    public final boolean isMyOrganizationOrPublicProfile() {
        boolean result;
        if (!this.isMyOrganization()) {
            label28: {
                Organization organization = this.organizationService.findById(NumberUtils.toLong(SecurityUtil.getRequestedOrganization())).orElse(null);
                if (organization != null) {
                    if (organization.getPublicProfile()) {
                        break label28;
                    }
                }
                result = false;
                return result;
            }
        }

        result = true;
        return result;
    }

    public final boolean isMeAndNotMyOrganization() {
        return this.isMe() && !this.isMyOrganization();
    }

    public final boolean isMeAndNotMyOrganizationOrIsMeAndOtheIsNotMe(Long other) {
        return (this.isMe() && other == null && !this.isMyOrganization()) || (other != null && !String.valueOf(other).equals(this.getMeId()));
    }

    public Object getFilterObject() {
        return this.filterObject;
    }

    public Object getReturnObject() {
        return this.returnObject;
    }

    public Object getThis() {
        return this;
    }

    public void setFilterObject(Object obj) {
        this.filterObject = obj;
    }

    public void setReturnObject(Object obj) {
        this.returnObject = obj;
    }

    public final IOrganizationService getOrganizationService() {
        return this.organizationService;
    }
    public CustomMethodSecurityExpressionRoot(Authentication authentication, IOrganizationService organizationService) {
        super(authentication);
        this.organizationService = organizationService;
    }
}
