package co.indexify.security;


import co.indexify.domain.model.Email;
import co.indexify.domain.model.Role;
import co.indexify.domain.model.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Sets;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CustomUserDetails  implements UserDetails {

    private static final long serialVersionUID = 1L;

    private Boolean accountNonExpired;
    private Boolean credentialsNonExpired;
    private Boolean accountNonLocked;
    private Collection<? extends GrantedAuthority> authorities;
    private Boolean enabled;
    private User user;
    private String usedUsername;


    public static final CustomUserDetails ANONYMOUS_CUSTOM_USER_DETAILS = new CustomUserDetails(User.ANONYMOUS_USER, Email.ANONYMOUS_EMAIL, Sets.newHashSet(AuthorityUtils.createAuthorityList(Role.ANONYMOUS_ROLE.privileges().toArray())));



    public CustomUserDetails(User user,  String usedUsername, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Set<? extends GrantedAuthority> authorities) {
        this.user = user;
        this.usedUsername = usedUsername;
        this.enabled = enabled;
        this.accountNonExpired = accountNonExpired;
        this.credentialsNonExpired = credentialsNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.authorities = authorities;

    }

    public CustomUserDetails(User user,String usedUsername, Set<? extends GrantedAuthority> authorities) {
        this(user,usedUsername, true, true, true, true, authorities);
    }


    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    @JsonIgnore
    public String getPassword() {
        return user.getPassword();
    }


    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return enabled;
    }


    @Override
    public String getUsername() {
        return usedUsername;
    }

    @JsonIgnore
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public CustomUserDetails authorities(){

        return this;

    }

    public Set<String> toStringCollection(){
        return this.authorities.stream().map(x -> x.getAuthority()).collect(Collectors.toSet());
    }


    public Set<String> getRoles(){
        return this.user.getRoles();
    }


    /*
    public List<String> getRoles(){
        List<String> roles  = new ArrayList<>();
        authorities.forEach(x -> {
            roles.add(x.getAuthority());
        });
        return roles;
    }*/



}
