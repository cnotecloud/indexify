package co.indexify.security;

import co.indexify.util.WebUtil;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

/**
 * SkipPathRequestMatcher
 * 
 *
 *
 * Aug 19, 2016
 */
public class SkipPathMethodRequestMatcher implements RequestMatcher {
    private OrRequestMatcher matchers;
    private RequestMatcher processingMatcher;

    public SkipPathMethodRequestMatcher(List<RequestMatcher> pathsToSkip, String processingPath) {
        matchers = new OrRequestMatcher(pathsToSkip);
        processingMatcher = new AntPathRequestMatcher(processingPath);
    }

    @Override
    public boolean matches(HttpServletRequest request) {
        if (matchers.matches(request)) {
            return false;
        }
        boolean pathMatch =  processingMatcher.matches(request);
        String authorizationHearder = WebUtil.getJwtAuthorizationHeader(request);
        boolean headerMatch = (authorizationHearder != null) && (authorizationHearder.startsWith("Bearer"));
        return pathMatch && headerMatch;

    }
}
