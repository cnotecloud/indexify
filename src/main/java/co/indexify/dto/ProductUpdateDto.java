package co.indexify.dto;

import co.indexify.domain.model.Control;
import com.ankurpathak.server.validation.constraints.ImageType;
import com.ankurpathak.server.validation.constraints.Url;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

public class ProductUpdateDto {
    @NotBlank
    private String displayName;

     List<@Valid @NotNull Control> controls;

     List<@ImageType @NotNull MultipartFile> images;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public List<Control> getControls() {
        return controls;
    }

    public void setControls(List<Control> controls) {
        this.controls = controls;
    }

	public List<MultipartFile> getImages() {
		return images;
	}

	public void setImages(List<MultipartFile> images) {
		this.images = images;
	}

}
