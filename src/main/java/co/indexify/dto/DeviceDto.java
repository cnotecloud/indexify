package co.indexify.dto;


import com.ankurpathak.server.validation.constraints.Any;

import javax.validation.constraints.NotBlank;

import static co.indexify.domain.model.Platform.*;

public class DeviceDto {

    @NotBlank
    @Any({GCM, APNS, APNS_SANDBOX})
    private String platform;


    @NotBlank
    private String device;


    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }
}
