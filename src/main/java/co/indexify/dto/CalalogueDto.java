package co.indexify.dto;

import com.ankurpathak.server.validation.constraints.Url;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;



public class CalalogueDto implements Serializable{


    @NotBlank
    @Url
    private String catalogue;


    public String getCatalogue() {
        return catalogue;
    }

    public void setCatalogue(String catalogue) {
        this.catalogue = catalogue;
    }
}
