package co.indexify.dto;

public final class MethodSecurityDto {
    public static final String ME = "isMe()";
    public static final String MY_ORGANIZATION = "isMyOrganization()";
    public static final String PRE_MY_ORGANIZATION_OR_PUBLIC_PROFILE = "isMyOrganizationOrPublicProfile()";
    public static final String MY_ORGANIZATION_NOT_EXISTS = "principal?.userDetails?.user?.organization == null";
    public static final String ME_AND_NOT_MY_ORGANIZATION = "isMeAndNotMyOrganization()";
    public static final String POST_ME_OR_PUBLIC_PROFILE = "returnObject?.orElse(null)?.id == principal?.userDetails?.user?.id || returnObject?.orElse(null)?.publicProfile == true";
    public static final String POST_MY_ORGANIZATION_OR_PUBLIC_PROFILE = "returnObject?.orElse(null)?.id == principal?.userDetails?.user?.organization?.id || returnObject?.orElse(null)?.publicProfile == true";
    public static final String ME_AND_NOT_MY_ORGANIZATION_AND_OTHER_IS_NOT_ME = "isMeAndNotMyOrganization() && ";

}
