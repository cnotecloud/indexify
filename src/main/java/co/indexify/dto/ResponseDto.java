package co.indexify.dto;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.http.HttpStatus;

/**
 * Error model for interacting with client.
 * 
 *
 *
 * Aug 3, 2016
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ResponseDto {
    // HTTP Response Status Code
    private final HttpStatus status;

    // General Error message
    private final String message;

    // Error code
    private final ResponseCode responseCode;

    @JsonFormat(pattern = "yyyy-MM-dd'T'hh:mm:ss.SSSZ")
    private final Date timestamp;

    private final Map<String, Object> extras = new HashMap<>();

    protected ResponseDto(final String message, final ResponseCode responseCode, HttpStatus status) {
        this.message = message;
        this.responseCode = responseCode;
        this.status = status;
        this.timestamp = new Date();
    }

    public static ResponseDto of(final String message, final ResponseCode responseCode, HttpStatus status) {
        return new ResponseDto(message, responseCode, status);
    }

    public Integer getStatus() {
        return status.value();
    }

    public String getMessage() {
        return message;
    }

    public ResponseCode getResponseCode() {
        return responseCode;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public Map<String, Object> getExtras() {
        return extras;
    }

    public ResponseDto addExtra(String name, Object value){
        this.extras.put(name, value);
        return this;
    }
}
