package co.indexify.dto;

import javax.validation.constraints.NotBlank;

public class FavouriteProductDto {

    @NotBlank
    private String productId;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
