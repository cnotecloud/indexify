package co.indexify.dto;

import java.io.Serializable;

public class OrganizationIdNameDto implements Serializable{
    private String organizationName;

    private Long organizationId;


    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

}
