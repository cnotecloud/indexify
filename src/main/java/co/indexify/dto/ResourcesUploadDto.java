package co.indexify.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

public class ResourcesUploadDto{

    @NotNull
    @NotEmpty
    List<@Valid @NotNull ResourceUploadDto> resources;

    public List<ResourceUploadDto> getResources() {
        return resources;
    }

    public void setResources(List<ResourceUploadDto> resources) {
        this.resources = resources;
    }
}
