package co.indexify.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ValidationErrorDto {
    private Map<String, List<String>> errors = new HashMap<>();

    public void addError(String path, String message) {
        List<String> messages = null;
        if(errors.containsKey(path)){
            messages = errors.get(path);

        }else {
            messages = new ArrayList<>();
            errors.put(path, messages);
        }
        messages.add(message);
    }

    public Map<String, List<String>> getErrors() {
        return errors;
    }

    public void setErrors(Map<String, List<String>> errors) {
        this.errors = errors;
    }
}
