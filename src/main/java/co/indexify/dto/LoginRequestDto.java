package co.indexify.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginRequestDto {

    private final String username;
    private final String password;

    public final String getUsername() {
        return this.username;
    }

    public final String getPassword() {
        return this.password;
    }

    @JsonCreator
    public LoginRequestDto(@JsonProperty("username") String username, @JsonProperty("password") String password) {
        this.username = username;
        this.password = password;
    }
}
