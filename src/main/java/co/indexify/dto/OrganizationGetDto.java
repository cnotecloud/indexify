package co.indexify.dto;

import co.indexify.domain.model.Location;
import co.indexify.domain.model.Organization;
import com.ankurpathak.server.validation.constraints.Contact;
import com.ankurpathak.server.validation.constraints.Email;
import com.ankurpathak.server.validation.constraints.Url;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@JsonInclude(Include.NON_EMPTY)

public final class OrganizationGetDto {
    private Long id;
    @NotBlank
    private String displayName;
    @Valid
    @NotNull
    private Location location;
    @Url
    private String website;
    @Email
    private String email;
    @Contact
    private String contact;
    private String description;
    private boolean publicProfile = true;
    private String catalogue;
    private String stall;
    private Set<String> catalogues;
    private Set<String> tags;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isPublicProfile() {
        return publicProfile;
    }

    public void setPublicProfile(boolean publicProfile) {
        this.publicProfile = publicProfile;
    }

    public String getCatalogue() {
        return catalogue;
    }

    public void setCatalogue(String catalogue) {
        this.catalogue = catalogue;
    }

    public String getStall() {
        return stall;
    }

    public void setStall(String stall) {
        this.stall = stall;
    }

    public Set<String> getCatalogues() {
        return catalogues;
    }

    public void setCatalogues(Set<String> catalogues) {
        this.catalogues = catalogues;
    }

    public Set<String> getTags() {
        return tags;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }

    public static OrganizationGetDto fromDomain(Organization organization) {
        OrganizationGetDto dto = new OrganizationGetDto();
        dto.setDisplayName(organization.getDisplayName());
        dto.setLocation(organization.getLocation());
        dto.setWebsite(organization.getWebsite());
        if(organization.getEmails()!= null)
            dto.setEmail(organization.getEmails().stream().filter(x -> co.indexify.domain.model.Email.EMAIL_PRIMARY.equals(x.getTag())).map(co.indexify.domain.model.Email::getValue).findFirst().orElse(null));
        dto.setDescription(organization.getDescription());
        dto.setId((Long)organization.getId());
        dto.setPublicProfile(organization.getPublicProfile());
        dto.setCatalogue(organization.getCatalogue());
        dto.setStall(organization.getStall());
        dto.setCatalogues(organization.getCatalogues());
        dto.setTags(organization.getTags());
        return dto;
    }

}
