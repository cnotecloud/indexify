package co.indexify.dto;

import javax.validation.constraints.NotBlank;
public class ProductNodeDto {
    @NotBlank
    private String id;
    @NotBlank
    private String leaf;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLeaf() {
        return leaf;
    }

    public void setLeaf(String leaf) {
        this.leaf = leaf;
    }
}
