package co.indexify.dto;

import org.springframework.mail.javamail.JavaMailSender;

import java.io.Serializable;
import java.util.List;

public class EmailsDto implements Serializable {
    private List<EmailDto> emails;
    private JavaMailSender sender;

    public List<EmailDto> getEmails() {
        return emails;
    }

    public EmailsDto(List<EmailDto> emails, JavaMailSender sender) {
        this.emails = emails;
        this.sender = sender;
    }

    public void setEmails(List<EmailDto> emails) {
        this.emails = emails;
    }

    public JavaMailSender getSender() {
        return sender;
    }

    public void setSender(JavaMailSender sender) {
        this.sender = sender;
    }
}
