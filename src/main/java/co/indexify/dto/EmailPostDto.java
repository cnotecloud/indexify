package co.indexify.dto;

import com.ankurpathak.server.validation.constraints.Email;

import javax.validation.constraints.NotBlank;

public class EmailPostDto {


    @NotBlank
    private String subject;

    @NotBlank
    @Email
    private String to;

    @NotBlank
    private String body;

    @Email
    private String replyTo;


    public String getSubject() {
        return subject;
    }

    public String getTo() {
        return to;
    }

    public String getBody() {
        return body;
    }

    public String getReplyTo() {
        return replyTo;
    }


    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }
}
