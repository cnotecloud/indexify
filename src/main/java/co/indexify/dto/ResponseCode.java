package co.indexify.dto;

import co.indexify.domain.model.Organization;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Enumeration of REST Error types.
 * 
 *
 *
 *         Aug 3, 2016
 */
public enum ResponseCode {
    SUCCESS(0),
    GLOBAL(2),
    ERROR(-1),
    AUTHORIZATION(9),
    AUTHENTICATION(10), JWT_TOKEN_EXPIRED(11), VERIFICATION_TOKEN_EXPIRED(12),
    VALIDATION(13), EMAIL_ALREADY_EXIST(14), VERIFICATION_TOKEN_INVALID(15),
    QUERY_PARAM_MISSING(16), USER_DISABLED(17), VERIFIVATION_TOKEN_NOT_FOUND(18),
    FORGOT_PASSWORD_NOT_SENT(17), PASSWORD_RESET_TOKEN_EXPIRED(18), PASSWORD_RESET_TOKEN_INVALID(19),
    ORGANIZATION_NOT_CREATED(20), FILE_UPLOAD_MAX_FILE_SIZE_EXCEED(21), FILE_UPLOAD_MAX_REQUEST_SIZE_EXCEED(22),
    ORGANIZATION_EXIST(23), NODE_TREE_EXIST(24), ORGANIZATION_NOT_EXIST(25), PRODUCT_NOT_FOUND(26),
    ORGANIZATION_NOT_FOUND(27), USER_SCAN_NOT_CREATED(28), CONTACT_ALREADY_EXIST(29), PAGE_NOT_FOUND(30), USER_NOT_FOUND(31);
    private int errorCode;

    private ResponseCode(int errorCode) {
        this.errorCode = errorCode;
    }

    @JsonValue
    public int getErrorCode() {
        return errorCode;
    }
}
