package co.indexify.dto;

import com.ankurpathak.server.validation.constraints.Any;
import com.ankurpathak.server.validation.constraints.FileType;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


public class ResourceUploadDto {
    @NotNull
    @FileType
    private MultipartFile file;
    @NotBlank
    @Any({"IMAGE", "FILE"})
    private String type;
    private String tag;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
