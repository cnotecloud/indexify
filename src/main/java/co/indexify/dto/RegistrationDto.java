package co.indexify.dto;

import com.ankurpathak.server.validation.constraints.Contact;
import com.ankurpathak.server.validation.constraints.Email;
import com.ankurpathak.server.validation.constraints.NotContainWhitespace;
import com.ankurpathak.server.validation.constraints.PasswordMatches;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@PasswordMatches

public final class RegistrationDto {
    @NotBlank
    @NotContainWhitespace
    private String firstName;
    @NotBlank
    @NotContainWhitespace
    private String lastName;
    @Contact
    private String contact;
    @Size(min = 6, max = 15)
    @NotContainWhitespace
    @NotBlank
    private String password;
    @NotBlank
    private String matchingPassword;
    @NotBlank
    @Email
    private String email;
    private String tag;
    private String company;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}
