package co.indexify.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

public class ProductNodeLinkDto{
    @NotNull
    @NotEmpty
    List<@NotNull @Valid ProductNodeDto> links;


    public List<ProductNodeDto> getLinks() {
        return links;
    }

    public void setLinks(List<ProductNodeDto> links) {
        this.links = links;
    }
}
