package co.indexify.dto;

import co.indexify.domain.model.Organization;
import co.indexify.domain.model.Resource.ResourceType;
import org.springframework.web.multipart.MultipartFile;


public class ResourceDto {
    private MultipartFile file;
    private ResourceType type;
    private String tag;
    private Organization organization;


    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public ResourceType getType() {
        return type;
    }

    public void setType(ResourceType type) {
        this.type = type;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public final ResourceDto assignFile(MultipartFile file) {
        this.file = file;
        return this;
    }

    public final ResourceDto image() {
        this.type = ResourceType.IMAGE;
        return this;
    }

    public final ResourceDto tag(String tag) {
        this.tag = tag;
        return this;
    }

    public final ResourceDto organization(Organization organization) {
        this.organization = organization;
        return this;
    }


    public static final ResourceDto fromUploadDto(ResourceUploadDto dto) {
        ResourceDto resource;
        resource = new ResourceDto();
        resource.setTag(dto.getTag());
        resource.setFile(dto.getFile());
        String type = dto.getType();
        if (type != null) {
            switch (type) {
                case "FILE":
                    resource.setType(ResourceType.FILE);
                    break;


                case "IMAGE":
                    resource.setType(ResourceType.IMAGE);
                    break;

            }
        }

        return resource;
    }



}
