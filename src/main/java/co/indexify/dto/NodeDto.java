package co.indexify.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public final class NodeDto {
    @NotBlank
    private String displayName;
    @NotBlank
    private String id;
    private String parent;
    private String imgUrl;
    @NotNull
    private Boolean leaf = true;


    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Boolean getLeaf() {
        return leaf;
    }

    public void setLeaf(Boolean leaf) {
        this.leaf = leaf;
    }
}
