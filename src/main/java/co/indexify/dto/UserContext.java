package co.indexify.dto;

import co.indexify.domain.model.Role;
import co.indexify.domain.model.User;
import co.indexify.security.CustomUserDetails;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * <p>
 * Aug 4, 2016
 */
public class UserContext {
    private String username;
    private Collection<? extends GrantedAuthority> authorities;
    private CustomUserDetails userDetails;
    private Collection<String> roles;
    ;

    public UserContext(String username, Collection<String> roles, Collection<? extends GrantedAuthority> authorities, CustomUserDetails userDetails) {
        this.username = username;
        this.authorities = authorities;
        this.userDetails = userDetails;
        this.roles = roles;
    }


    @JsonGetter
    public Collection<String> getRoles() {
        return this.roles;
    }


    public static UserContext create(String username,Collection<String> roles, Collection<? extends GrantedAuthority> authorities, CustomUserDetails userDetails) {
        if (StringUtils.isBlank(username)) throw new IllegalArgumentException("Username is blank: " + username);
        return new UserContext(username,roles, authorities, userDetails);
    }

    public static final UserContext ANONYMOUS_USER_CONTEXT = UserContext.create(User.ANONYMOUS_USERNAME,Sets.newHashSet(Role.ROLE_ANONYMOUS), AuthorityUtils.createAuthorityList(Role.ANONYMOUS_ROLE.privileges().toArray()), CustomUserDetails.ANONYMOUS_CUSTOM_USER_DETAILS);


    public Long getOrganization() {
        if (userDetails != null && userDetails.getUser() != null && userDetails.getUser().getOrganization() != null)
            return userDetails.getUser().getOrganization().getId();
        else return null;
    }

    public Long getId() {
        if (userDetails != null && userDetails.getUser() != null)
            return userDetails.getUser().getId();
        else return null;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    public CustomUserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(CustomUserDetails userDetails) {
        this.userDetails = userDetails;
    }
}


