package co.indexify.dto;

import java.util.List;

public class EmailDto {
    private final String subject;
    private final String to;
    private final String from;
    private final String body;
    private final List<DataSourceDto> dataSources;
    private final List<String> ccs;
    private final String replyTo;

    public EmailDto(String subject, String to, String from, String body, List<DataSourceDto> dataSources, List<String> ccs, String replyTo) {
        this.subject = subject;
        this.to = to;
        this.from = from;
        this.body = body;
        this.dataSources = dataSources;
        this.ccs = ccs;
        this.replyTo = replyTo;
    }


    public String getSubject() {
        return subject;
    }

    public String getTo() {
        return to;
    }

    public String getFrom() {
        return from;
    }

    public String getBody() {
        return body;
    }

    public List<DataSourceDto> getDataSources() {
        return dataSources;
    }

    public List<String> getCcs() {
        return ccs;
    }

    public String getReplyTo() {
        return replyTo;
    }
}
