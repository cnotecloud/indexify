package co.indexify.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CatalogueStatusDto {

    @NotNull
    private Boolean organizationCreated = false;

    @NotNull
    private Boolean nodesCreated = false;

    @NotNull
    private Boolean productsCreated = false;

    @NotNull
    private Boolean nodesProductsMapped = false;


    public Boolean getOrganizationCreated() {
        return organizationCreated;
    }

    public void setOrganizationCreated(Boolean organizationCreated) {
        this.organizationCreated = organizationCreated;
    }

    public Boolean getNodesCreated() {
        return nodesCreated;
    }

    public void setNodesCreated(Boolean nodesCreated) {
        this.nodesCreated = nodesCreated;
    }

    public Boolean getProductsCreated() {
        return productsCreated;
    }

    public void setProductsCreated(Boolean productsCreated) {
        this.productsCreated = productsCreated;
    }

    public Boolean getNodesProductsMapped() {
        return nodesProductsMapped;
    }

    public void setNodesProductsMapped(Boolean nodesProductsMapped) {
        this.nodesProductsMapped = nodesProductsMapped;
    }
}
