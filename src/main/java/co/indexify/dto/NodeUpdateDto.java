package co.indexify.dto;

import javax.validation.constraints.NotBlank;

public class NodeUpdateDto {
    @NotBlank
    private String displayName;
    private String imgUrl;
    private Boolean leaf;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
    
    public Boolean getLeaf() {
        return leaf;
    }

    public void setLeaf(Boolean leaf) {
        this.leaf = leaf;
    }

}


//877787980
