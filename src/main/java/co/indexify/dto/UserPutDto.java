package co.indexify.dto;

import co.indexify.domain.model.Location;
import com.ankurpathak.server.validation.constraints.Contact;
import com.ankurpathak.server.validation.constraints.Email;
import com.ankurpathak.server.validation.constraints.NotContainWhitespace;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserPutDto {
    @NotBlank
    @NotContainWhitespace
    private String firstName;
    @NotBlank
    @NotContainWhitespace
    private String lastName;
    //@Contact
   // private String contact;

    //@NotBlank
    //@Email
    //private String email;


    @NotNull
    @Valid
    private LocationDto location;


    public LocationDto getLocation() {
        return location;
    }

    public void setLocation(LocationDto location) {
        this.location = location;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


}
