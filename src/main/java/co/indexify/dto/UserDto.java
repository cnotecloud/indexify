package co.indexify.dto;

import co.indexify.domain.model.CatalogueStatus;
import co.indexify.domain.model.Location;
import co.indexify.domain.model.User;
import com.ankurpathak.server.validation.constraints.*;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@JsonInclude(Include.NON_EMPTY)
public final class UserDto {
    @Email
    @NotBlank
    private String email;
    @Contact
    private String contact;
    @Size(min = 8, max = 30)
    @UsernamePattern
    @StartWithAlphaNumeric
    @NotContainConsecutivePeriod
    @NotContainConsecutiveUnderscore
    @NotContainPeriodFollowedByUnderscore
    @NotContainUnderscoreFollowedByPeriod
    @EndWithAlphaNumeric
    private String username;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    private String middleName;
    private Set roles;
    private CatalogueStatus catalogueStatus;
    private OrganizationGetDto organization;
    private String company;
    @Url
    private String imgUrl;
    private Set<String> tags;
    @Valid
    private Location location;
    private Long id;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Set getRoles() {
        return roles;
    }

    public void setRoles(Set roles) {
        this.roles = roles;
    }

    public CatalogueStatus getCatalogueStatus() {
        return catalogueStatus;
    }

    public void setCatalogueStatus(CatalogueStatus catalogueStatus) {
        this.catalogueStatus = catalogueStatus;
    }

    public OrganizationGetDto getOrganization() {
        return organization;
    }

    public void setOrganization(OrganizationGetDto organization) {
        this.organization = organization;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Set<String> getTags() {
        return tags;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }




    public  static final UserDto fromUser(User user) {
        UserDto dto = new UserDto();
        dto.setId(user.getId());
        dto.setUsername(user.getUsername());
        co.indexify.domain.model.Email var10001 = user.getEmail();
        dto.setEmail(var10001.getValue());
        co.indexify.domain.model.Contact var3 = user.getContact();
        dto.setContact(var3 != null ? var3.getValue() : null);
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());
        dto.setMiddleName(user.getMiddleName());
        dto.setRoles(user.getRoles());
        dto.setImgUrl(user.getImgUrl());
        dto.setLocation(user.getLocation());
        dto.setCompany(user.getCompany());
        dto.setCatalogueStatus(user.getCatalogueStatus());
        dto.setTags(user.getTags());
        return dto;
    }



}
