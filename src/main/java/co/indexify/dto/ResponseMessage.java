package co.indexify.dto;

public interface ResponseMessage {
    String MESSAGE_VALIDATION = "message.validation";
    String MESSAGE_EMAIL_EXISTS = "message.exists.email";
    String MESSAGE_USER_REGISTRATION_SUCCESS="message.user.registration.success";
    String MESSAGE_USER_REGISTRATION_CONFIRMED="message.user.registration.confirmed";
    String MESSAGE_USER_REGISTRATION_NOT_CONFIRMED="message.user.registration.notConfirmed";
    String MESSAGE_QUERY_PARAM_MISSING ="message.missing.queryParam";
    String MESSAGE_USER_DISABLED="message.user.disabled";
    String MESSAGE_USER_REGISTRATION_EMAIL_RESEND="message.user.registration.email.resend";
    String MESSAGE_USER_REGISTRATION_EMAIL_NOT_RESEND="message.user.registration.email.notResend";
    String MESSAGE_FORGOT_PASSWORD_EMAIL_SENT="message.user.forgetPassword.email.sent";
    String MESSAGE_FORGOT_PASSWORD_EMAIL_NOT_SENT="message.user.forgetPassword.email.notSent";
    String MESSAGE_CHANGE_PASSWORD_CONFIRMED ="message.user.changePassword.confirmed";
    String MESSAGE_CHANGE_PASSWORD_NOT_CONFIRMED ="message.user.changePassword.notConfirmed";
    String MESSAGE_SAVE_PASSWORD_SUCCESS="message.user.savePassword.success";
    String MESSAGE_ORGANIZATION_CREATE_SUCCESS="message.organization.create.success";
    String MESSAGE_ORGANIZATION_CREATE_ERROR="message.organization.create.error";
    String MESSAGE_FILE_UPLOAD_MAX_REQUEST_SIZE="message.fileUpload.maxRequestSize";
    String MESSAGE_FILE_UPLOAD_MAX_FILE_SIZE="message.fileUpload.maxFileSize";
    String MESSAGE_ORGANIZATION_EXISTS="message.exists.organization";
    String MESSAGE_NODE_TREE_EXISTS="message.exists.node";
    String MESSAGE_ORGANIZATION_NOT_EXISTS ="message.notExists.organization";
    String MESSAGE_PRODUCT_CREATE_SUCCESS="message.product.create.success";
    String MESSAGE_NODE_TREE_CREATE_SUCCESS="message.nodeTree.create.success";
    String MESSAGE_PRODUCT_UPDATE_SUCCESS="message.product.update.success";
    String MESSAGE_PRODUCT_NOT_FOUND="message.product.notFound";
    String MESSAGE_PRODUCT_NODE_LINK_SUCCESS ="message.product.node.link.success";
    String MESSAGE_PRODUCT_NODE_UNLINK_SUCCESS ="message.product.node.unlink.success";
    String MESSAGE_RESOURCE_UPLOAD_SUCCESS="message.resource.upload.success";
    String MESSAGE_USER_SCAN_CREATE_SUCCESS="message.userScan.create.success";
    String MESSAGE_USER_SCAN_CREATE_ERROR="message.userScan.create.error";
    String MESSAGE_ORGANIZATION_NOT_FOUND = "message.notFound.organization";
    String MESSAGE_CONTACT_EXISTS = "message.exists.contact";
    String MESSAGE_FAVOURITE_PRODUCT_CREATE_SUCCESS="message.favouriteProduct.create.success";
    String MESSAGE_FORBIDDEN = "message.forbidden";
    String MESSAGE_NOT_FOUND = "message.notFound";

    String MESSAGE_SUCCESS="message.success";

    String MESSAGE_EXIST = "message.exist";


    String MESSAGE_NOT_EXIST = "message.notExist";
    String MESSAGE_AUTHENTICATION = "message.authentication";
}
