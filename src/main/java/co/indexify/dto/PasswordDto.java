package co.indexify.dto;

import com.ankurpathak.server.validation.constraints.NotContainWhitespace;
import com.ankurpathak.server.validation.constraints.PasswordMatches;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@PasswordMatches

public class PasswordDto {
    @Size(min = 8, max = 30)
    @NotContainWhitespace
    private String password;
    @NotBlank
    private String matchingPassword;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }
}
