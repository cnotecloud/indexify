package co.indexify.dto;

import org.apache.commons.lang3.mutable.Mutable;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class NodesDto{

    @NotEmpty
    @NotNull
    List<@Valid @NotNull NodeDto> nodes;

    public List<NodeDto> getNodes() {
        return nodes;
    }

    public void setNodes(List<NodeDto> nodes) {
        this.nodes = nodes;
    }
}
