package co.indexify.dto;

import javax.validation.constraints.NotEmpty;

public class TagDto {

    @NotEmpty
    private String tag;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
