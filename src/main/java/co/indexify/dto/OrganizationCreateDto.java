package co.indexify.dto;

import co.indexify.domain.model.Location;
import com.ankurpathak.server.validation.constraints.Contact;
import com.ankurpathak.server.validation.constraints.Email;
import com.ankurpathak.server.validation.constraints.ImageType;
import com.ankurpathak.server.validation.constraints.Url;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


public class OrganizationCreateDto {
    @NotBlank
    private String displayName;
    @Valid
    @NotNull
    private LocationDto location;
    @Url
    private String website;
    @Email
    private String email;
    @Contact
    private String contact;
    private String description;
    @ImageType
    private MultipartFile image;
    private String tag;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public LocationDto getLocation() {
        return location;
    }

    public void setLocation(LocationDto location) {
        this.location = location;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
