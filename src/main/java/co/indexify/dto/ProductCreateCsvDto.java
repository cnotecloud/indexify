package co.indexify.dto;

import com.ankurpathak.server.validation.constraints.FileType;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

public class ProductCreateCsvDto {


    @FileType
    @NotNull
    private MultipartFile csv;


    private String tag;


    public MultipartFile getCsv() {
        return csv;
    }

    public void setCsv(MultipartFile csv) {
        this.csv = csv;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
