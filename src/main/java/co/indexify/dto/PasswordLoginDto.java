package co.indexify.dto;

import com.ankurpathak.server.validation.constraints.Password;
public class PasswordLoginDto {

    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
