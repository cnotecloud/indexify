package co.indexify.domain;

public interface DomainConstants {

    String PLATFORM_TYPE_INDEX_NAME = "platformsTypeIdx";
    String USER_EMAIL_INDEX_NAME = "usersEmailIdx";
    String USER_CONTACT_INDEX_NAME = "usersContactIdx";
    String USER_USERNAME_INDEX_NAME = "usersUsernameIdx";
    String USER_VERIFICATION_TOKEN_INDEX_NAME = "usersVerificationTokenIdx";
    String USER_PASSWORD_RESET_TOKEN_INDEX_NAME = "usersPasswordResetTokenIdx";
    String USER_SEQ = "USER_SEQ";
    String USER_COLLECTION = "users";
    String USER_CONTACT_INDEX_DEF = "{\"contact.value\" : 1} ";
    String USER_EMAIL_INDEX_DEF = "{\"email.value\" : 1} ";
    String USER_VERIFICATION_TOKEN_INDEX_DEF = "{\"verificationToken.value\" : 1} ";
    String USER_PASSWORD_RESET_TOKEN_INDEX_DEF = "{\"passwordResetToken.value\" : 1} ";
    String ORGANIZATION_EXTENSION_KEY = "organizationExtensionsKeyIdx";


}
