package co.indexify.domain.repository;

import co.indexify.domain.model.ReverseFavouriteProduct;
import co.indexify.domain.repository.custom.IReverseFavouriteProductRepositoryCustom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

@Repository(IReverseFavouriteProductRepository.REPOSITORY)
public interface IReverseFavouriteProductRepository extends MongoRepository<ReverseFavouriteProduct, String>, IReverseFavouriteProductRepositoryCustom {

    public static final String REPOSITORY = "reverseFavouriteProductRepository";
    public static final String REPOSITORY_IMPL = "reverseFavouriteProductRepositoryImpl";



    @PreAuthorize("isMyOrganization()")
    Page<ReverseFavouriteProduct> findByOrganizationId(@Param("organizationId") Long organizationId, Pageable pageable);

    @PreAuthorize("isMyOrganization()")
    Page<ReverseFavouriteProduct> findAll(Pageable pageable);


}
