package co.indexify.domain.repository;

import co.indexify.domain.model.Node;
import co.indexify.domain.repository.custom.INodeRepositoryCustom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository("nodeRepository")

public interface INodeRepository extends MongoRepository<Node, String>, INodeRepositoryCustom {
    public static  final String PARAM_PARENT_ID = "parentId";
    public static  final String PARAM_ID = "id";
    public static  final String REPOSITORY = "nodeRepository";
    public static  final String REPOSITORY_IMPL = "nodeRepositoryImpl";

    @PreAuthorize("isMyOrganizationOrPublicProfile()")
    Page<Node> findAll(Pageable pageable);

    @PreAuthorize("isMyOrganizationOrPublicProfile()")
    Page<Node> findByParentIsNull(Pageable pageable);

    @PreAuthorize("isMyOrganizationOrPublicProfile()")
    Page<Node> findByParentId(@Param("parentId") String parentId, Pageable pageable);

    @RestResource(exported = false)
    Optional<Node> findByIdAndLeafTrue(@Param("id") String id);

    @PreAuthorize("isMyOrganizationOrPublicProfile()")
    Page<Node> findByLeafTrue(Pageable pageable);


    @RestResource(exported = false)
    Long countByParentId(@Param("parentId") String parentId);


}
