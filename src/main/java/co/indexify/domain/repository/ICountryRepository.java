package co.indexify.domain.repository;

import co.indexify.domain.model.Country;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ICountryRepository extends MongoRepository<Country, String> {
    Page<Country> findAllByOrderByName(Pageable pageable);

    Page<Country> findByNameContainingIgnoreCase(@Param("name") String name, Pageable pageable);

    Optional<Country> findByIso2IgnoreCase(@Param("iso2") String iso2);
}
