package co.indexify.domain.repository;

import co.indexify.domain.model.Product;
import co.indexify.domain.repository.custom.IProductRepositoryCustom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository(IProductRepository.REPOSITORY)
public interface IProductRepository extends MongoRepository<Product, String>, IProductRepositoryCustom {


    public static final String PARAM_LEAF_ID = "leafId";
    public static final String PARAM_NAME = "name";
    public static final String PARAM_ID = "productId";


    @PreAuthorize("isMyOrganizationOrPublicProfile()")
    Page<Product> findAll(Pageable pageable);

    @PreAuthorize("isMyOrganizationOrPublicProfile()")
    Page<Product> findByLeafId(@Param("leafId") String leafId, Pageable pageable);

    @PreAuthorize("isMyOrganizationOrPublicProfile()")
    Page<Product> findByNameContainingIgnoreCase(@Param("name") String name, Pageable pageable);

    @PreAuthorize("isMyOrganizationOrPublicProfile()")
    Page<Product> findByLeafIdAndNameContainingIgnoreCase(@Param("leafId") String leafId, @Param("name") String name, Pageable pageable);

    @PreAuthorize("isMyOrganizationOrPublicProfile()")
    Optional<Product> getById(@Param("productId") String productId);

    @PreAuthorize("isMyOrganizationOrPublicProfile()")
    Page<Product> findByLeafIsNull(Pageable pageable);



    public static final String REPOSITORY = "productRepository";
    public static final String REPOSITORY_IMPL = "productRepositoryImpl";





}
