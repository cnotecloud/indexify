package co.indexify.domain.repository;

import co.indexify.domain.model.Event;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface IEventRepository extends MongoRepository<Event, String> {
    Page<Event> findByOrderByStartDesc(Pageable pageable);
    Page<Event> findByOrderByName(Pageable pageable);
}
