package co.indexify.domain.repository;

import co.indexify.domain.model.Notification;
import co.indexify.domain.model.UserScan;
import co.indexify.domain.repository.custom.INotificationRepositoryCustom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

@Repository(INotificationRepository.REPOSITORY)
public interface INotificationRepository extends MongoRepository<Notification, String>, INotificationRepositoryCustom {
    @PreAuthorize("isMe()")
    @Override
    Page<Notification> findAll(Pageable pageable);

    @PreAuthorize("isMe()")
    Page<Notification> findByOrderByCreatedDesc(Pageable pageable);


    String REPOSITORY = "notificationRepository";
    String REPOSITORY_IMPL = "notificationRepositoryImpl";
}
