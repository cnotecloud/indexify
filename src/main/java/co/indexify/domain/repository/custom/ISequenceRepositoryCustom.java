package co.indexify.domain.repository.custom;

public interface ISequenceRepositoryCustom  {
    long next(String id);
}
