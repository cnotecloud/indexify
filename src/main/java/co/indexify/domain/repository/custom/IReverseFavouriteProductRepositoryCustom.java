package co.indexify.domain.repository.custom;

import co.indexify.domain.model.ReverseFavouriteProduct;
import co.indexify.dto.OrganizationIdNameDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by ankur on 05-02-2017.
 */
public interface IReverseFavouriteProductRepositoryCustom {
    Page<OrganizationIdNameDto> getOrganizationIdName(Pageable pageable);
    ReverseFavouriteProduct persist(ReverseFavouriteProduct reverseFavouriteProduct, Long organizationId);
}
