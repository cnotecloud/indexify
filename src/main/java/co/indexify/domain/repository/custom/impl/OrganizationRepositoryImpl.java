package co.indexify.domain.repository.custom.impl;

import co.indexify.domain.model.FavouriteProduct;
import co.indexify.domain.model.Organization;
import co.indexify.domain.model.User;
import co.indexify.domain.repository.IOrganizationRepository;
import co.indexify.domain.repository.ISequenceRepository;
import co.indexify.domain.repository.custom.IOrganizationRepositoryCustom;
import co.indexify.domain.repository.custom.ISequenceRepositoryCustom;
import co.indexify.dto.OrganizationIdNameDto;
import co.indexify.util.SecurityUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Maps;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import org.apache.commons.collections.CollectionUtils;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

/**
 * Created by ankur on 05-02-2017.
 */
@Component(IOrganizationRepository.REPOSITORY_IMPL)
public class OrganizationRepositoryImpl implements IOrganizationRepositoryCustom {


    @Autowired
    private ISequenceRepository sequenceRepository;


    @Autowired
    private MongoTemplate mongoTemplate;


    @Override
    public Organization persist(final Organization organization) {
        organization.setId(sequenceRepository.next(Organization.SEQ));
        organization.setCreated(Calendar.getInstance().getTime());
        organization.setCreatedBy(SecurityUtil.getMe().orElse(User.ANONYMOUS_USER));
        try {
           mongoTemplate.save(organization);
           return organization;
        } catch (Exception ex) {
            return null;
        }
    }


    /*
    @Override
    public Page<OrganizationIdNameDto> getOrganizationIdName(Pageable pageable) {

        Aggregation aggregationCount = newAggregation(
                FavouriteProductRepositoryImpl::organizationToArrayProjection,
                FavouriteProductRepositoryImpl::organizationArrayToObjectProjection,
                FavouriteProductRepositoryImpl::organizationIdNameGroup,
                FavouriteProductRepositoryImpl::organizationIdNameProjection,
                FavouriteProductRepositoryImpl::organizationIdNameSort,
                count().as("count")
        );

        long total = getCount(aggregationCount);

        Aggregation aggregation = newAggregation(
                FavouriteProductRepositoryImpl::organizationToArrayProjection,
                FavouriteProductRepositoryImpl::organizationArrayToObjectProjection,
                FavouriteProductRepositoryImpl::organizationIdNameGroup,
                FavouriteProductRepositoryImpl::organizationIdNameProjection,
                FavouriteProductRepositoryImpl::organizationIdNameSort,
                skip((long)pageable.getPageNumber() * (long)pageable.getPageSize()),
                limit(pageable.getPageSize())
        );


        List<OrganizationIdNameDto> results =  getList(aggregation, OrganizationIdNameDto.class);
        return new PageImpl<>(results, pageable, total);

    } */



    private <T> List<T> getList(Aggregation aggregation, Class<T> clazz){
        return mongoTemplate.aggregate(aggregation, FavouriteProduct.class, clazz).getMappedResults();
    }

    private Long getCount(Aggregation aggregation){
        List<Document> results = mongoTemplate.aggregate(aggregation, FavouriteProduct.class, Document.class).getMappedResults();
        if(CollectionUtils.isNotEmpty(results)){
            Document result = results.get(0);
            return  ((Integer)result.get("count")).longValue();
        }else {
            return 0L;
        }
    }


}
