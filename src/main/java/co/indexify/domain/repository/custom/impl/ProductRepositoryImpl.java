package co.indexify.domain.repository.custom.impl;


import co.indexify.domain.model.Product;
import co.indexify.domain.repository.IProductRepository;
import co.indexify.domain.repository.custom.IProductRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

@Component(IProductRepository.REPOSITORY_IMPL)
public class ProductRepositoryImpl implements IProductRepositoryCustom {

    @Autowired
    private MongoTemplate mongoTemplate;



    @Override
    public void unassignLeaf(String leafId){
        Query query = new Query();
        query.addCriteria(Criteria.where("leaf.$id").is(leafId));
        Update update = new Update();
        update.unset("leaf");
        mongoTemplate.updateMulti(query, update, Product.class);
    }
}
