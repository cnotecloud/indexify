package co.indexify.domain.repository.custom;

import co.indexify.domain.model.Node;
import co.indexify.domain.model.User;

/**
 * Created by ankur on 05-02-2017.
 */
public interface INodeRepositoryCustom {
    Node persist(Node node);
    void makeParentLeaf(Node node);
    void makeNonLeaf(Node node);
}
