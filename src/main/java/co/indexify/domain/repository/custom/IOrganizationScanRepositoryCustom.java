package co.indexify.domain.repository.custom;

import co.indexify.domain.model.Organization;
import co.indexify.domain.model.OrganizationScan;

public interface IOrganizationScanRepositoryCustom {
    OrganizationScan persist(OrganizationScan organizationScan);
}
