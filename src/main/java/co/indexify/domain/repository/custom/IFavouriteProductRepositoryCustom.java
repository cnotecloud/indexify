package co.indexify.domain.repository.custom;

import co.indexify.dto.OrganizationIdNameDto;
import com.mongodb.DBObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;

import java.util.Map;

/**
 * Created by ankur on 05-02-2017.
 */
public interface IFavouriteProductRepositoryCustom {
    Page<OrganizationIdNameDto> getOrganizationIdName(Pageable pageable);
}
