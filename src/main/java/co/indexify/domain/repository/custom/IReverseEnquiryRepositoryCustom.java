package co.indexify.domain.repository.custom;

import co.indexify.domain.model.OrganizationScan;
import co.indexify.domain.model.ReverseEnquiry;

public interface IReverseEnquiryRepositoryCustom {
    ReverseEnquiry persist(ReverseEnquiry reverseEnquiry);
}
