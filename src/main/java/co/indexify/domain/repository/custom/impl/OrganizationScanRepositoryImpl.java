package co.indexify.domain.repository.custom.impl;

import co.indexify.domain.model.Organization;
import co.indexify.domain.model.OrganizationScan;
import co.indexify.domain.repository.IOrganizationScanRepository;
import co.indexify.domain.repository.custom.IOrganizationRepositoryCustom;
import co.indexify.domain.repository.custom.IOrganizationScanRepositoryCustom;
import co.indexify.util.SecurityUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

@Component(IOrganizationScanRepository.REPOSITORY_IMPL)
public class OrganizationScanRepositoryImpl implements IOrganizationScanRepositoryCustom {

    @Autowired
    private MongoTemplate mongoTemplate;


    @Override
    public OrganizationScan persist(OrganizationScan organizationScan) {
        try{
            mongoTemplate.save(organizationScan, String.format(COLLECTION, SecurityUtil.getOrganization()));
            return organizationScan;
        }catch (Exception ex){
            return null;
        }
    }

    public static final String COLLECTION = "organizationScans%s";


}
