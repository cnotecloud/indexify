package co.indexify.domain.repository.custom.impl;

import co.indexify.domain.model.Notification;
import co.indexify.domain.repository.INotificationRepository;
import co.indexify.domain.repository.ISequenceRepository;
import co.indexify.domain.repository.custom.INotificationRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

/**
 * Created by ankur on 05-02-2017.
 */
@Component(INotificationRepository.REPOSITORY_IMPL)
public class NotificationRepositoryImpl implements INotificationRepositoryCustom {


    @Autowired
    private ISequenceRepository sequenceRepository;


    @Autowired
    private MongoTemplate mongoTemplate;


    @Override
    public Notification persist(final Notification notification, Long userId) {
        try {
            mongoTemplate.save(notification, String.format(COLLECTION, String.valueOf(userId)));
            return notification;
        } catch (Exception ex) {
            return null;
        }
    }


    public static final String COLLECTION = "notifications%s";

}
