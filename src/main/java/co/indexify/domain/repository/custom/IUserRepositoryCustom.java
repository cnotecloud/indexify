package co.indexify.domain.repository.custom;

import co.indexify.domain.model.User;

import java.util.Date;

/**
 * Created by ankur on 05-02-2017.
 */
public interface IUserRepositoryCustom {
    User byEmail(String email);
    User persist(User user);
    void deleteVerificationTokenVerified();
    void deletePasswordResetAllExpiredSince(Date now);
}
