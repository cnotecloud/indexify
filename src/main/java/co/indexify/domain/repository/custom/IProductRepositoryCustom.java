package co.indexify.domain.repository.custom;

public interface IProductRepositoryCustom {
    void unassignLeaf(String leafId);
}
