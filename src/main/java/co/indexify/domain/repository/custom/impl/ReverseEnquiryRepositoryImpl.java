package co.indexify.domain.repository.custom.impl;

import co.indexify.domain.model.ReverseEnquiry;
import co.indexify.domain.repository.IReverseEnquiryRepository;
import co.indexify.domain.repository.custom.IReverseEnquiryRepositoryCustom;
import co.indexify.util.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

@Component(IReverseEnquiryRepository.REPOSITORY_IMPL)
public class ReverseEnquiryRepositoryImpl implements IReverseEnquiryRepositoryCustom {

    @Autowired
    private MongoTemplate mongoTemplate;


    @Override
    public ReverseEnquiry persist(ReverseEnquiry reverseEnquiry) {
        try{
            mongoTemplate.save(reverseEnquiry, String.format(COLLECTION, SecurityUtil.getOrganization()));
            return reverseEnquiry;
        }catch (Exception ex){
            return null;
        }
    }

    public static final String COLLECTION = "reverseEnquiries%s";


}
