package co.indexify.domain.repository.custom.impl;

import co.indexify.domain.model.Sequence;
import co.indexify.domain.repository.ISequenceRepository;
import co.indexify.domain.repository.custom.ISequenceRepositoryCustom;
import co.indexify.exception.SequenceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;


@Component(ISequenceRepository.REPOSITORY_IMPL)
public class SequenceRepositoryImpl implements ISequenceRepositoryCustom {
    public static final String MESSAGE_SEQUENCE_PROBLEM = "Unable to get next for sequence %s";

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private MongoOperations mongoOperation;



    @Override
    public long next(String id)  {

        //get sequence id
        Query query = new Query(Criteria.where(Sequence.FIELD_ID).is(id));

        //increase sequence id by 1
        Update update = new Update();
        update.inc(Sequence.FIELD_CURRENT, 1);

        //return new increased id
        FindAndModifyOptions options = new FindAndModifyOptions();
        options.returnNew(true);

        //this is the magic happened.
        Sequence seq =
                mongoOperation.findAndModify(query, update, options, Sequence.class);

        //if no id, throws SequenceException
        //optional, just a way to tell user when the sequence id is failed to generate.
        if (seq == null) {
            throw new SequenceException(String.format(MESSAGE_SEQUENCE_PROBLEM, id));
        }

        return seq.getCurr();

    }

}
