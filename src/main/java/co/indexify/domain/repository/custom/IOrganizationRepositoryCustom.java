package co.indexify.domain.repository.custom;

import co.indexify.domain.model.Organization;

import java.util.Optional;

/**
 * Created by ankur on 05-02-2017.
 */
public interface IOrganizationRepositoryCustom {

    /*
    User byEmail(String email);
    List<User> byFirstName(String firstName);
    List<User> byFirstNameEndingWith(String firstName);
    List<User> byFirstNameStartingWith(String firstName);
    List<User> byFirstNameContaining(String firstName);
    List<User> byAgeBetween(BigInteger lower, BigInteger higher);
    List<User> byOrderAgeAsc();
    List<User> byPage(BigInteger pageNo, BigInteger pageSize);

    */
    Organization persist(Organization organization);

}
