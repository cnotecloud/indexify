package co.indexify.domain.repository.custom.impl;

import co.indexify.domain.model.FavouriteProduct;
import co.indexify.domain.repository.IFavouriteProductRepository;
import co.indexify.domain.repository.custom.IFavouriteProductRepositoryCustom;
import co.indexify.dto.OrganizationIdNameDto;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.apache.commons.collections.CollectionUtils;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperationContext;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

/**
 * Created by ankur on 05-02-2017.
 */
@Component(IFavouriteProductRepository.REPOSITORY_IMPL)
class FavouriteProductRepositoryImpl implements IFavouriteProductRepositoryCustom {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Page<OrganizationIdNameDto> getOrganizationIdName(Pageable pageable) {

        Aggregation aggregationCount = newAggregation(
                FavouriteProductRepositoryImpl::organizationToArrayProjection,
                FavouriteProductRepositoryImpl::organizationArrayToObjectProjection,
                FavouriteProductRepositoryImpl::organizationIdNameGroup,
                FavouriteProductRepositoryImpl::organizationIdNameProjection,
                FavouriteProductRepositoryImpl::organizationIdNameSort,
                count().as("count")
        );

        long total = getCount(aggregationCount);

        Aggregation aggregation = newAggregation(
                FavouriteProductRepositoryImpl::organizationToArrayProjection,
                FavouriteProductRepositoryImpl::organizationArrayToObjectProjection,
                FavouriteProductRepositoryImpl::organizationIdNameGroup,
                FavouriteProductRepositoryImpl::organizationIdNameProjection,
                FavouriteProductRepositoryImpl::organizationIdNameSort,
                skip((long)pageable.getPageNumber() * (long)pageable.getPageSize()),
                limit(pageable.getPageSize())
        );


       List<OrganizationIdNameDto> results =  getList(aggregation, OrganizationIdNameDto.class);
        return new PageImpl<>(results, pageable, total);

    }


    private <T> List<T> getList(Aggregation aggregation, Class<T> clazz){
        return mongoTemplate.aggregate(aggregation, FavouriteProduct.class, clazz).getMappedResults();
    }

    private Long getCount(Aggregation aggregation){
        List<Document> results = mongoTemplate.aggregate(aggregation, FavouriteProduct.class, Document.class).getMappedResults();
        if(CollectionUtils.isNotEmpty(results)){
            Document result = results.get(0);
            return  ((Integer)result.get("count")).longValue();
        }else {
            return 0L;
        }
    }

    private static Document organizationIdNameSort(AggregationOperationContext aggregationOperationContext) {
        return new Document("$sort", new Document("organizationId", 1)
                                            .append("organizationName", 1)
        );
    }

    private static Document organizationIdNameProjection(AggregationOperationContext aggregationOperationContext) {
        return new Document("$project", new Document("organizationId", "$_id.organizationId")
                                                .append("organizationName", "$_id.organizationName")
                                                .append("_id", false)

        );
    }

    private static Document organizationIdNameGroup(AggregationOperationContext aggregationOperationContext) {
        return new Document("$group",
                new Document("_id", new Document("organizationId", "$organizationId")
                                            .append("organizationName", "$organizationName")
                )

        );
    }

    private static Document organizationArrayToObjectProjection(AggregationOperationContext aggregationOperationContext) {
        return new Document("$project",
                new Document("organizationId",
                        new Document("$arrayElemAt", new BasicDBList(){{addAll(Arrays.asList("$organization.v", 1));}})
                )
                .append("organizationName", "$organizationName")
        );
    }

    private static Document organizationToArrayProjection(AggregationOperationContext aggregationOperationContext) {
        return new Document("$project",
                new Document("organization",
                        new Document("$objectToArray", "$$ROOT.organization"
                        )
                )
                .append("_id", false)
                .append("organizationName", "$organizationName")

        );


    }

}
