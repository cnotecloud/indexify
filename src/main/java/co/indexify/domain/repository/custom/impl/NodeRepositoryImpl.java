package co.indexify.domain.repository.custom.impl;

import co.indexify.domain.model.Node;
import co.indexify.domain.model.User;
import co.indexify.domain.repository.INodeRepository;
import co.indexify.domain.repository.ISequenceRepository;
import co.indexify.domain.repository.custom.INodeRepositoryCustom;
import co.indexify.domain.repository.custom.IUserRepositoryCustom;
import co.indexify.util.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import java.util.Calendar;

/**
 * Created by ankur on 05-02-2017.
 */
@Component(INodeRepository.REPOSITORY_IMPL)
public class NodeRepositoryImpl implements INodeRepositoryCustom {

    @Autowired
    private MongoTemplate mongoTemplate;


    @Override
    public Node persist(Node node) {
        node.setCreated(Calendar.getInstance().getTime());
        node.setCreatedBy(SecurityUtil.getMe().orElse(User.ANONYMOUS_USER));
        try {
            mongoTemplate.insert(node);
            return node;
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public void makeParentLeaf(Node node){
        if(node.getParent() != null){
            setLeaf(node.getParent().getId(), true);
            node.getParent().setLeaf(true);
        }
    }


    private void setLeaf(String nodeId, Boolean leaf){
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(nodeId));
        Update update = new Update();
        update.set("leaf", leaf);
        mongoTemplate.updateFirst(query, update, Node.class);
    }

    @Override
    public void makeNonLeaf(Node node){
        setLeaf(node.getId(), false);
        node.setLeaf(false);
    }


}
