package co.indexify.domain.repository.custom.impl;

import co.indexify.domain.DomainConstants;
import co.indexify.domain.model.Organization;
import co.indexify.domain.model.User;
import co.indexify.domain.repository.ISequenceRepository;
import co.indexify.domain.repository.IUserRepository;
import co.indexify.domain.repository.custom.IUserRepositoryCustom;
import co.indexify.util.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by ankur on 05-02-2017.
 */
@Component(IUserRepository.REPOSITORY_IMPL)
public class UserRepositoryImpl implements IUserRepositoryCustom {

    @Autowired
    private MongoTemplate mongoTemplate;


    @Autowired
    private ISequenceRepository sequenceRepository;


    @Override
    public User byEmail(String email) {
        Query query = new Query();
        query.addCriteria(Criteria.where("email").is(email));
        return mongoTemplate.findOne(query, User.class);
    }

    @Override
    public User persist(final User user) {
        user.setId(sequenceRepository.next(DomainConstants.USER_SEQ));
        try {
            mongoTemplate.insert(user);
            return user;
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public void deleteVerificationTokenVerified(){
        Criteria criteria = Criteria.where("verificationToken").exists(true).and("enabled").is(true);
        Update update = new Update().unset("verificationToken");
        Query query = new Query(criteria);
        mongoTemplate.updateMulti(query, update, User.class);
    }

    @Override
    public void deletePasswordResetAllExpiredSince(Date now){
        Criteria criteria = Criteria.where("verificationToken").exists(true).and("passwordResetToken.expiryDate").lt(now);
        Update update = new Update().unset("passwordResetToken");
        Query query = new Query(criteria);
        mongoTemplate.updateMulti(query, update, User.class);
    }



}
