package co.indexify.domain.repository.custom.impl;

import co.indexify.domain.model.OrganizationScan;
import co.indexify.domain.model.UserVisitor;
import co.indexify.domain.repository.IOrganizationScanRepository;
import co.indexify.domain.repository.IUserVisitorRepository;
import co.indexify.domain.repository.IUserVisitorRepositoryCustom;
import co.indexify.util.SecurityUtil;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

@Component(IUserVisitorRepository.REPOSITORY_IMPL)
public class UserVisitorRepositoryImpl implements IUserVisitorRepositoryCustom {

    @Autowired
    private MongoTemplate mongoTemplate;


    @Override
    public UserVisitor persist(UserVisitor userVisitor, Long userId) {
        try{
            mongoTemplate.save(userVisitor, String.format(COLLECTION, String.valueOf(userId)));
            return userVisitor;
        }catch (Exception ex){
            return null;
        }
    }

    public static final String COLLECTION = "userVisitors%s";


}
