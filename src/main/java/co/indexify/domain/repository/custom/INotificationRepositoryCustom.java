package co.indexify.domain.repository.custom;

import co.indexify.domain.model.Notification;
import co.indexify.domain.model.Organization;

/**
 * Created by ankur on 05-02-2017.
 */
public interface INotificationRepositoryCustom {

    Notification persist(Notification notification, Long userId);

}
