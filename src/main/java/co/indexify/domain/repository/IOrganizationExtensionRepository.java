package co.indexify.domain.repository;

import co.indexify.domain.model.OrganizationExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Optional;

public interface IOrganizationExtensionRepository extends MongoRepository<OrganizationExtension, String> {

    @PreAuthorize("isMyOrganizationOrPublicProfile()")
    Page<OrganizationExtension> findByTags(@Param("tag") String tag, Pageable pageable);

    @PreAuthorize("isMyOrganizationOrPublicProfile()")
    Optional<OrganizationExtension> findByKey(@Param("key") String key);
}
