package co.indexify.domain.repository;

import co.indexify.domain.model.ReverseEnquiry;
import co.indexify.domain.repository.custom.IReverseEnquiryRepositoryCustom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;


@Repository(IReverseEnquiryRepository.REPOSITORY)
public interface IReverseEnquiryRepository extends MongoRepository<ReverseEnquiry, String>, IReverseEnquiryRepositoryCustom {


    @Override
    @PreAuthorize("isMyOrganization()")
    Page<ReverseEnquiry> findAll(Pageable pageable);

    String REPOSITORY = "reverseEnquiryRepository";
    String REPOSITORY_IMPL = "reverseEnquiryRepositoryImpl";

}
