package co.indexify.domain.repository;

import co.indexify.domain.model.Role;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IRoleRepository extends MongoRepository<Role, String> {
    Role findByName(@Param("name") String name);
}
