package co.indexify.domain.repository;

import co.indexify.domain.model.Resource;
import co.indexify.domain.model.Resource.ResourceType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IResourceRepository extends MongoRepository<Resource, String> {
    @PreAuthorize("isMyOrganization()")
    Page<Resource> findByType(@Param("type") ResourceType type, Pageable pageable);

    @PreAuthorize("isMyOrganization()")
    Page<Resource> findAll(Pageable pageable);


    @PreAuthorize("isMyOrganization()")
    Page<Resource> findByTagsAndType(@Param("tag") String tag, @Param("type") String type, Pageable pageable);
}
