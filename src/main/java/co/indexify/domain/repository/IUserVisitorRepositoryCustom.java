package co.indexify.domain.repository;

import co.indexify.domain.model.OrganizationScan;
import co.indexify.domain.model.UserVisitor;

public interface IUserVisitorRepositoryCustom {
    UserVisitor persist(UserVisitor userVisitor, Long userId);
}
