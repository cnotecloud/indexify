package co.indexify.domain.repository;

import co.indexify.domain.model.Sequence;
import co.indexify.domain.repository.custom.ISequenceRepositoryCustom;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository(ISequenceRepository.REPOSITORY)

public interface ISequenceRepository extends MongoRepository<Sequence, String>, ISequenceRepositoryCustom {


    public static final String REPOSITORY = "sequenceRepository";
    public static final String REPOSITORY_IMPL = "sequenceRepositoryImpl";


}
