package co.indexify.domain.repository;

import co.indexify.domain.model.OrganizationScan;
import co.indexify.domain.model.UserVisitor;
import co.indexify.domain.repository.custom.IOrganizationScanRepositoryCustom;
import co.indexify.dto.MethodSecurityDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.method.P;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

@Repository(IUserVisitorRepository.REPOSITORY)
public interface IUserVisitorRepository extends MongoRepository<UserVisitor, String>, IUserVisitorRepositoryCustom{

    @PreAuthorize(MethodSecurityDto.ME)
    @Override
    Page<UserVisitor> findAll(Pageable pageable);

    @PreAuthorize(MethodSecurityDto.ME)
    Page<UserVisitor> findByOrderByCreatedDesc(Pageable pageable);

    @PreAuthorize(MethodSecurityDto.ME)
    Page<UserVisitor> findByNameContainingIgnoreCaseOrderByCreatedDesc(@Param("name") String name, Pageable pageable);


    @PreAuthorize(MethodSecurityDto.ME)
    Page<UserVisitor> findByTagsOrderByCreatedDesc(@Param("tag") String tag, Pageable pageable);

    @PreAuthorize(MethodSecurityDto.ME)
    Page<UserVisitor> findByTagsAndNameContainingIgnoreCaseOrderByCreatedDesc(@Param("tag") String tag, @Param("name") String name, Pageable  pageable);




    String REPOSITORY = "userVisitorRepository";
        String REPOSITORY_IMPL = "userVisitorRepositoryImpl";

}

