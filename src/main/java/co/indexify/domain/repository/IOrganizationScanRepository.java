package co.indexify.domain.repository;

import co.indexify.domain.model.OrganizationScan;
import co.indexify.domain.repository.custom.IOrganizationScanRepositoryCustom;
import co.indexify.dto.MethodSecurityDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.method.P;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

@Repository(IOrganizationScanRepository.REPOSITORY)
public interface IOrganizationScanRepository extends MongoRepository<OrganizationScan, String>, IOrganizationScanRepositoryCustom {
    @Override
    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    Page<OrganizationScan> findAll(Pageable pageable);

    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    Page<OrganizationScan> findByOrderByCreatedDesc(Pageable pageable);

    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    Page<OrganizationScan> findByNameContainingIgnoreCaseOrderByCreatedDesc(@Param("name") String name, Pageable pageable);


    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    Page<OrganizationScan> findByTagsOrderByCreatedDesc(@Param("tag") String tag, Pageable pageable);

    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    Page<OrganizationScan> findByTagsAndNameContainingIgnoreCaseOrderByCreatedDesc(@Param("tag") String tag, @Param("name") String name, Pageable pageable);


    String REPOSITORY = "organizationScanRepository";
    String REPOSITORY_IMPL = "organizationScanRepositoryImpl";

}

