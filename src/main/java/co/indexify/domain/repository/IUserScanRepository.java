package co.indexify.domain.repository;

import co.indexify.domain.model.UserScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserScanRepository extends MongoRepository<UserScan, String> {
    @PreAuthorize("isMe()")
    Page<UserScan> findAll(Pageable pageable);

    @PreAuthorize("isMe()")
    Page<UserScan> findByOrderByCreatedDesc(Pageable pageable);

    @PreAuthorize("isMe()")
    Page<UserScan> findByNameContainingIgnoreCaseOrderByCreatedDesc(@Param("name") String name, Pageable pageable);


    //Now

    @PreAuthorize("isMe()")
    Page<UserScan> findByNameContainingIgnoreCaseAndOrganizationIsNullOrderByCreatedDesc(@Param("name") String name, Pageable pageable);


    @PreAuthorize("isMe()")
    Page<UserScan> findByNameContainingIgnoreCaseAndUserIsNullOrderByCreatedDesc(@Param("name") String name, Pageable pageable);

    @PreAuthorize("isMe()")
    Page<UserScan> findByOrganizationIsNullOrderByCreatedDesc(Pageable pageable);


    @PreAuthorize("isMe()")
    Page<UserScan> findByUserIsNullOrderByCreatedDesc(Pageable pageable);


    //Now




    @PreAuthorize("isMe()")
    Page<UserScan> findByTagsOrderByCreatedDesc(@Param("tag") String tag, Pageable pageable);

    @PreAuthorize("isMe()")
    Page<UserScan> findByTagsAndNameContainingIgnoreCaseOrderByCreatedDesc(@Param("tag") String tag, @Param("name") String name, Pageable pageable);

    @PreAuthorize("isMe()")
    Page<UserScan> findByTagsAndUserIsNullOrderByCreatedDesc(@Param("tag") String tag, Pageable pageable);

    @PreAuthorize("isMe()")
    Page<UserScan> findByTagsAndNameContainingIgnoreCaseAndUserIsNullOrderByCreatedDesc(@Param("tag") String tag, @Param("name") String name, Pageable pageable);

    @PreAuthorize("isMe()")
    Page<UserScan> findByTagsAndOrganizationIsNullOrderByCreatedDesc(@Param("tag") String tag, Pageable pageable);

    @PreAuthorize("isMe()")
    Page<UserScan> findByTagsAndNameContainingIgnoreCaseAndOrganizationIsNullOrderByCreatedDesc(@Param("tag") String tag, @Param("name") String name, Pageable pageable);
}
