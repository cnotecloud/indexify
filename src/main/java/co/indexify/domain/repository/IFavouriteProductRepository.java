package co.indexify.domain.repository;

import co.indexify.domain.model.FavouriteProduct;
import co.indexify.domain.repository.custom.IFavouriteProductRepositoryCustom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

@Repository(IFavouriteProductRepository.REPOSITORY)
public interface IFavouriteProductRepository extends MongoRepository<FavouriteProduct, String>, IFavouriteProductRepositoryCustom {
    public static final String REPOSITORY = "favouriteProductRepository";
    public static final String REPOSITORY_IMPL = "favouriteProductRepositoryImpl";

    @PreAuthorize("isMe()")
    Page<FavouriteProduct> findByOrganizationId(@Param("organizationId")  Long organizationId, Pageable pageable);

    @PreAuthorize("isMe()")
    Page<FavouriteProduct> findAll(Pageable pageable);




}
