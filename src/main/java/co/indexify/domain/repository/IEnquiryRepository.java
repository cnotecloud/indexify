package co.indexify.domain.repository;

import co.indexify.domain.model.Enquiry;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;


@Repository
public interface IEnquiryRepository extends MongoRepository<Enquiry, String> {

    @Override
    @PreAuthorize("isMe()")
    Page<Enquiry> findAll(Pageable pageable);

}
