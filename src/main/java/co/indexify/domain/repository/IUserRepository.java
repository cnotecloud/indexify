package co.indexify.domain.repository;

import co.indexify.domain.model.User;
import co.indexify.domain.repository.custom.IUserRepositoryCustom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.stream.Stream;

@Repository(IUserRepository.REPOSITORY)

public interface IUserRepository extends MongoRepository<User, Long>, IUserRepositoryCustom {


    public static final String REPOSITORY = "userRepository";
    public static final String REPOSITORY_IMPL = "userRepositoryImpl";


    @RestResource(exported = false)
    @Query("{\"email.value\" : ?0 }")
    Optional<User> findByEmail(@Param("email") String email);

    @RestResource(exported = false)
    Optional<User> findByUsername(@Param("username") String username);

    @RestResource(exported = false)
    @Query(" {\"contact.value\": { $exists : true, $eq : ?0 } } ")
    Optional<User> findByContact(@Param("contact") String contact);

    @RestResource(exported = false)
    Stream<User> findByOrganizationId(@Param("organizationId") Long organizationId);

    @RestResource(exported = false)
    @Query("{\"verificationToken.value\" : {$exists : true, $eq : ?0 } }")
    Optional<User> findByVerificationToken(@Param("verificationToken") String verificationToken);

    @RestResource(exported = false)
    @Query("{\"passwordResetToken.value\" : {$exists : true, $eq : ?0 } }")
    Optional<User> findByPasswordResetToken(@Param("passwordResetToken") String passwordResetToken);

    @Query("\n        { $or: [\n            {\"username\": {$exists : true, $eq : ?0 } },\n            {\"email.value\": ?0 },\n            { \"contact.value\" : {$exists : true, $eq : ?0 } },\n            { \"verificationToken.value\" : {$exists : true, $eq : ?0 } },\n            { \"passwordResetToken.value\" : {$exists : true, $eq : ?0 } } ]\n        }\n    ")
    @RestResource(exported = false)
    Optional<User> findByCandidateKey(@Param("candidateKey") String candidateKey);

    @PostAuthorize("returnObject?.orElse(null)?.id == principal?.userDetails?.user?.id || returnObject?.orElse(null)?.publicProfile == true")
    Optional<User> getById(@Param("id") Long id);

    @Query("{ tags: ?0 , publicProfile: true } ")
    Page<User> findByTagsAndPublicProfileTrue(@Param("tag") String tag, Pageable pageable);

    @Query("{\n    $and: [{\"tags\" : ?0}, {\"publicProfile\":true},\n    {\n        $or: [{\"firstName\" : {$regex: ?1 ,$options : \"i\"}},\n             {\"middleName\" : {$exists:true, $regex: ?1,$options: \"i\"} },\n             {\"lastName\" : {$regex: ?1 ,$options: \"i\"}}]}]\n\n}")
    Page<User> findByTagsAndNameContainingIgnoreCaseAndPublicProfileTrue(@Param("tag") String tag, @Param("name") String name, Pageable pageable);


}


