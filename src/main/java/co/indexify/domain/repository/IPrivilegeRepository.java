package co.indexify.domain.repository;

import co.indexify.domain.model.Privilege;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IPrivilegeRepository extends MongoRepository<Privilege, String> {
    Optional<Privilege> findByName(@Param("name") String name);
}
