package co.indexify.domain.repository;

import co.indexify.domain.model.Platform;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface IPlatformRepository extends MongoRepository<Platform, String> {
    Optional<Platform> findByType(String type);
}
