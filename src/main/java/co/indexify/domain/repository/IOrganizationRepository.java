package co.indexify.domain.repository;

import co.indexify.domain.model.Organization;
import co.indexify.domain.repository.custom.IOrganizationRepositoryCustom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository(IOrganizationRepository.REPOSITORY)
public interface IOrganizationRepository extends MongoRepository<Organization, Long>, IOrganizationRepositoryCustom {
    public static final String REPOSITORY = "organizationRepository";
    public static final String REPOSITORY_IMPL = "organizationRepositoryImpl";

    @PostAuthorize("returnObject?.orElse(null)?.id == principal?.userDetails?.user?.organization?.id || returnObject?.orElse(null)?.publicProfile == true")
    Optional<Organization> getById(@Param("organizationId") Long organizationId);

    //@Query("{ tags: ?0 , publicProfile: true } ")
    Page<Organization> findByTagsAndPublicProfileTrue(@Param("tag") String tag, Pageable pageable);

    Page<Organization> findByTagsAndPublicProfileTrueOrderByDisplayNameAsc(@Param("tag") String tag, Pageable pageable);


    Page<Organization> findByTagsAndDisplayNameContainingIgnoreCaseAndPublicProfileTrue(@Param("tag") String tag, @Param("displayName") String displayName, Pageable pageable);

    Page<Organization> findByTagsAndDisplayNameContainingIgnoreCaseAndPublicProfileTrueOrderByDisplayNameAsc(@Param("tag") String tag, @Param("displayName") String displayName, Pageable pageable);


    Page<Organization> findByTagsAndDescriptionContainingIgnoreCaseAndPublicProfileTrue(@Param("tag") String tag, @Param("description") String description, Pageable pageable);
}
