package co.indexify.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.querydsl.core.annotations.QueryEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "sequences")
@QueryEntity
@JsonInclude(Include.NON_EMPTY)
public final class Sequence extends Domain implements Serializable {
    private Long curr;
    public static final String FIELD_ID = "_id";
    public static final String FIELD_CURRENT = "curr";

    public Long getCurr() {
        return curr;
    }

    public void setCurr(Long curr) {
        this.curr = curr;
    }
}
