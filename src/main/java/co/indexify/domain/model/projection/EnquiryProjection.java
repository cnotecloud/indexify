package co.indexify.domain.model.projection;

import co.indexify.domain.model.Enquiry;
import co.indexify.domain.model.UserScan;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.Set;

@Projection(name = "detail", types = {Enquiry.class})
@JsonInclude(Include.NON_EMPTY)

public interface EnquiryProjection extends DomainProjection<String> {

    @Value("#{target?.organization?.id}")
    Long getOrganizationId();

    String getOrganizationName();

    String getBody();

    String getSubject();

}
