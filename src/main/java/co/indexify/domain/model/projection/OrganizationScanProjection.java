package co.indexify.domain.model.projection;

import co.indexify.domain.model.OrganizationScan;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.Set;

@Projection(name = "detail", types = {OrganizationScan.class})
@JsonInclude(Include.NON_EMPTY)

public interface OrganizationScanProjection extends DomainProjection<String> {
    Set getTags();

    @Value("#{target?.createdBy?.id}")
    Long getUserId();

    String getName();
}
