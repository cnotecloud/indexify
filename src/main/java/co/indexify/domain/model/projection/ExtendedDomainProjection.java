package co.indexify.domain.model.projection;

import java.io.Serializable;

/**
 * Created by ankur on 10-05-2017.
 */
public interface ExtendedDomainProjection<ID extends Serializable> extends DomainProjection<ID> {
    BasicUserProjection getCreatedBy();
    BasicUserProjection getUpdatedBy();
}
