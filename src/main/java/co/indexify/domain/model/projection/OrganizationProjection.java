package co.indexify.domain.model.projection;

import co.indexify.domain.model.Organization;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;
import java.util.Set;

@Projection(name = "detail", types = {Organization.class})
@JsonInclude(Include.NON_EMPTY)

public interface OrganizationProjection extends DomainProjection<Long> {
    String getDisplayName();

    LocationProjection getLocation();

    String getImgUrl();

    boolean getPublicProfile();

    String getWebsite();

    List getEmails();

    List getContacts();

    String getCatalogue();

    String getStall();

    List getCatalogues();

    Set getTags();
}
