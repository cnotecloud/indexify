package co.indexify.domain.model.projection;

import co.indexify.domain.model.User;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "detailWithOrganization", types = {User.class})

public interface UserOrganizationProjection extends UserProjection {
    OrganizationProjection getOrganization();
}
