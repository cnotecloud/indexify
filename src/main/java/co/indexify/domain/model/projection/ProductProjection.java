package co.indexify.domain.model.projection;

import co.indexify.domain.model.Product;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;

@JsonInclude(Include.NON_EMPTY)
@Projection(name = "detail", types = {Product.class})
public interface ProductProjection extends DomainProjection<String> {
    String getName();

    List getImgUrls();

    List getControls();

    @Value("#{target?.leaf?.id }")
    String getLeafId();
}
