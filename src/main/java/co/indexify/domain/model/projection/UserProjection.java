package co.indexify.domain.model.projection;

import co.indexify.domain.model.CatalogueStatus;
import co.indexify.domain.model.Contact;
import co.indexify.domain.model.Email;
import co.indexify.domain.model.User;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.springframework.data.rest.core.config.Projection;

import java.util.Set;

@JsonInclude(Include.NON_EMPTY)
@Projection(types = {User.class}, name = "detail")

public interface UserProjection extends BasicUserProjection {
    Contact getContact();

    Email getEmail();

    LocationProjection getLocation();

    boolean getEnabled();

    boolean getPublicProfile();

    String getCompany();

    CatalogueStatus getCatalogueStatus();

    Set getTags();
}
