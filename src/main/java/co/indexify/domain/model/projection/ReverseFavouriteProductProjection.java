package co.indexify.domain.model.projection;

import co.indexify.domain.model.ReverseFavouriteProduct;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "detail", types = {ReverseFavouriteProduct.class})
@JsonInclude(Include.NON_EMPTY)
public interface ReverseFavouriteProductProjection extends DomainProjection<String> {
    String getProductName();

    String getOrganizationName();

    @Value("#{target?.product?.id}")
    String getProductId();

    @Value("#{target?.organization?.id}")
    String getOrganizationId();

    @Value("#{target?.createdBy?.id}")
    Long getUserId();

    String getUserName();
}
