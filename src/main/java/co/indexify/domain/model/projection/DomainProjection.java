package co.indexify.domain.model.projection;


import java.io.Serializable;
import java.util.Date;

/**
 * Created by ankur on 10-05-2017.
 */
public interface DomainProjection<ID extends Serializable> {
    Date getCreated();
    Date getUpdated();
    ID getId();
}
