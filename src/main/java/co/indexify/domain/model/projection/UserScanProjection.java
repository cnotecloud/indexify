package co.indexify.domain.model.projection;

import co.indexify.domain.model.UserScan;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.Set;

@Projection(name = "detail", types = {UserScan.class})
@JsonInclude(Include.NON_EMPTY)

public interface UserScanProjection extends DomainProjection<String> {
    Set<String> getTags();

    @Value("#{target?.organization?.id}")
    Long getOrganizationId();

    String getName();

    @Value("#{target?.user?.id}")
    Long getUserId();
}
