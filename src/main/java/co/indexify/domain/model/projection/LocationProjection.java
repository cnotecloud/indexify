package co.indexify.domain.model.projection;

import co.indexify.domain.model.Location;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.springframework.data.rest.core.config.Projection;

@Projection(types = {Location.class})
@JsonInclude(Include.NON_EMPTY)

public interface LocationProjection {
    String getAddress();

    String getCountry();

    String getCity();

    String getRegion();

    String getPostalCode();
}
