package co.indexify.domain.model.projection;

import co.indexify.domain.model.Organization;

import java.io.Serializable;

public interface OrganizedExtendedDomainProjection<ID extends Serializable> extends ExtendedDomainProjection<ID>{
    OrganizationProjection getOrganization();
}
