package co.indexify.domain.model.projection;

import co.indexify.domain.model.User;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.springframework.data.rest.core.config.Projection;

import java.util.Set;

@Projection(types = {User.class})
@JsonInclude(Include.NON_EMPTY)

public interface BasicUserProjection extends DomainProjection<Long> {
    String getUsername();

       String getFirstName();

    String getLastName();

    Set getRoles();

    String getImgUrl();

    String getMiddleName();
}
