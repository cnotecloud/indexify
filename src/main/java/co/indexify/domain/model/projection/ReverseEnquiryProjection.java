package co.indexify.domain.model.projection;

import co.indexify.domain.model.Enquiry;
import co.indexify.domain.model.ReverseEnquiry;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "detail", types = {ReverseEnquiry.class})
@JsonInclude(Include.NON_EMPTY)

public interface ReverseEnquiryProjection extends DomainProjection<String> {


    String getBody();

    String getSubject();

    @Value("#{target?.createdBy?.id}")
    Long getUserId();

    String getCreatedByName();


}
