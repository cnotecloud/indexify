package co.indexify.domain.model.projection;

import co.indexify.domain.model.UserVisitor;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.Set;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "detail", types = {UserVisitor.class})
@JsonInclude(Include.NON_EMPTY)
public interface UserVisitorProjection extends DomainProjection<String> {
    Set getTags();

    @Value("#{target?.createdBy?.id}")
    Long getUserId();

    String getName();
}
