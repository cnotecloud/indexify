package co.indexify.domain.model.projection;

import co.indexify.domain.model.Node;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;

@Projection(name = "detail", types = {Node.class})
@JsonInclude(Include.NON_EMPTY)

public interface NodeProjection extends DomainProjection<String> {
    String getDisplayName();

    Boolean getLeaf();

    String getDescription();

    List<String> getImgUrls();

    @Value("#{target?.parent?.id}")
    String getParentId();
}
