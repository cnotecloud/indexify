package co.indexify.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = UserVisitor.COLLECTION)
@JsonInclude(Include.NON_EMPTY)

public final class UserVisitor extends ExtendedDomain<String> {
    private String name;
    public static final String COLLECTION = "#{'userVisitors' + T(co.indexify.util.SecurityUtil).getUser()}";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
