package co.indexify.domain.model;

import co.indexify.dto.OrganizationCreateDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.querydsl.core.annotations.QueryEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Document(collection = "organizations")
@QueryEntity
@JsonInclude(Include.NON_EMPTY)

public class Organization extends ExtendedDomain<Long> implements Serializable {
    private SmtpCredentials smtpCredentials;
    private String displayName;
    private Location location;
    private String imgUrl;
    private boolean publicProfile = true;
    private String website;
    private Set<Email> emails = new HashSet<>();
    private Set<Contact> contacts = new HashSet<>();
    private String description;
    private String stall;
    private String catalogue;
    private Set<String> catalogues = new HashSet<>();
    public static final String SEQ = "ORGANIZATION_SEQ";
    public static final String COLLECTION = "organizations";


    public final void addPrimaryContact(String cotact) {
        if (this.contacts != null) {
            this.contacts.add(new Contact(cotact));
        }

    }

    public void addPrimaryEmail(String email) {
        if (this.emails != null) {
            this.emails.add(new Email(email));
        }

    }

    public SmtpCredentials getSmtpCredentials() {
        return smtpCredentials;
    }

    public void setSmtpCredentials(SmtpCredentials smtpCredentials) {
        this.smtpCredentials = smtpCredentials;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public boolean getPublicProfile() {
        return publicProfile;
    }

    public void setPublicProfile(boolean publicProfile) {
        this.publicProfile = publicProfile;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Set<Email> getEmails() {
        return emails;
    }

    public void setEmails(Set<Email> emails) {
        this.emails = emails;
    }

    public Set<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(Set<Contact> contacts) {
        this.contacts = contacts;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStall() {
        return stall;
    }

    public void setStall(String stall) {
        this.stall = stall;
    }

    public String getCatalogue() {
        return catalogue;
    }

    public void setCatalogue(String catalogue) {
        this.catalogue = catalogue;
    }

    public Set<String> getCatalogues() {
        return catalogues;
    }

    public void setCatalogues(Set<String> catalogues) {
        this.catalogues = catalogues;
    }




    public static Organization fromDto(OrganizationCreateDto createDto) {
        Organization organization = new Organization();
        organization.setDisplayName(createDto.getDisplayName());
        organization.addPrimaryContact(createDto.getContact());
        organization.addPrimaryEmail(createDto.getEmail());
        organization.setLocation(Location.fromDto(createDto.getLocation()));
        organization.setWebsite(createDto.getWebsite());
        organization.setDescription(createDto.getDescription());
        if (createDto.getTag() != null) {
            organization.addTag(createDto.getTag());
        }

        return organization;
    }



    public void addCatalogue(String catalogue) {
        if( this.catalogues!= null) this.catalogues.add(catalogue);
    }



}
