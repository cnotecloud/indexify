package co.indexify.domain.model;

import co.indexify.aws.sns.StringUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.NotNull;

/**
 * Created by ankur on 02-06-2017.
 */
public class Domain<ID  extends Serializable> implements Serializable {
    @CreatedDate
    @JsonFormat(pattern = "yyyy-MM-dd'T'hh:mm:ss.SSSZ")
    private Date created;

    @LastModifiedDate
    @JsonFormat(pattern = "yyyy-MM-dd'T'hh:mm:ss.SSSZ")
    private Date updated;

    @Id
    private ID id;

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }


    private Set<String> tags = new HashSet<>();


    public final void addTag(String tag) {
        if(tags != null) {
            if(!StringUtils.isEmpty(tag))
                tags.add(tag);
        }
    }

    public final void removeTag(String tag) {
        if(tags != null) tags.remove(tag);
    }


    public Set<String> getTags() {
        return tags;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }
}
