package co.indexify.domain.model;


import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = ReverseEnquiry.COLLECTION)
public class ReverseEnquiry extends ExtendedDomain<String> {

    public static final String COLLECTION = "#{'reverseEnquiries' + T(co.indexify.util.SecurityUtil).getOrganization()}";


    private String body;

    private String subject;


    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }


    private String createdByName;


    public static String getCOLLECTION() {
        return COLLECTION;
    }

    public String getCreatedByName() {
        return createdByName;
    }

    public void setCreatedByName(String createdByName) {
        this.createdByName = createdByName;
    }
}
