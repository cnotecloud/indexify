package co.indexify.domain.model;


import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = Enquiry.COLLECTION)
public class Enquiry extends OrganizedExtendedDomain<String> {

    public static final String COLLECTION = "#{'enquiries' + T(co.indexify.util.SecurityUtil).getUser()}";



    private String organizationName;



    private String body;

    private String subject;


    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }


    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
