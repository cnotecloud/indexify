package co.indexify.domain.model;


import co.indexify.domain.DomainConstants;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import static co.indexify.domain.model.Platform.COLLECTION;

@Document(collection = COLLECTION)
public class Platform extends Domain<String> {

    private String application;
    @Indexed(name = DomainConstants.PLATFORM_TYPE_INDEX_NAME, unique = true)
    private String type;

    private String principal;
    private String credential;


    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getCredential() {
        return credential;
    }

    public void setCredential(String credential) {
        this.credential = credential;
    }

    public static final String COLLECTION = "platforms";


    public static  final String GCM = "GCM";
    public static final String APNS = "APNS";
    public static final  String APNS_SANDBOX = "APNS_SANDBOX";



}
