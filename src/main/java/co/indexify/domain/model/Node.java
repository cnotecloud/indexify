package co.indexify.domain.model;

import co.indexify.dto.NodeDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.querydsl.core.annotations.QueryEntity;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Document(collection = Node.COLLECTION)
@QueryEntity
@JsonInclude(Include.NON_EMPTY)
public class Node extends ExtendedDomain<String> implements Serializable {
    private String displayName;
    private List<String> imgUrls = new ArrayList<>();
    private String description;
    @DBRef(lazy = true)
    private Node parent;
    private Boolean leaf;
    public static final String COLLECTION = "#{'nodes' +  T(co.indexify.util.SecurityUtil).getOrganization()}";


    public final Node addImgUrl(String url) {
        imgUrls.add(url);
        return this;
    }

    public final Node assignParent(Node node) {
        this.parent = node;
        return this;
    }

    public Node() {
        this.leaf = false;
    }


    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public List<String> getImgUrls() {
        return imgUrls;
    }

    public void setImgUrls(List<String> imgUrls) {
        this.imgUrls = imgUrls;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public Boolean getLeaf() {
        return leaf;
    }

    public void setLeaf(Boolean leaf) {
        this.leaf = leaf;
    }

    public static final Node fromDto(NodeDto dto) {
        Node node = new Node();
        node.setId(dto.getId());
        node.setDisplayName(dto.getDisplayName());
        if (dto.getImgUrl() != null) {
            node.addImgUrl(dto.getImgUrl());
        }
        node.setLeaf(dto.getLeaf());
        return node;
    }






}
