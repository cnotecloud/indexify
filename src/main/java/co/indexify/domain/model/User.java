package co.indexify.domain.model;

import co.indexify.domain.DomainConstants;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.collect.Sets;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by ankur on 05-02-2017.
 */
@CompoundIndexes({
        @CompoundIndex(name = DomainConstants.USER_CONTACT_INDEX_NAME, def = DomainConstants.USER_CONTACT_INDEX_DEF, sparse = true, unique = true),
        @CompoundIndex(name = DomainConstants.USER_EMAIL_INDEX_NAME, def = DomainConstants.USER_EMAIL_INDEX_DEF, sparse = true, unique = true),
        @CompoundIndex(name = DomainConstants.USER_VERIFICATION_TOKEN_INDEX_NAME, def = DomainConstants.USER_VERIFICATION_TOKEN_INDEX_DEF, sparse = true, unique = true),
        @CompoundIndex(name = DomainConstants.USER_PASSWORD_RESET_TOKEN_INDEX_NAME, def = DomainConstants.USER_PASSWORD_RESET_TOKEN_INDEX_DEF, sparse = true, unique = true)

})
@Document(collection = DomainConstants.USER_COLLECTION)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class User extends ExtendedDomain<Long> implements Serializable {

    public static final User ANONYMOUS_USER = new User();
    public static final String ANONYMOUS_USERNAME= "anonymous";
    public static final Long ANONYMOUS_ID = 1L;
    static
    {
        ANONYMOUS_USER.setEmail(new Email(Email.ANONYMOUS_EMAIL, Email.EMAIL_PRIMARY));
        ANONYMOUS_USER.setUsername(ANONYMOUS_USERNAME);
        ANONYMOUS_USER.setRoles(Sets.newHashSet(Role.ROLE_ANONYMOUS));
        ANONYMOUS_USER.setId(ANONYMOUS_ID);
    }


    @JsonIgnore
    private Otp otp;

    public Otp getOtp() {
        return otp;
    }

    public void setOtp(Otp otp) {
        this.otp = otp;
    }

    private Boolean enabled;
    private Email email;


    private String company;


    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    private CatalogueStatus catalogueStatus;


    public CatalogueStatus getCatalogueStatus() {
        return catalogueStatus;
    }

    public void setCatalogueStatus(CatalogueStatus catalogueStatus) {
        this.catalogueStatus = catalogueStatus;
    }

    @DBRef
    private Organization  organization;
    public Organization getOrganization() {
        return organization;
    }
    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    /*
        @Size( min = 8, max = 30)
        @UsernamePattern
        @StartWithAlphaNumeric
        @NotContainConsecutivePeriod
        @NotContainConsecutiveUnderscore
        @NotContainPeriodFollowedByUnderscore
        @NotContainUnderscoreFollowedByPeriod
        @EndWithAlphaNumeric
        */
    @Indexed(name = DomainConstants.USER_USERNAME_INDEX_NAME, unique = true, sparse = true)
    private String username;

    @JsonIgnore
    private String password;

    private Boolean using2FA;

    private String firstName;

    private String lastName;

    private String middleName;

    private Set<String> roles = new HashSet<>();

    private Contact contact;

    private String imgUrl;

    private Location location;

    public void addRole(String role){
        roles.add(role);
    }
    public void removeRole(String role){
        roles.remove(role);
    }

    @JsonIgnore
    private VerificationToken verificationToken;



    @JsonIgnore
    private PasswordResetToken passwordResetToken;

    private Boolean publicProfile = true;

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getUsing2FA() {
        return using2FA;
    }

    public void setUsing2FA(Boolean using2FA) {
        this.using2FA = using2FA;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public VerificationToken getVerificationToken() {
        return verificationToken;
    }

    public void setVerificationToken(VerificationToken verificationToken) {
        this.verificationToken = verificationToken;
    }

    public PasswordResetToken getPasswordResetToken() {
        return passwordResetToken;
    }

    public void setPasswordResetToken(PasswordResetToken passwordResetToken) {
        this.passwordResetToken = passwordResetToken;
    }

    public Boolean getPublicProfile() {
        return publicProfile;
    }

    public void setPublicProfile(Boolean publicProfile) {
        this.publicProfile = publicProfile;
    }



    private Set<Device> devices = new HashSet<>();

    public  Set<Device> getDevices() {
        return devices;
    }

    public void setDevices(Set<Device> devices) {
        this.devices = devices;
    }

    public void addDevice(Device device){
        if(devices != null) devices.add(device);
    }


    public void removeDevice(Device device){
        if(devices != null) devices.remove(device);
    }





    @JsonIgnore
    public String getName(){
        String name = firstName;
        if (middleName != null)
            name += " " + middleName + " ";
        else
            name += " ";
        name += lastName;
        return name;

    }
}
