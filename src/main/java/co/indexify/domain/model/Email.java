package co.indexify.domain.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import static co.indexify.domain.model.Contact.CONTACT_PRIMARY;
public class Email implements Serializable{
    private String value;
    private String tag;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Email() {
        this(EMAIL_PRIMARY);
    }


    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Email(String value, String tag){
        this.value = value;
        this.tag = tag;
    }


    @JsonCreator
    public Email(String value){
        this(value, EMAIL_PRIMARY);
    }



    public static final String EMAIL_PRIMARY = "primary";
    public static final String ANONYMOUS_EMAIL ="anonymous@indexify.co";

}
