package co.indexify.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.querydsl.core.annotations.QueryEntity;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = FavouriteProduct.COLLECTION)
@QueryEntity
@JsonInclude(Include.NON_EMPTY)

public class FavouriteProduct extends OrganizedExtendedDomain<String> implements Serializable {
    private String productName;
    private String organizationName;
    @DBRef(lazy = true)
    private Product product;
    public static final String COLLECTION = "#{'favouriteProducts' + T(co.indexify.util.SecurityUtil).getUser()}";


    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public static String getCOLLECTION() {
        return COLLECTION;
    }
}
