package co.indexify.domain.model;

import co.indexify.dto.ProductCreateDto;
import co.indexify.dto.ProductUpdateDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.querydsl.core.annotations.QueryEntity;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Document(collection = Product.COLLECTION)
@QueryEntity
@JsonInclude(Include.NON_EMPTY)

public class Product extends ExtendedDomain<String> implements Serializable {
    private String name;
    private List<String> imgUrls = new ArrayList<>();
    private List<ComplexControl> complexControls;
    private List<Control> controls;
    @DBRef(lazy = true)
    private Node leaf;
    public static final String COLLECTION = "#{'products' +  T(co.indexify.util.SecurityUtil).getOrganization()}";


    public final Product addImageUrls(String url) {
        if (this.imgUrls != null) {
            this.imgUrls.add(url);
        }
        return this;
    }
    
    public final Product updateImageUrls(String url) {
            this.imgUrls.add(url);
        return this;
    }

    public final void updateFromUpdateDto(ProductUpdateDto dto) {
        this.controls = dto.getControls();
        this.name = dto.getDisplayName();
//        this.imgUrls = dto.getImage();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getImgUrls() {
        return imgUrls;
    }

    public void setImgUrls(List<String> imgUrls) {
        this.imgUrls = imgUrls;
    }

    public List<ComplexControl> getComplexControls() {
        return complexControls;
    }

    public void setComplexControls(List<ComplexControl> complexControls) {
        this.complexControls = complexControls;
    }

    public List<Control> getControls() {
        return controls;
    }

    public void setControls(List<Control> controls) {
        this.controls = controls;
    }

    public Node getLeaf() {
        return leaf;
    }

    public void setLeaf(Node leaf) {
        this.leaf = leaf;
    }

    public static Product fromCreateDto(ProductCreateDto dto) {
        Product product = new Product();
        product.setControls(dto.getControls());
        product.setName(dto.getDisplayName());
        return product;
    }



    public static Product fromUpdateDto(ProductUpdateDto dto) {
        Product product = new Product();
        product.setControls(dto.getControls());
//        product.setImgUrls(dto.getImages());
        return product;
    }


}
