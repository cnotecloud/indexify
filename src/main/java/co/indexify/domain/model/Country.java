package co.indexify.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.querydsl.core.annotations.QueryEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "countries")
@QueryEntity
@JsonInclude(Include.NON_EMPTY)

public class Country extends Domain<String> implements Serializable {
    private String name;
    @Indexed(unique = true, sparse = true, name = "countriesIso3Idx")
    private String iso3;
    private String code;
    @Indexed(unique = true, sparse = true, name = "countriesIso2Idx")
    private String iso2;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIso3() {
        return iso3;
    }

    public void setIso3(String iso3) {
        this.iso3 = iso3;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIso2() {
        return iso2;
    }

    public void setIso2(String iso2) {
        this.iso2 = iso2;
    }
}
