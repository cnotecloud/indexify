package co.indexify.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.io.Serializable;

/**
 * Created by ankurpathak on 17-03-2016.
 */
public class ExtendedDomain<ID extends Serializable> extends Domain<ID> implements Serializable {

    @CreatedBy
    @DBRef(lazy = true)
    @JsonIgnore
    User createdBy;


    @LastModifiedBy
    @DBRef(lazy = true)
    @JsonIgnore
    User updatedBy;


    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }
}
