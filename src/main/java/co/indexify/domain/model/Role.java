package co.indexify.domain.model;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.querydsl.core.annotations.QueryEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Document(collection = "roles")
@QueryEntity
public class Role extends Domain<String> implements Serializable {


    private Set<String> privileges = new HashSet<>();

    @Indexed(name = "rolesNameIdx", unique = true, sparse = true)
    private String name;


    public Role(String name) {
        this.name = name;
    }

    public Role privileges(){
        return this;
    }

    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_USER_PASSWORD_RESET = "ROLE_USER_PASSWORD_RESET";
    public static final String ROLE_USER = "ROLE_USER";
    public static final String ROLE_ECATALOGUE = "ROLE_ECATALOGUE";
    public static final String ROLE_ANONYMOUS = "ROLE_ANONYMOUS";
    public static final String ROLE_USER_FOUND = "ROLE_USER_FOUND";


    public static final Role ANONYMOUS_ROLE = new Role(ROLE_ANONYMOUS);

    static {
        ANONYMOUS_ROLE.privileges = Sets.newHashSet(
                Privilege.PRIV_ANONYMOUS,
                Privilege.PRIV_PRODUCTS_GET,
                Privilege.PRIV_NODE_TREE_GET,
                Privilege.PRIV_ORGANIZATION_GET,
                Privilege.PRIV_LEAF_CHILD_PRODUCTS_GET,
                Privilege.PRIV_SEND_EMAIL,
                Privilege.PRIV_EVENTS_ORDER_BY_NAME_GET,
                Privilege.PRIV_EVENTS_ORDER_BY_START_GET
        );
    }

    public Set<String> getPrivileges() {
        return privileges;
    }

    public List<String> toList() {
        return Lists.newArrayList(this.privileges);
    }

    public String[] toArray() {
        String[] stockArr = new String[privileges.size()];
        return this.privileges.toArray(stockArr);
    }




    public void setPrivileges(Set<String> privileges) {
        this.privileges = privileges;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
