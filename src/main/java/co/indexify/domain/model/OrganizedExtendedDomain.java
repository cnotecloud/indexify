package co.indexify.domain.model;

import org.springframework.data.mongodb.core.mapping.DBRef;

import java.io.Serializable;
import javax.validation.constraints.NotNull;

public class OrganizedExtendedDomain<ID extends Serializable> extends ExtendedDomain<ID> implements Serializable{
    @DBRef(lazy = true)
    private Organization  organization;


    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }
}