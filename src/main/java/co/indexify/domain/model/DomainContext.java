package co.indexify.domain.model;

import co.indexify.util.WebUtil;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class DomainContext extends WebAuthenticationDetails {
    private final String requestedUser;
    private final String requestedOrganization;
    private final String tag;
    private final Boolean otp;
    private final String remoteAddress;
    private final Locale locale;

    public Locale getLocale() {
        return locale;
    }

    public DomainContext(final HttpServletRequest request) {
        super(request);
        this.requestedUser = WebUtil.getRequestedUser(request);
        this.requestedOrganization = WebUtil.getRequestedOrganization(request);
        this.tag = WebUtil.getTag(request);
        this.otp = WebUtil.getOtp(request);
        this.remoteAddress = WebUtil.getClientIP(request);
        this.locale = WebUtil.getLocale(request);
    }

    public String getRequestedUser() {
        return requestedUser;
    }

    public String getRequestedOrganization() {
        return requestedOrganization;
    }

    public String getTag() {
        return tag;
    }

    public Boolean getOtp() {
        return otp;
    }

    @Override
    public String getRemoteAddress() {
        return this.remoteAddress;
    }
}
