package co.indexify.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.querydsl.core.annotations.QueryEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = Notification.COLLECTION)
@QueryEntity
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Notification extends ExtendedDomain<String> implements Serializable  {
    private String title;
    private String body;



    public static final String COLLECTION = "#{'notifications' + T(co.indexify.util.SecurityUtil).getUser()}";


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
