package co.indexify.domain.model;


import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class EventFlags {
    private Boolean visitor = false;
    private Boolean exibitor = true;
    private Boolean conference = false;
    private Boolean floorPlan = true;


    public Boolean getVisitor() {
        return visitor;
    }

    public void setVisitor(Boolean visitor) {
        this.visitor = visitor;
    }

    public Boolean getExibitor() {
        return exibitor;
    }

    public void setExibitor(Boolean exibitor) {
        this.exibitor = exibitor;
    }

    public Boolean getConference() {
        return conference;
    }

    public void setConference(Boolean conference) {
        this.conference = conference;
    }

    public Boolean getFloorPlan() {
        return floorPlan;
    }

    public void setFloorPlan(Boolean floorPlan) {
        this.floorPlan = floorPlan;
    }
}
