package co.indexify.domain.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Document(collection = Event.COLLECTION)
public class Event extends  Domain<String>{
    private Set<String> imgUrls;
    private String key;
    private String name;
    private Location location;

    private String website;


    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @JsonFormat(pattern = "yyyy-MM-dd'T'hh:mm:ss.SSSZ")
    private Date start;

    @JsonFormat(pattern = "yyyy-MM-dd'T'hh:mm:ss.SSSZ")
    private Date end;

    private EventFlags flags;
    private List<EmailData> emails;
    private List<FloorPlan> floorPlans;

    private List<Conference> conferences;



    private List<Control> controls;

    public List<Control> getControls() {
        return controls;
    }

    public void setControls(List<Control> controls) {
        this.controls = controls;
    }

    public List<Conference> getConferences() {
        return conferences;
    }

    public void setConferences(List<Conference> conferences) {
        this.conferences = conferences;
    }

    public List<FloorPlan> getFloorPlans() {
        return floorPlans;
    }

    public void setFloorPlans(List<FloorPlan> floorPlans) {
        this.floorPlans = floorPlans;
    }

    public static final String COLLECTION = "events";


    public Set<String> getImgUrls() {
        return imgUrls;
    }

    public void setImgUrls(Set<String> imgUrls) {
        this.imgUrls = imgUrls;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public EventFlags getFlags() {
        return flags;
    }

    public void setFlags(EventFlags flags) {
        this.flags = flags;
    }

    public List<EmailData> getEmails() {
        return emails;
    }

    public void setEmails(List<EmailData> emails) {
        this.emails = emails;
    }
}
