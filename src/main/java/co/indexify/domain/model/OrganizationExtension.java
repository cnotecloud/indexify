package co.indexify.domain.model;

import co.indexify.domain.DomainConstants;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

import static co.indexify.domain.model.OrganizationExtension.COLLECTION;

@Document(collection = COLLECTION)
public class OrganizationExtension extends Domain<String>{


    public static final String COLLECTION = "#{'organizationExtensions' +  T(co.indexify.util.SecurityUtil).getOrganization()}";



    @Indexed(name = DomainConstants.ORGANIZATION_EXTENSION_KEY, unique = true, sparse = true)
    private String key;


    private List<Control> controls;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public List<Control> getControls() {
        return controls;
    }

    public void setControls(List<Control> controls) {
        this.controls = controls;
    }
}
