package co.indexify.domain.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Contact implements Serializable {
    private String value;
    private String tag;
    public Contact(String value, String tag){
        this.value = value;
        this.tag = tag;

    }


    public Contact() {
        this(CONTACT_PRIMARY);
    }

    public Contact(String value){
        this(value, CONTACT_PRIMARY);
    }


    public static final String CONTACT_PRIMARY = "primary";


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
