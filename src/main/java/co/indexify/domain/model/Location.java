package co.indexify.domain.model;

import co.indexify.dto.LocationDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.io.Serializable;


@JsonInclude(Include.NON_EMPTY)
public class Location implements Serializable {
    private String address;
    private String country;
    private String city;
    private String region;
    private String postalCode;
    private String tag;
    public static final String LOCATION_PRIMARY = "primary";


    public Location(String tag) {
        this.tag = tag;
    }

    public Location() {
        this.tag = LOCATION_PRIMARY;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }



    public static final Location fromDto(LocationDto locationDto){
        Location location = new Location();
        location.setAddress(locationDto.getAddress());
        location.setCity(locationDto.getCity());
        location.setCountry(locationDto.getCountry());
        location.setPostalCode(locationDto.getPostalCode());
        location.setRegion(locationDto.getRegion());
        return location;
    }
}
