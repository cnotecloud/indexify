package co.indexify.domain.model;

import co.indexify.dto.DeviceDto;
import com.ankurpathak.server.validation.constraints.Any;

public class Device {
    private String id;

    private String platform;


    public Device() {
    }


    public Device(DeviceDto dto){
        id = dto.getDevice();
        platform = dto.getPlatform();
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Device device = (Device) o;

        if (id != null ? !id.equals(device.id) : device.id != null) return false;
        return platform != null ? platform.equals(device.platform) : device.platform == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (platform != null ? platform.hashCode() : 0);
        return result;
    }
}
