package co.indexify.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

@JsonInclude(Include.NON_EMPTY)
public class Otp implements Serializable {
    private String value;
    private Date expiryDate;
    private static final int EXPIRATION = 5;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    private final Date calculateExpiryDate(int expiryTimeInMinutes) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, expiryTimeInMinutes);
        return cal.getTime();
    }

    public final void updateToken(String token) {
        this.value = token;
        this.expiryDate = this.calculateExpiryDate(EXPIRATION);
    }

    public Otp() {
    }

    public Otp(String value) {
        this.value = value;
        this.expiryDate = this.calculateExpiryDate(EXPIRATION);
    }

    public Otp(String value, User user) {
       this(value);
        user.setOtp(this);
    }


    public enum OtpStatus {
        VALID,
        INVALID,
        EXPIRED;
    }


}
