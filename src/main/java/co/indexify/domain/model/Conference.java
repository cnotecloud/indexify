package co.indexify.domain.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class Conference {
    private String title;
    private String venue;
    private String description;
    private String speaker;
    private String participants;


    @JsonFormat(pattern = "yyyy-MM-dd'T'hh:mm:ss.SSSZ")
    private Date start;

    @JsonFormat(pattern = "yyyy-MM-dd'T'hh:mm:ss.SSSZ")
    private Date end;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSpeaker() {
        return speaker;
    }

    public void setSpeaker(String speaker) {
        this.speaker = speaker;
    }

    public String getParticipants() {
        return participants;
    }

    public void setParticipants(String participants) {
        this.participants = participants;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }
}
