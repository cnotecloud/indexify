package co.indexify.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.querydsl.core.annotations.QueryEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = Resource.COLLECTION)
@QueryEntity
@JsonInclude(Include.NON_EMPTY)

public class Resource extends ExtendedDomain<String> implements Serializable {
    private String url;
    private Resource.ResourceType type;
    public static final String COLLECTION = "#{'resources' +  T(co.indexify.util.SecurityUtil).getOrganization()}";


    public Resource(String url, Resource.ResourceType type, String tag) {
        this.url = url;
        this.type = type;
        this.addTag(tag);
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ResourceType getType() {
        return type;
    }

    public void setType(ResourceType type) {
        this.type = type;
    }


    public Resource() {
    }

    public  enum ResourceType {


        FILE("files"), IMAGE("images");


        ResourceType(String subDirectory) {
            this.subDirectory = subDirectory;
        }

        private String subDirectory;

        public final String getSubDirectory() {
            return this.subDirectory;
        }


    }

}
