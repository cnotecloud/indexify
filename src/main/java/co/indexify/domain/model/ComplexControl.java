package co.indexify.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@JsonInclude(Include.NON_EMPTY)
public class ComplexControl implements Serializable {
    private Map<String, List<Object>> dimensions;
    private String displayName;
    private Map<String, List<Integer>> values;


    public Map<String, List<Object>> getDimensions() {
        return dimensions;
    }

    public void setDimensions(Map<String, List<Object>> dimensions) {
        this.dimensions = dimensions;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Map<String, List<Integer>> getValues() {
        return values;
    }

    public void setValues(Map<String, List<Integer>> values) {
        this.values = values;
    }
}
