package co.indexify.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.querydsl.core.annotations.QueryEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "privileges")
@QueryEntity
@JsonInclude(Include.NON_EMPTY)
public class Privilege extends Domain<String> implements Serializable {


    public static final String PRIV_PRODUCT_CREATE_CSV = "PRIV_PRODUCT_CREATE_CSV";
    @Indexed(name = "privilegesNameIdx", unique = true, sparse = true)
    private String name;
    public static final String PRIV_ANONYMOUS = "PRIV_ANONYMOUS";
    public static final String PRIV_USER_PASSWORD_RESET = "PRIV_USER_PASSWORD_RESET";
    public static final String PRIV_USER_ME_GET = "PRIV_USER_ME_GET";
    public static final String PRIV_ORG_CREATE = "PRIV_ORG_CREATE";
    public static final String PRIV_NODE_TREE_CREATE = "PRIV_NODE_TREE_CREATE";
    public static final String PRIV_PRODUCT_CREATE = "PRIV_PRODUCT_CREATE";
    public static final String PRIV_PRODUCT_UPDATE = "PRIV_PRODUCT_CREATE";
    public static final String PRIV_PRODUCT_NODE_LINK = "PRIV_PRODUCT_NODE_LINK";
    public static final String PRIV_RESOURCE_CREATE = "PRIV_RESOURCE_CREATE";
    public static final String PRIV_NODE_TREE_GET = "PRIV_NODE_TREE_GET";
    public static final String PRIV_PRODUCTS_GET = "PRIV_PRODUCTS_GET";
    public static final String PRIV_LEAF_CHILD_PRODUCTS_GET = "PRIV_LEAF_CHILD_PRODUCTS_GET";
    public static final String PRIV_PRODUCTS_SEARCH_BY_NAME_GET = "PRIV_PRODUCTS_SEARCH_BY_NAME_GET";
    public static final String PRIV_ROOT_NODES_GET = "PRIV_ROOT_NODES_GET";
    public static final String PRIV_NODE_CHILD_NODES_GET = "PRIV_NODE_CHILD_NODES_GET";
    public static final String PRIV_LEAF_NODES_GET = "PRIV_NODE_CHILD_NODES_GET";
    public static final String PRIV_TEST_GET = "PRIV_NODE_CHILD_NODES_GET";
    public static final String PRIV_PRODUCTS_NOT_LINKED_GET = "PRIV_PRODUCTS_NOT_LINKED_GET";
    public static final String PRIV_USER_SCAN_CREATE = "PRIV_USER_SCAN_CREATE";
    public static final String PRIV_USER_SCANS_GET = "PRIV_USER_SCANS_GET";
    public static final String PRIV_ORGANIZATION_GET = "PRIV_ORGANIZATION_GET";
    public static final String PRIV_ORGANIZATION_SCANS_GET = "PRIV_ORGANIZATION_SCANS_GET";
    public static final String PRIV_USER_GET = "PRIV_USER_GET";
    public static final String PRIV_USER_SCANS_SEARCH_BY_NAME_GET = "PRIV_USER_SCANS_SEARCH_BY_NAME_GET";
    public static final String PRIV_ORGANIZATION_SCANS_SEARCH_BY_NAME_GET = "PRIV_ORGANIZATION_SCANS_SEARCH_BY_NAME_GET";
    public static final String PRIV_FAVOURITE_PRODUCT_CREATE = "PRIV_FAVOURITE_PRODUCT_CREATE";
    public static final String PRIV_FAVOURITE_PRODUCTS_GET = "PRIV_FAVOURITE_PRODUCTS_GET";
    public static final String PRIV_FAVOURITE_PRODUCTS_BY_ORGANIZATION_GET = "PRIV_FAVOURITE_PRODUCTS_BY_ORGANIZATION_GET";
    public static final String PRIV_PRODUCT_GET = "PRIV_PRODUCT_GET";
    public static final String PRIV_FAVOURITE_PRODUCT_ORGANIZATIONS_ID_NAME_GET = "PRIV_FAVOURITE_PRODUCT_ORGANIZATIONS_ID_NAME_GET";
    public static final String PRIV_FAVOURITE_PRODUCT_DELETE = "PRIV_FAVOURITE_PRODUCT_DELETE";
    public static final String PRIV_USER_SCAN_DELETE = "PRIV_USER_SCAN_DELETE";
    public static final String PRIV_ORGANIZATION_SCAN_DELETE = "PRIV_ORGANIZATION_SCAN_DELETE";
    public static final String PRIV_USERS_BY_TAG_GET = "PRIV_USERS_BY_TAG_GET";
    public static final String PRIV_ORGANIZATIONS_BY_TAG_GET = "PRIV_ORGANIZATIONS_BY_TAG_GET";
    public static final String PRIV_USER_VISITORS_GET = "PRIV_USER_VISITORS_GET";
    public static final String PRIV_USER_VISITORS_SEARCH_BY_NAME_GET = "PRIV_USER_VISITORS_SEARCH_BY_NAME_GET";
    public static final String PRIV_USER_SCANS_BY_TAG_ORDER_BY_CREATED_GET = "PRIV_USER_SCANS_BY_TAG_ORDER_BY_CREATED_GET";
    public static final String PRIV_USER_SCANS_BY_TAG_SEARCH_BY_NAME_GET = "PRIV_USER_SCANS_BY_TAG_SEARCH_BY_NAME_GET";
    public static final String PRIV_USER_VISITORS_BY_TAG_ORDER_BY_CREATED_GET = "PRIV_USER_VISITORS_BY_TAG_ORDER_BY_CREATED_GET";
    public static final String PRIV_USER_VISITORS_BY_TAG_SEARCH_BY_NAME_GET = "PRIV_USER_VISITORS_BY_TAG_SEARCH_BY_NAME_GET";
    public static final String PRIV_ORGANIZATION_SCANS_BY_TAG_ORDER_BY_CREATED_GET = "PRIV_ORGANIZATION_SCANS_BY_TAG_ORDER_BY_CREATED_GET";
    public static final String PRIV_ORGANIZATION_SCANS_BY_TAG_SEARCH_BY_NAME_GET = "PRIV_ORGANIZATION_SCANS_BY_TAG_SEARCH_BY_NAME_GET";
    public static final String PRIV_ORGANIZATIONS_BY_TAG_SEARCH_BY_DISPLAY_NAME_GET = "PRIV_ORGANIZATIONS_BY_TAG_SEARCH_BY_DISPLAY_NAME_GET";
    public static final String PRIV_USERS_BY_TAG_SEARCH_BY_NAME_GET = "PRIV_USERS_BY_TAG_SEARCH_BY_NAME_GET";
    public static final String PRIV_USER_VISITOR_DELETE = "PRIV_USER_VISITOR_DELETE";
    public static final String PRIV_USER_SCANS_USERS_BY_TAG_ORDER_BY_CREATED_GET = "PRIV_USER_SCANS_USERS_BY_TAG_ORDER_BY_CREATED_GET";
    public static final String PRIV_USER_SCANS_USERS_BY_TAG_SEARCH_BY_NAME_GET = "PRIV_USER_SCANS_USERS_BY_TAG_SEARCH_BY_NAME_GET";
    public static final String PRIV_USER_SCANS_ORGANIZATIONS_BY_TAG_ORDER_BY_CREATED_GET = "PRIV_USER_SCANS_ORGANIZATIONS_BY_TAG_ORDER_BY_CREATED_GET";
    public static final String PRIV_USER_SCANS_ORGANIZATIONS_BY_TAG_SEARCH_BY_NAME_GET = "PRIV_USER_SCANS_ORGANIZATIONS_BY_TAG_SEARCH_BY_NAME_GET";
    public static final String PRIV_USER_FOUND = "PRIV_USER_FOUND";
    public static final String PRIV_RESOURCES_GET = "PRIV_RESOURCES_GET";
    public static final String PRIV_ORGANIZATION_CATALOGUE_UPDATE = "PRIV_ORGANIZATION_CATALOGUE_UPDATE";
    public static final String PRIV_USER_CATALOGUE_STATUS_UPDATE = "PRIV_USER_CATALOGUE_STATUS_UPDATE";
    public static final String PRIV_ORGANIZATION_CATALOGUE_ADD = "PRIV_ORGANIZATION_CATALOGUE_ADD";
    public static final String PRIV_SEND_EMAIL = "PRIV_SEND_EMAIL";
    public static final String PRIV_REVERSE_FAVOURITE_PRODUCT_ORGANIZATIONS_ID_NAME_GET = "PRIV_REVERSE_FAVOURITE_PRODUCT_ORGANIZATIONS_ID_NAME_GET";
    public static final String PRIV_REVERSE_FAVOURITE_PRODUCT_DELETE = "PRIV_REVERSE_FAVOURITE_PRODUCT_DELETE";
    public static final String PRIV_REVERSE_FAVOURITE_PRODUCTS_GET = "PRIV_REVERSE_FAVOURITE_PRODUCTS_GET";
    public static final String PRIV_REVERSE_FAVOURITE_PRODUCTS_BY_ORGANIZATION_GET = "PRIV_REVERSE_FAVOURITE_PRODUCTS_BY_ORGANIZATION_GET";

    public static final String PRIV_USER_SCANS_USERS_BY_SEARCH_BY_NAME_GET = "PRIV_USER_SCANS_USERS_BY_SEARCH_BY_NAME_GET";

    public static final String PRIV_USER_SCANS_ORGANIZATIONS_ORDER_BY_CREATED_GET = "PRIV_USER_SCANS_ORGANIZATIONS_ORDER_BY_CREATED_GET";

    public static final String PRIV_USER_SCANS_USERS_ORDER_BY_CREATED_GET = "PRIV_USER_SCANS_USERS_ORDER_BY_CREATED_GET";


    public static final String PRIV_USER_SCANS_ORGANIZATIONS_SEARCH_BY_NAME_GET = "PRIV_USER_SCANS_ORGANIZATIONS_SEARCH_BY_NAME_GET";


    public static final String PRIV_EVENTS_ORDER_BY_NAME_GET = "PRIV_EVENTS_ORDER_BY_NAME_GET";
    public static final String PRIV_EVENTS_ORDER_BY_START_GET = "PRIV_EVENTS_ORDER_BY_START_GET";

    public static final String PRIV_USER_TAG_ADD = "PRIV_USER_TAG_ADD";

    public static final String PRIV_LEAF_DELETE = "PRIV_LEAF_DELETE";

    public static final String PRIV_CREATE_NODE = "PRIV_CREATE_NODE";

    public static final String PRIV_UPDATE_NODE = "PRIV_UPDATE_NODE";

    public static final String PRIV_NODE_DELETE = "PRIV_NODE_DELETE";

    public static final String PRIV_USER_DEVICE_ADD = "PRIV_USER_DEVICE_ADD";

    public static final String PRIV_USER_DEVICE_REMOVE = "PRIV_USER_DEVICE_REMOVE";

    public static final String PRIV_RESOURCES_BY_TYPE_AND_TAG_GET = "PRIV_RESOURCES_BY_TYPE_AND_TAG_GET";
    public static final String PRIV_ORGANIZATIONS_BY_TAG_SEARCH_BY_DESCRIPTION_GET = "PRIV_ORGANIZATIONS_BY_TAG_SEARCH_BY_DESCRIPTION_GET";
    public static final String PRIV_NOTIFICATION_GET = "PRIV_NOTIFICATION_GET";

    public static final String PRIV_USER_UPADATE = "PRIV_USER_UPADATE";

    public static final String PRIV_ORGANIZATION_UPDATE ="PRIV_ORGANIZATION_UPDATE";
    public static final String PRIV_RESOURSE_TAG_ADD ="PRIV_RESOURSE_TAG_ADD";
    public static final String PRIV_RESOURSE_TAG_REMOVE ="PRIV_RESOURSE_TAG_REMOVE";
    public static final String PRIV_ORGANIZATION_EXTENSION_BY_TAG_GET ="PRIV_ORGANIZATION_EXTENSION_BY_TAG_GET";
    public static final String PRIV_ORGANIZATION_EXTENSION_BY_KEY_GET ="PRIV_ORGANIZATION_EXTENSION_BY_KEY_GET";


    public static final String PRIV_ENQUIRIES_GET = "PRIV_ENQUIRIES_GET";

    public static final String PRIV_REVERSE_ENQUIRIES_GET = "PRIV_REVERSE_ENQUIRIES_GET";

    public static final String  PRIV_ENQUIRY_CREATE = "PRIV_ENQUIRY_CREATE";








    public Privilege( String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
