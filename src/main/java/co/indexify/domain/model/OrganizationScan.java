package co.indexify.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.querydsl.core.annotations.QueryEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = OrganizationScan.COLLECTION)
@QueryEntity
@JsonInclude(Include.NON_EMPTY)

public class OrganizationScan extends ExtendedDomain<String> implements Serializable {
    private String name;
    public static final String COLLECTION = "#{'organizationScans' + T(co.indexify.util.SecurityUtil).getOrganization()}";


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
