package co.indexify.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

@JsonInclude(Include.NON_EMPTY)
public final class VerificationToken implements Serializable {
    private String value;
    private Date expiryDate;
    private static final int EXPIRATION = 1440;


    private final Date calculateExpiryDate(int expiryTimeInMinutes) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, expiryTimeInMinutes);
        return cal.getTime();

    }

    public final void updateToken(String token) {
        this.value = token;
        this.expiryDate = this.calculateExpiryDate(EXPIRATION);
    }

    public VerificationToken() {
    }

    public VerificationToken(String token) {
        this();
        this.value = token;
        this.expiryDate = this.calculateExpiryDate(EXPIRATION);
    }

    public VerificationToken(String value, User user) {
        this(value);
        user.setVerificationToken(this);
    }

    public static enum VerificationTokenStatus {
        VALID,
        INVALID,
        EXPIRED
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }
}
