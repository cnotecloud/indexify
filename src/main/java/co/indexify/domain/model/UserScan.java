package co.indexify.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.querydsl.core.annotations.QueryEntity;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = UserScan.COLLECTION)
@QueryEntity
@JsonInclude(Include.NON_EMPTY)

public final class UserScan extends OrganizedExtendedDomain<String> implements Serializable {
    private String name;
    @DBRef(lazy = true)
    private User user;
    public static final String COLLECTION = "#{'userScans' + T(co.indexify.util.SecurityUtil).getUser()}";


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
