package co.indexify.domain.model;

import co.indexify.dto.CatalogueStatusDto;

public class CatalogueStatus {
    private Boolean organizationCreated = false;
    private Boolean nodesCreated = false;
    private Boolean productsCreated = false;
    private Boolean nodesProductsMapped = false;

    public CatalogueStatus(CatalogueStatusDto dto) {
        this.organizationCreated = dto.getOrganizationCreated();
        this.nodesCreated = dto.getNodesCreated();
        this.productsCreated = dto.getProductsCreated();
        this.nodesProductsMapped = dto.getNodesProductsMapped();

    }

    public CatalogueStatus() {
    }

    public Boolean getOrganizationCreated() {
        return organizationCreated;
    }

    public void setOrganizationCreated(Boolean organizationCreated) {
        this.organizationCreated = organizationCreated;
    }

    public Boolean getNodesCreated() {
        return nodesCreated;
    }

    public void setNodesCreated(Boolean nodesCreated) {
        this.nodesCreated = nodesCreated;
    }

    public Boolean getProductsCreated() {
        return productsCreated;
    }

    public void setProductsCreated(Boolean productsCreated) {
        this.productsCreated = productsCreated;
    }

    public Boolean getNodesProductsMapped() {
        return nodesProductsMapped;
    }

    public void setNodesProductsMapped(Boolean nodesProductsMapped) {
        this.nodesProductsMapped = nodesProductsMapped;
    }
}
