package co.indexify.queryparam;

import co.indexify.dto.UserContext;
import co.indexify.service.IUserService;
import co.indexify.util.SecurityUtil;
import org.springframework.security.core.Authentication;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;


public class ExtraQueryParamFilter implements Filter, Serializable, FilterConfig {

    private FilterConfig config;


    private IUserService userService;

    public ExtraQueryParamFilter(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
        this.config = config;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        Optional<Authentication> token = SecurityUtil.getAuthentication();
        if(token.isPresent()){
            Enumeration<String> paramNames = request.getParameterNames();
            List<String> paramNamesList =  Collections.list(paramNames);
            Map<String, String[]> extraParam = new LinkedHashMap<>();
            if(token.get().getPrincipal() instanceof UserContext) {
                if (!paramNamesList.contains("organizationId")) {
                    extraParam.put("organizationId", new String[]{SecurityUtil.getOrganization()});
                }
                if(!paramNamesList.contains("id")){
                    extraParam.put("id", new String[]{String.valueOf(SecurityUtil.getUser())});
                }
            }
            ExtraQueryParamWrappedRequest modifiedRequest = new ExtraQueryParamWrappedRequest((HttpServletRequest)request, extraParam);
            chain.doFilter(modifiedRequest, response);
        }else {
            chain.doFilter(request, response);
        }

    }

    @Override
    public void destroy() {

    }


    @Override
    public String getFilterName() {
        return config.getFilterName();
    }

    @Override
    public ServletContext getServletContext() {
        return config.getServletContext();
    }

    @Override
    public String getInitParameter(String param) {
        return config.getInitParameter(param);
    }

    @Override
    public Enumeration<String> getInitParameterNames() {
        return config.getInitParameterNames();
    }
}

