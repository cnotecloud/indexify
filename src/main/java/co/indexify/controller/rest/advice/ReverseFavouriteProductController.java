package co.indexify.controller.rest.advice;

import co.indexify.annotation.CurrentUser;
import co.indexify.domain.model.User;
import co.indexify.dto.*;
import co.indexify.event.PaginatedResultsRetrievedEvent;
import co.indexify.exception.NotFoundException;
import co.indexify.service.IReverseFavouriteProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
public class ReverseFavouriteProductController {

    @Autowired
    private IReverseFavouriteProductService reverseFavouriteProductService;

    @Autowired
    private MessageSource messageSource;


    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;


    public static final String PATH_REVERSE_FAVOURITE_PRODUCT_ORGANIZATIONS_ID_NAME_GET = "/api/reverse-favourite-products/organizations";

    public static final String RESOURCE_ID = "id";
    public static final String QUERY_PARAM_SIZE = "size";
    public static final String QUERY_PARAM_PAGE = "page";
    public static final String QUERY_PARAM_SIZE_DEFAULT = "20";
    public static final String QUERY_PARAM_PAGE_DEFAULT = "0";

    public static final String PATH_REVERSE_FAVOURITE_PRODUCT_DELETE = "/api/reverse-favourite-products/{id}";
    public static final String PATH_REVERSE_FAVOURITE_PRODUCT_DELETE_SECURITY = "/api/reverse-favourite-products/*";








    @GetMapping(PATH_REVERSE_FAVOURITE_PRODUCT_ORGANIZATIONS_ID_NAME_GET)
    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    List<OrganizationIdNameDto> organizationsIdName(HttpServletResponse response, HttpServletRequest request, @CurrentUser User user, @RequestParam(name = QUERY_PARAM_PAGE, required = false, defaultValue = QUERY_PARAM_PAGE_DEFAULT) int page, @RequestParam(name = QUERY_PARAM_SIZE, required = false, defaultValue = QUERY_PARAM_SIZE_DEFAULT) int size) {
        Page<OrganizationIdNameDto> resultPage = reverseFavouriteProductService.getOrganizationIdName(new PageRequest(page, size));
        if (page > resultPage.getTotalPages()-1)
            throw new NotFoundException(Page.class.getSimpleName(), ResponseCode.PAGE_NOT_FOUND);
        applicationEventPublisher.publishEvent(new PaginatedResultsRetrievedEvent<OrganizationIdNameDto>(resultPage, response) );
        return resultPage.getContent();
    }


    @DeleteMapping(PATH_REVERSE_FAVOURITE_PRODUCT_DELETE)
    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteProduct(@PathVariable(value = PATH_PARAM_ID, required = true) String id){
        reverseFavouriteProductService.deleteById(id);
    }

    public static final String PATH_PARAM_ID = "id";




}
