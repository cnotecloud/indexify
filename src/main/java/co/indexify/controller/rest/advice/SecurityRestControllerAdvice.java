package co.indexify.controller.rest.advice;

import co.indexify.dto.ResponseCode;
import co.indexify.dto.ResponseDto;
import co.indexify.dto.ResponseMessage;
import co.indexify.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.nio.file.AccessDeniedException;


@ControllerAdvice
public class SecurityRestControllerAdvice extends ResponseEntityExceptionHandler {

    @Autowired
    private MessageSource messageSource;

    @ExceptionHandler({AccessDeniedException.class})
    public ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex, WebRequest request) {
        String message = messageSource.getMessage(ResponseMessage.MESSAGE_FORBIDDEN, null, request.getLocale());
        return handleExceptionInternal(ex, ResponseDto.of(message, ResponseCode.AUTHORIZATION, HttpStatus.FORBIDDEN), new HttpHeaders(), HttpStatus.FORBIDDEN, request);
    }


    @ExceptionHandler({AuthenticationException.class})
    public ResponseEntity<Object> handleAuthenticationException(AuthenticationException ex, WebRequest request) {
        String message = messageSource.getMessage(ResponseMessage.MESSAGE_AUTHENTICATION, null, request.getLocale());
        return handleExceptionInternal(ex, ResponseDto.of(message, ResponseCode.AUTHENTICATION, HttpStatus.UNAUTHORIZED), new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
    }
}
