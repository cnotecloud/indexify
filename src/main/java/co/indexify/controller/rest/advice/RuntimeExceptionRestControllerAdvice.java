package co.indexify.controller.rest.advice;

import co.indexify.controller.rest.MeController;
import co.indexify.dto.ResponseCode;
import co.indexify.dto.ResponseDto;
import co.indexify.dto.ResponseMessage;
import co.indexify.exception.NotExistException;
import co.indexify.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.nio.file.AccessDeniedException;

/**
 * Created by ankur on 26-10-2016.
 */
@RestControllerAdvice(basePackageClasses = MeController.class)
public class RuntimeExceptionRestControllerAdvice extends ResponseEntityExceptionHandler {


    @Autowired
    private MessageSource messageSource;


    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<Object> handleNotFoundException(NotFoundException ex, WebRequest request) {
        String message = messageSource.getMessage(ResponseMessage.MESSAGE_NOT_FOUND, new Object[]{ex.getEntity()}, request.getLocale());
        return handleExceptionInternal(ex, ResponseDto.of(message, ex.getCode(), HttpStatus.NOT_FOUND), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }




    @ExceptionHandler({NotExistException.class})
    public ResponseEntity<Object> handleNotExistException(NotExistException ex, WebRequest request) {
        String message = messageSource.getMessage(ResponseMessage.MESSAGE_NOT_EXIST, new Object[]{ex.getEntity()}, request.getLocale());
        return handleExceptionInternal(ex, ResponseDto.of(message, ex.getCode(), HttpStatus.CONFLICT), new HttpHeaders(), HttpStatus.CONFLICT, request);
    }






    /*@ExceptionHandler({Exception.class })
    public ResponseEntity<Object> handleRuntimeException(Exception ex, WebRequest request) {
        logger.error("500 Status Code", ex);
        String message = messageSource.getMessage("message.error", null, request.getLocale());
        return handleExceptionInternal(
                ex, ResponseDto.of(message, ResponseCode.ERROR, HttpStatus.INTERNAL_SERVER_ERROR), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }*/


    @ExceptionHandler({BadCredentialsException.class })
    public ResponseEntity<Object> handleBadCredentialsException(BadCredentialsException ex, WebRequest request) {
        String message = messageSource.getMessage("message.authentication", null, request.getLocale());
        return handleExceptionInternal(
                ex, ResponseDto.of(message, ResponseCode.AUTHENTICATION, HttpStatus.UNAUTHORIZED), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }


@ExceptionHandler({AccessDeniedException.class })
    public ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex, WebRequest request) {
    String message = messageSource.getMessage(ResponseMessage.MESSAGE_FORBIDDEN, null, "", request.getLocale());
        return handleExceptionInternal(
                ex, ResponseDto.of(message, ResponseCode.AUTHORIZATION, HttpStatus.FORBIDDEN), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }


















}
