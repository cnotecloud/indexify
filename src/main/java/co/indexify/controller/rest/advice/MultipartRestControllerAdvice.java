package co.indexify.controller.rest.advice;

import co.indexify.dto.ResponseCode;
import co.indexify.dto.ResponseDto;
import co.indexify.dto.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@RestControllerAdvice
public class MultipartRestControllerAdvice extends ResponseEntityExceptionHandler {

    @Autowired
    private MessageSource messageSource;


    @ExceptionHandler(MultipartException.class)
    protected ResponseEntity<Object> handleMultipartException(MultipartException ex, WebRequest request) {
        String message = null;
        ResponseCode code = null;
        if (ex.getMessage().contains("field")) {
            message = messageSource.getMessage(ResponseMessage.MESSAGE_FILE_UPLOAD_MAX_FILE_SIZE, new Object[]{3}, request.getLocale());
            code = ResponseCode.FILE_UPLOAD_MAX_FILE_SIZE_EXCEED;
        } else if (ex.getMessage().contains("request")) {
            code = ResponseCode.FILE_UPLOAD_MAX_REQUEST_SIZE_EXCEED;
            message = messageSource.getMessage(ResponseMessage.MESSAGE_FILE_UPLOAD_MAX_REQUEST_SIZE, new Object[]{16}, request.getLocale());
        }
        return super.handleExceptionInternal(ex, ResponseDto.of(message, code, HttpStatus.BAD_REQUEST), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }




}
