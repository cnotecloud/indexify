package co.indexify.controller.rest;

import co.indexify.dto.MethodSecurityDto;
import co.indexify.service.IOrganizationScanService;
import co.indexify.service.IUserVisitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserVisitController {

    @Autowired
    private IUserVisitorService userVisitorService;

    public static final String PATH_USER_VISITOR_DELETE = "/api/user-visitors/{id}";
    public static final String PATH_USER_VISITOR_DELETE_SECURITY = "/api/user-visitors/*";

    public static final String PATH_PARAM_ID = "id";



    @DeleteMapping(PATH_USER_VISITOR_DELETE)
    @PreAuthorize(MethodSecurityDto.ME)
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteVisit(@PathVariable(value = PATH_PARAM_ID) String id){
        userVisitorService.deleteById(id);
    }


}
