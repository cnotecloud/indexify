package co.indexify.controller.rest;

import co.indexify.annotation.CurrentUser;
import co.indexify.domain.model.Organization;
import co.indexify.domain.model.User;
import co.indexify.dto.*;
import co.indexify.exception.ValidationException;
import co.indexify.service.IOrganizationService;
import co.indexify.service.IUserService;
import co.indexify.util.SecurityUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController("organizationController")
public class OrganizationController {

    public static final String PATH_ORGANIZATION_CREATE ="/api/organization/create";
    public static final String PATH_ORGANIZATION_CATALOGUE_UPDATE ="/api/organization/catalogue";
    public static final String PATH_ORGANIZATION_CATALOGUE_ADD ="/api/organization/catalogues";

    public static final String PATH_ORGANIZATION_UPDATE ="/api/organization";



    @PutMapping(PATH_ORGANIZATION_UPDATE)
    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    public ResponseEntity<?> update(HttpServletRequest request, @CurrentUser User user, @Valid OrganizationUpdateDto organizationUpdateDto, BindingResult result){
        Util.processValidaton(result, messageSource, request);
        if(user != null && user.getOrganization() != null){
            organizationService.update(user.getOrganization(), organizationUpdateDto);
        }
        return Util.processSuccess(messageSource, request, HttpStatus.OK);
   }







    @Autowired
    private MessageSource messageSource;

    @Autowired
    private IOrganizationService organizationService;

    @PostMapping(PATH_ORGANIZATION_CREATE)
    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION_NOT_EXISTS)
    public ResponseEntity<?> createNew(HttpServletRequest request, @CurrentUser User user, @Valid OrganizationCreateDto organizationCreateDto, BindingResult result){
        String message = null;
        if(result.hasErrors()){
            message = messageSource.getMessage(ResponseMessage.MESSAGE_VALIDATION, null, request.getLocale());
            throw new ValidationException(result, message, ResponseCode.VALIDATION);
        }
        Organization registered = organizationService.registerNew(organizationCreateDto, user);
        message = messageSource.getMessage(ResponseMessage.MESSAGE_SUCCESS, null, request.getLocale());
        return ResponseEntity.status(HttpStatus.CREATED).body(ResponseDto.of(message, ResponseCode.SUCCESS, HttpStatus.CREATED).addExtra("id", registered.getId()));
    }


    @PutMapping(PATH_ORGANIZATION_CATALOGUE_UPDATE)
    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    public ResponseEntity<?> updateCatalogue(HttpServletRequest request, @CurrentUser User user, @RequestBody @Valid CalalogueDto dto, BindingResult result){
        String message = null;
        if(result.hasErrors()){
            message = messageSource.getMessage(ResponseMessage.MESSAGE_VALIDATION, null, request.getLocale());
            throw new ValidationException(result, message, ResponseCode.VALIDATION);
        }
        if(user.getOrganization()!= null){
            organizationService.updateCatalogue(user.getOrganization(), dto);
        }

        message = messageSource.getMessage(ResponseMessage.MESSAGE_SUCCESS, null, "",request.getLocale());
        return ResponseEntity.status(HttpStatus.OK).body(ResponseDto.of(message, ResponseCode.SUCCESS, HttpStatus.OK));

    }


    @PutMapping(PATH_ORGANIZATION_CATALOGUE_ADD)
    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    ResponseEntity<?> addCatalogue(HttpServletRequest request, @CurrentUser User user, @RequestBody @Valid CalalogueDto dto, BindingResult result){
        String message = null;
        if(result.hasErrors()){
            message = messageSource.getMessage(ResponseMessage.MESSAGE_VALIDATION, null, request.getLocale());
            throw new ValidationException(result, message, ResponseCode.VALIDATION);
        }
        if(user.getOrganization()!= null){
            organizationService.addCatalogue(user.getOrganization(), dto);
        }

        message = messageSource.getMessage(ResponseMessage.MESSAGE_SUCCESS, null, request.getLocale());
        return ResponseEntity.status(HttpStatus.OK).body(ResponseDto.of(message, ResponseCode.SUCCESS, HttpStatus.OK));

    }






}
