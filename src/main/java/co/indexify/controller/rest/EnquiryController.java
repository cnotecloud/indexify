package co.indexify.controller.rest;

import co.indexify.annotation.CurrentUser;
import co.indexify.domain.model.User;
import co.indexify.dto.*;
import co.indexify.exception.ValidationException;
import co.indexify.service.IEnquiryService;
import co.indexify.service.IOrganizationScanService;
import co.indexify.service.IUserScanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
public class EnquiryController {

    public static final String PATH_ENQUIRY_CREATE = "/api/enquiry";

    public static final String PATH_PARAM_ID = "id";


    @Autowired
    private IEnquiryService enquiryService;

    @Autowired
    private IOrganizationScanService organizationScanService;

    @Autowired
    private MessageSource messageSource;


    @PostMapping(PATH_ENQUIRY_CREATE)
    @PreAuthorize("isMeAndNotMyOrganization()")
    public ResponseEntity<?> createNewScan(HttpServletRequest request, @CurrentUser User user, @Param("dto") @RequestBody @Valid EnquiryDto dto, BindingResult result){
        Util.processValidaton(result, messageSource, request);
        enquiryService.createNew(dto, user, request.getLocale());
        return Util.processSuccess(messageSource, request, HttpStatus.CREATED);
    }




}
