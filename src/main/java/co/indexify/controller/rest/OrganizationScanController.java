package co.indexify.controller.rest;

import co.indexify.dto.MethodSecurityDto;
import co.indexify.service.IOrganizationScanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrganizationScanController {

    @Autowired
    private IOrganizationScanService organizationScanService;

    public static final String PATH_ORGANIZATION_SCAN_DELETE = "/api/organization-scans/{id}";
    public static final String PATH_ORGANIZATION_SCAN_DELETE_SECURITY = "/api/organization-scans/*";

    public static final String PATH_PARAM_ID = "id";



    @DeleteMapping(PATH_ORGANIZATION_SCAN_DELETE)
    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteScan(@PathVariable(value = PATH_PARAM_ID) String id){
        organizationScanService.deleteById(id);
    }


}
