package co.indexify.controller.rest;

import co.indexify.annotation.CurrentUser;
import co.indexify.domain.model.User;
import co.indexify.dto.*;
import co.indexify.exception.ValidationException;
import co.indexify.service.INodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.NodeList;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
public class NodeController {

    public static final  String PATH_CREATE_TREE = "/api/node/tree";
    public static final  String PATH_LEAF_DELETE = "/api/node/leaf/{id}";
    public static final  String PATH_LEAF_DELETE_SECURITY = "/api/node/leaf/*";
    public static final String PATH_CREATE_NODE = "/api/node";

    public static final String PATH_UPDATE_NODE = "/api/node/{id}";
    public static final String PATH_UPDATE_NODE_SECURITY = "/api/node/*";
    public static final String PATH_NODE_DELETE = "/api/node/{id}";
    public static final String PATH_NODE_DELETE_SECURITY = "/api/node/*";
	public static final String PATH_ALL_DELETE_NODE="/api/node/deleteAllStructure";



    @Autowired
    private INodeService nodeService;


    @Autowired
    private MessageSource messageSource;

    @PostMapping(PATH_CREATE_TREE)
    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    public ResponseEntity<ResponseDto> createTree(HttpServletRequest request, @CurrentUser User user, @RequestBody @Valid NodesDto nodesDto, BindingResult result){
        Util.processValidaton(result, messageSource, request);
        nodeService.saveTree(nodesDto, user);
        String message = messageSource.getMessage(ResponseMessage.MESSAGE_NODE_TREE_CREATE_SUCCESS, null, request.getLocale());
        return ResponseEntity.ok().body(ResponseDto.of(message, ResponseCode.SUCCESS, HttpStatus.CREATED));
    }

    @PutMapping(PATH_LEAF_DELETE)
    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
   // public ResponseEntity<ResponseDto> deleteLeaf(HttpServletRequest request, @CurrentUser User user, @PathVariable("id") String id, BindingResult result){
    public ResponseEntity<ResponseDto> deleteLeaf(HttpServletRequest request, @CurrentUser User user, @PathVariable("id") String id){
        //Util.processValidaton(result, messageSource, request);
        nodeService.deleteLeaf(id);
        return Util.processSuccess(messageSource, request, HttpStatus.OK);
    }


    @PostMapping(PATH_CREATE_NODE)
    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    public ResponseEntity<ResponseDto> create(HttpServletRequest request, @CurrentUser User user, @RequestBody @Valid NodeDto nodeDto, BindingResult result){
        Util.processValidaton(result, messageSource, request);
        nodeService.add(nodeDto);
        return Util.processSuccess(messageSource, request, HttpStatus.CREATED);
    }


    @PutMapping(PATH_UPDATE_NODE)
    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    public ResponseEntity<ResponseDto> edit(HttpServletRequest request, @CurrentUser User user, @PathVariable("id") String id, @RequestBody @Valid NodeUpdateDto nodeDto, BindingResult result){
        Util.processValidaton(result, messageSource, request);
        nodeService.edit(id, nodeDto);
        return Util.processSuccess(messageSource, request, HttpStatus.OK);
    }


    @DeleteMapping(PATH_NODE_DELETE)
    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    // public ResponseEntity<ResponseDto> deleteLeaf(HttpServletRequest request, @CurrentUser User user, @PathVariable("id") String id, BindingResult result){
    public ResponseEntity<ResponseDto> delete(HttpServletRequest request, @CurrentUser User user, @PathVariable("id") String id){
        //Util.processValidaton(result, messageSource, request);
        nodeService.deleteNode(id);
        return Util.processSuccess(messageSource, request, HttpStatus.OK);
    }

    @PostMapping(PATH_ALL_DELETE_NODE)
    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    public ResponseEntity<ResponseDto> deleteAllStructure(HttpServletRequest request, @CurrentUser User user, @RequestBody @Valid List<String> nodeListDto,BindingResult result){
    	 Util.processValidaton(result, messageSource, request);
    	 nodeService.deleteAllStructure(nodeListDto);
    	 return Util.processSuccess(messageSource, request, HttpStatus.OK);
    }
    
    

}
