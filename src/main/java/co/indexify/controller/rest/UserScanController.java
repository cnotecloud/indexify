package co.indexify.controller.rest;

import co.indexify.annotation.CurrentUser;
import co.indexify.domain.model.User;
import co.indexify.domain.model.UserScan;
import co.indexify.dto.*;
import co.indexify.exception.ValidationException;
import co.indexify.service.IOrganizationScanService;
import co.indexify.service.IUserScanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;

@RestController
public class UserScanController {

    public static final String PATH_USER_SCAN_CREATE = "/api/user-scan";
    public static final String PATH_USER_SCAN_DELETE = "/api/user-scan/{id}";
    public static final String PATH_USER_SCAN_DELETE_SECURITY = "/api/user-scan/*";

    public static final String PATH_PARAM_ID = "id";


    @Autowired
    private IUserScanService userScanService;

    @Autowired
    private IOrganizationScanService organizationScanService;

    @Autowired
    private MessageSource messageSource;


    @PostMapping(PATH_USER_SCAN_CREATE)
    //@PreAuthorize(MethodSecurityDto.ME_AND_NOT_MY_ORGANIZATION)
    @PreAuthorize("isMeAndNotMyOrganizationOrIsMeAndOtheIsNotMe(#dto.userId)")
    public ResponseEntity<?> createNewScan(HttpServletRequest request, @CurrentUser User user, @Param("dto") @RequestBody UserScanDto dto, BindingResult result){
        String message = null;
        if(result.hasErrors()){
            message = messageSource.getMessage(ResponseMessage.MESSAGE_VALIDATION, null, request.getLocale());
            throw new ValidationException(result, message, ResponseCode.VALIDATION);
        }
        userScanService.createNew(dto, user, request.getLocale());
        message = messageSource.getMessage(ResponseMessage.MESSAGE_USER_SCAN_CREATE_SUCCESS, null, request.getLocale());
        return ResponseEntity.status(HttpStatus.CREATED).body(ResponseDto.of(message, ResponseCode.SUCCESS, HttpStatus.CREATED));
    }


    @DeleteMapping(PATH_USER_SCAN_DELETE)
    @PreAuthorize(MethodSecurityDto.ME)
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteScan(@PathVariable(value = PATH_PARAM_ID) String id){
        userScanService.deleteById(id);
    }


}
