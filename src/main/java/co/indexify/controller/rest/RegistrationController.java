package co.indexify.controller.rest;

import co.indexify.domain.model.*;
import co.indexify.domain.model.PasswordResetToken;
import co.indexify.dto.*;
import co.indexify.event.RegistrationCompleteEvent;
import co.indexify.exception.ExistException;
import co.indexify.exception.ValidationException;
import co.indexify.security.CustomUserDetails;
import co.indexify.security.CustomUserDetailsService;
import co.indexify.security.JwtAuthenticationToken;
import co.indexify.service.IEmailService;
import co.indexify.service.IUserService;
import co.indexify.token.jwt.AccessToken;
import co.indexify.token.jwt.TokenFactory;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Collection;
import java.util.Locale;
import java.util.Optional;
import java.util.UUID;

@RestController
public class RegistrationController {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    public static final String PATH_USER_REGISTRATION = "/api/user/registration";
    public static final String PATH_USER_REGISTRATION_CONFIRM = "/api/user/registration-confirm";
    public static final String PATH_USER_REGISTRATION_EMAIL_RESEND="/api/user/registration-email-resend";
    public static final String PATH_FORGET_PASSWORD ="/api/user/forget-password";
    public static final String PATH_CHANGE_PASSWORD ="/api/user/change-password";
    public static final String PATH_SAVE_PASSWORD ="/api/user/save-password";

    @Autowired
    private IUserService userService;

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private Environment env;

    public RegistrationController() {
        super();
    }

    // Registration

    @PostMapping(PATH_USER_REGISTRATION)
    public ResponseEntity<?> registerUserAccount(final HttpServletRequest request, @Valid @RequestBody final RegistrationDto accountDto, BindingResult result) {
        String message = null;
        if(result.hasErrors()){
            message = messageSource.getMessage(ResponseMessage.MESSAGE_VALIDATION, null, request.getLocale());
            throw new ValidationException(result, message, ResponseCode.VALIDATION);
        }
        final User registered;
        try{
            registered = userService.registerNewUserAccount(accountDto);
        }catch (ExistException ex){
            if(ex.getEntity().equalsIgnoreCase("email")){
                result.rejectValue("email", "Exists", null, "Exists.userDto.email");
                message = messageSource.getMessage(ResponseMessage.MESSAGE_EMAIL_EXISTS, new Object[]{ex.getValue()}, request.getLocale());
            }else if(ex.getEntity().equalsIgnoreCase("contact")){
                result.rejectValue("contact", "Exists", null, "Exists.userDto.contact");
                message = messageSource.getMessage(ResponseMessage.MESSAGE_CONTACT_EXISTS, new Object[]{ex.getValue()}, request.getLocale());
            }
            throw new ExistException(ex.getEntity(), ex.getValue(), message, ResponseCode.EMAIL_ALREADY_EXIST, result);
        }
        eventPublisher.publishEvent(new RegistrationCompleteEvent(registered, accountDto.getTag(), request.getLocale()));
        message = message= messageSource.getMessage(ResponseMessage.MESSAGE_USER_REGISTRATION_SUCCESS, null, request.getLocale());
        return ResponseEntity.status(HttpStatus.CREATED).body(ResponseDto.of(message, ResponseCode.SUCCESS, HttpStatus.CREATED));
    }

    @GetMapping(PATH_USER_REGISTRATION_CONFIRM)
    public ResponseEntity<?> confirmRegistration(final Locale locale, @RequestParam(name = "token", required = true) final String token)  {
        final VerificationToken.VerificationTokenStatus status = userService.validateVerificationToken(token);
        String message = null;
        if (VerificationToken.VerificationTokenStatus.VALID.equals(status)) {
            final User user = userService.getUserByVerificationToken(token);
            System.out.println(user);
            /*
            if (user.getUsing2FA()) {
                model.addAttribute("qr", userService.generateQRUrl(user));
                return "redirect:/qrcode.html?lang=" + locale.getLanguage();
            } */
            message= messageSource.getMessage(ResponseMessage.MESSAGE_USER_REGISTRATION_CONFIRMED, null, locale);
            return ResponseEntity.ok(ResponseDto.of(message,ResponseCode.SUCCESS, HttpStatus.OK));
        } if(VerificationToken.VerificationTokenStatus.EXPIRED.equals(status)){
            message = messageSource.getMessage(ResponseMessage.MESSAGE_USER_REGISTRATION_NOT_CONFIRMED ,null, locale);
            return ResponseEntity.ok(ResponseDto.of(message,ResponseCode.VERIFICATION_TOKEN_EXPIRED, HttpStatus.OK).addExtra("token", token));
        }else {
            message = messageSource.getMessage(ResponseMessage.MESSAGE_USER_REGISTRATION_NOT_CONFIRMED ,null, locale);
            return ResponseEntity.ok(ResponseDto.of(message,ResponseCode.VERIFICATION_TOKEN_INVALID, HttpStatus.OK));
        }


    }

    @Autowired
    private IEmailService emailService;



    @GetMapping(PATH_USER_REGISTRATION_EMAIL_RESEND)
    public ResponseEntity<?> resendRegistrationToken(final HttpServletRequest request, @RequestParam(name = "token", required = true) final String existingToken) {
        final User user = userService.generateNewVerificationToken(existingToken);
        String message = null;
        if(user != null){
            emailService.sendForRegistration(user,"", request.getLocale());
            message = messageSource.getMessage(ResponseMessage.MESSAGE_USER_REGISTRATION_EMAIL_RESEND, null, request.getLocale());
            return ResponseEntity.ok(ResponseDto.of(message, ResponseCode.SUCCESS, HttpStatus.OK));
        }else{
            message = messageSource.getMessage(ResponseMessage.MESSAGE_USER_REGISTRATION_EMAIL_NOT_RESEND, null, request.getLocale());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ResponseDto.of(message, ResponseCode.VERIFIVATION_TOKEN_NOT_FOUND, HttpStatus.NOT_FOUND));
        }
    }

    @GetMapping(PATH_FORGET_PASSWORD)
    @ResponseBody
    public ResponseEntity<?> resetPassword(final HttpServletRequest request, @RequestParam(value = "email", required = true) final String userEmail) {
        final Optional<User> userOpt = userService.findUserByEmail(userEmail);
        String message = null;
        if (userOpt.isPresent()) {
            final String token = UUID.randomUUID()
                    .toString();
            userService.createPasswordResetTokenForUser(userOpt.get(), token);
            emailService.sendForForgetPassword(userOpt.get(), request.getLocale());
            message = messageSource.getMessage(ResponseMessage.MESSAGE_FORGOT_PASSWORD_EMAIL_SENT, null , request.getLocale());
            return ResponseEntity.ok(ResponseDto.of(message, ResponseCode.SUCCESS, HttpStatus.OK));
        }
        message = messageSource.getMessage(ResponseMessage.MESSAGE_FORGOT_PASSWORD_EMAIL_NOT_SENT, null , request.getLocale());
        return ResponseEntity.ok(ResponseDto.of(message, ResponseCode.FORGOT_PASSWORD_NOT_SENT, HttpStatus.OK));
    }


    @Autowired
    private TokenFactory tokenFactory;


    @GetMapping(PATH_CHANGE_PASSWORD)
    public ResponseEntity<?> showChangePasswordPage(final Locale locale, HttpServletRequest request, @RequestParam(name = "id", required = true) final String id, @RequestParam(name = "token", required = true) final String token) {
        CustomUserDetails userDetails = null;
        PasswordResetToken.PasswordResetTokenStatus status = PasswordResetToken.PasswordResetTokenStatus.VALID;
        try {
            userDetails = (CustomUserDetails) customUserDetailsService.loadUserByUsername(token);
        }catch (UsernameNotFoundException ex){
            status = PasswordResetToken.PasswordResetTokenStatus.INVALID;
        }
        String message = null;
        if (PasswordResetToken.PasswordResetTokenStatus.VALID.equals(status)) {
            String subject = userDetails.getUser().getEmail().getValue();
            Collection<? extends GrantedAuthority> authorities = Sets.newHashSet(new SimpleGrantedAuthority(Privilege.PRIV_USER_PASSWORD_RESET));
            UserContext userContext = UserContext.create(userDetails.getUser().getEmail().getValue(),Sets.newHashSet(Role.ROLE_USER_PASSWORD_RESET), authorities, userDetails);
            AccessToken accessToken = tokenFactory.createAccessJwtToken(userContext, 3);
            message= messageSource.getMessage(ResponseMessage.MESSAGE_CHANGE_PASSWORD_CONFIRMED, null, locale);
            return ResponseEntity.ok(ResponseDto.of(message,ResponseCode.SUCCESS, HttpStatus.OK).addExtra("accessToken", accessToken.getToken()));
        } if(PasswordResetToken.PasswordResetTokenStatus.EXPIRED.equals(status)){
            message = messageSource.getMessage(ResponseMessage.MESSAGE_CHANGE_PASSWORD_NOT_CONFIRMED,null, locale);
            return ResponseEntity.ok(ResponseDto.of(message,ResponseCode.PASSWORD_RESET_TOKEN_EXPIRED, HttpStatus.OK));
        }else {
            message = messageSource.getMessage(ResponseMessage.MESSAGE_CHANGE_PASSWORD_NOT_CONFIRMED,null, locale);
            return ResponseEntity.ok(ResponseDto.of(message,ResponseCode.PASSWORD_RESET_TOKEN_INVALID, HttpStatus.OK));
        }
    }



    @PostMapping(PATH_SAVE_PASSWORD)
    //@Secured(Privilege.PRIV_USER_PASSWORD_RESET)
    public ResponseEntity<?> savePassword(final HttpServletRequest request, @RequestBody @Valid PasswordDto passwordDto, BindingResult result) {
        String message = null;
        if(result.hasErrors()){
            message = messageSource.getMessage(ResponseMessage.MESSAGE_VALIDATION, null, request.getLocale());
            throw new ValidationException(result, message, ResponseCode.VALIDATION);
        }
        final JwtAuthenticationToken token= (JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        UserContext context = (UserContext) token.getPrincipal();
        User user= userService.findById(context.getId()).orElse(null);
        if(user!= null){
            userService.changeUserPassword(user, passwordDto.getPassword());
        }
        message = messageSource.getMessage(ResponseMessage.MESSAGE_SAVE_PASSWORD_SUCCESS, null, request.getLocale());
        return ResponseEntity.ok(ResponseDto.of(message,ResponseCode.SUCCESS, HttpStatus.OK));
    }

    /*

    // change user password
    @RequestMapping(value = "/user/updatePassword", method = RequestMethod.POST)
    @ResponseBody
    public GenericResponse changeUserPassword(final Locale locale, @Valid PasswordDto passwordDto) {
        final User user = userService.findUserByEmail(((User) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal()).getEmail());
        if (!userService.checkIfValidOldPassword(user, passwordDto.getOldPassword())) {
            throw new InvalidOldPasswordException();
        }
        userService.changeUserPassword(user, passwordDto.getNewPassword());
        return new GenericResponse(messageSource.getMessage("message.updatePasswordSuc", null, locale));
    }

    @RequestMapping(value = "/user/update/2fa", method = RequestMethod.POST)
    @ResponseBody
    public GenericResponse modifyUser2FA(@RequestParam("use2FA") final boolean use2FA) throws UnsupportedEncodingException {
        final User user = userService.updateUser2FA(use2FA);
        if (use2FA) {
            return new GenericResponse(userService.generateQRUrl(user));
        }
        return null;
    }

    // ============== NON-API ============

    private SimpleMailMessage constructResendVerificationTokenEmail(final String contextPath, final Locale locale, final VerificationToken newToken, final User user) {
        final String confirmationUrl = contextPath + "/registrationConfirm.html?token=" + newToken.getToken();
        final String message = messageSource.getMessage("message.resendToken", null, locale);
        return constructEmail("Resend Registration Token", message + " \r\n" + confirmationUrl, user);
    }

    private SimpleMailMessage constructResetTokenEmail(final String contextPath, final Locale locale, final String token, final User user) {
        final String url = contextPath + "/user/changePassword?id=" + user.getId() + "&token=" + token;
        final String message = messageSource.getMessage("message.resetPassword", null, locale);
        return constructEmail("Reset Password", message + " \r\n" + url, user);
    }

    private SimpleMailMessage constructEmail(String subject, String body, User user) {
        final SimpleMailMessage email = new SimpleMailMessage();
        email.setSubject(subject);
        email.setText(body);
        email.setTo(user.getEmail());
        email.setFrom(env.getProperty("support.email"));
        return email;
    }

    private String getAppUrl(HttpServletRequest request) {
        return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }

    */

}
