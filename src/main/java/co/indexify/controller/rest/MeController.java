package co.indexify.controller.rest;

import co.indexify.annotation.CurrentUser;
import co.indexify.domain.model.Organization;
import co.indexify.domain.model.User;
import co.indexify.dto.OrganizationGetDto;
import co.indexify.dto.UserContext;
import co.indexify.dto.UserDto;
import co.indexify.security.CustomUserDetails;
import co.indexify.security.CustomUserDetailsService;
import co.indexify.security.JwtAuthenticationToken;
import co.indexify.service.IOrganizationService;
import co.indexify.util.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;


@RestController
public class MeController {

    @Autowired
    private IOrganizationService organizationService;

    public static final String PATH_USER_ME ="/api/user/me";

    @Autowired
    private CustomUserDetailsService userDetailsService;
    @GetMapping(PATH_USER_ME)
    public @ResponseBody UserDto get(@CurrentUser User user){
        UserDto dto = UserDto.fromUser(user);
        if(user.getOrganization() != null && user.getOrganization().getId() != null){
            //dto.setOrganization(OrganizationGetDto.fromDomain(user.getOrganization()));
            dto.setOrganization(OrganizationGetDto.fromDomain(user.getOrganization()));
        }
        return dto;
    }
}
