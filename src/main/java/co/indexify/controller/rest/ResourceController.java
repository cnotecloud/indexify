package co.indexify.controller.rest;

import co.indexify.annotation.CurrentUser;
import co.indexify.domain.model.User;
import co.indexify.dto.*;
import co.indexify.exception.ValidationException;
import co.indexify.service.IResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController

public class ResourceController {
    public static final String PATH_RESOURSE_CREATE="/api/resource";
    public static final String PATH_RESOURSE_TAG_ADD="/api/resource/{id}/add-tag";
    public static final String PATH_RESOURSE_TAG_REMOVE="/api/resource/{id}/remove-tag";



    @Autowired
    private MessageSource messageSource;


    @Autowired
    private IResourceService resourceService;


    @PostMapping(PATH_RESOURSE_CREATE)
    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    public ResponseEntity<?> create(HttpServletRequest request, @CurrentUser User user, @Valid ResourcesUploadDto dto, BindingResult result){
        String message = null;
        if(result.hasErrors()){
            message = messageSource.getMessage(ResponseMessage.MESSAGE_VALIDATION, null, request.getLocale());
            throw new ValidationException(result, message, ResponseCode.VALIDATION);
        }
        resourceService.upload(dto, user);
        message = messageSource.getMessage(ResponseMessage.MESSAGE_RESOURCE_UPLOAD_SUCCESS, null, request.getLocale());
        return ResponseEntity.ok().body(ResponseDto.of(message, ResponseCode.SUCCESS, HttpStatus.CREATED));
    }


    @PutMapping(PATH_RESOURSE_TAG_ADD)
    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    ResponseEntity<?> addTag(HttpServletRequest request, @CurrentUser User user, @PathVariable String id, @Valid @RequestBody TagDto dto, BindingResult result){
        Util.processValidaton(result, messageSource, request);
        if(user!= null && user.getOrganization() != null){
            resourceService.addTag(user.getOrganization(), id, dto.getTag());
        }
        return Util.processSuccess(messageSource, request, HttpStatus.OK);
    }


    @PutMapping(PATH_RESOURSE_TAG_REMOVE)
    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    ResponseEntity<?> removeTag(HttpServletRequest request, @CurrentUser User user, @PathVariable String id, @Valid @RequestBody TagDto dto, BindingResult result){
        Util.processValidaton(result, messageSource, request);
        if(user!= null && user.getOrganization() != null){
            resourceService.removeTag(user.getOrganization(), id, dto.getTag());
        }
        return Util.processSuccess(messageSource, request, HttpStatus.OK);
    }




}
