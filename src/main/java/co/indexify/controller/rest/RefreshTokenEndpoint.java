package co.indexify.controller.rest;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.indexify.config.JwtSettings;
import co.indexify.config.WebSecurityConfig;
import co.indexify.dto.UserContext;
import co.indexify.exception.InvalidJwtToken;
import co.indexify.security.CustomUserDetails;
import co.indexify.token.Token;
import co.indexify.token.jwt.RawAccessToken;
import co.indexify.token.jwt.RefreshToken;
import co.indexify.token.jwt.TokenFactory;
import co.indexify.token.jwt.extractor.TokenExtractor;
import co.indexify.token.jwt.verifier.TokenVerifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


/**
 * RefreshTokenEndpoint
 * 
 *
 *
 * Aug 17, 2016
 */
@RestController
public class RefreshTokenEndpoint {
    @Autowired private TokenFactory tokenFactory;
    @Autowired private JwtSettings jwtSettings;
    @Autowired private UserDetailsService userService;
    @Autowired private TokenVerifier tokenVerifier;
    @Autowired @Qualifier("jwtHeaderTokenExtractor") private TokenExtractor tokenExtractor;
    
    @RequestMapping(value="/api/auth/token", method=RequestMethod.GET, produces={ MediaType.APPLICATION_JSON_VALUE })
    public @ResponseBody
    //Token refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    Map<String, String> refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String tokenPayload = tokenExtractor.extract(request.getHeader(WebSecurityConfig.AUTHENTICATION_HEADER_NAME));
        RawAccessToken rawToken = new RawAccessToken(tokenPayload);
        RefreshToken refreshTokenIn = RefreshToken.create(rawToken, jwtSettings.getTokenSigningKey()).orElseThrow(() -> new InvalidJwtToken());
        String jti = refreshTokenIn.getJti();
        if (!tokenVerifier.verify(jti)) {
            throw new InvalidJwtToken();
        }

        String subject = refreshTokenIn.getSubject();
        CustomUserDetails userDetails = (CustomUserDetails) userService.loadUserByUsername(subject);

        if (userDetails.getAuthorities() == null && userDetails.getAuthorities().isEmpty()) throw new InsufficientAuthenticationException("User has no roles assigned");

        UserContext userContext = UserContext.create(userDetails.getUsername(),userDetails.getRoles() ,userDetails.getAuthorities(), userDetails);
        Token accessToken = tokenFactory.createAccessJwtToken(userContext);
        Token refreshToken = tokenFactory.createRefreshToken(userContext);

        Map<String, String> tokenMap = new HashMap<String, String>();
        tokenMap.put("token", accessToken.getToken());
        tokenMap.put("refreshToken", refreshToken.getToken());

       // return tokenFactory.createAccessJwtToken(userContext);
        return tokenMap;
    }
}
