package co.indexify.controller.rest;

import co.indexify.annotation.CurrentUser;
import co.indexify.domain.model.CatalogueStatus;
import co.indexify.domain.model.Organization;
import co.indexify.domain.model.User;
import co.indexify.dto.*;
import co.indexify.exception.ValidationException;
import co.indexify.service.IOrganizationService;
import co.indexify.service.IUserService;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController("userController")
public class UserController {

    public static final String PATH_USER_CATALOGUE_STAUS_UPDATE ="/api/user/catalogue-status";
    public static final String PATH_USER_TAG_ADD = "/api/user/add-tag" ;
    public static final String PATH_USER_DEVICE_ADD = "/api/user/add-device";
    public static final String PATH_USER_DEVICE_REMOVE = "/api/user/remove-device";
    public static final String PATH_USER_UPADATE ="/api/user";



    @Autowired
    private MessageSource messageSource;

    @Autowired
    private IUserService userService;


    @PutMapping(PATH_USER_UPADATE)
    @PreAuthorize(MethodSecurityDto.ME)
    ResponseEntity<?> update(HttpServletRequest request, @CurrentUser User user, @Valid @RequestBody UserPutDto dto, BindingResult result){
        Util.processValidaton(result, messageSource, request);
        if(user!= null){
            userService.update(user,dto);
        }

        return Util.processSuccess(messageSource, request, HttpStatus.OK);
    }





    @PutMapping(PATH_USER_CATALOGUE_STAUS_UPDATE)
    @PreAuthorize(MethodSecurityDto.ME)
    ResponseEntity<?> updateCatalogue(HttpServletRequest request, @CurrentUser User user, @Valid @RequestBody CatalogueStatusDto dto, BindingResult result){
        String message = null;
        if(result.hasErrors()){
            message = messageSource.getMessage(ResponseMessage.MESSAGE_VALIDATION, null, request.getLocale());
            throw new ValidationException(result, message, ResponseCode.VALIDATION);
        }
        if(user!= null){
            userService.updateCatalogueStatus(user, dto);
        }

        message = messageSource.getMessage(ResponseMessage.MESSAGE_SUCCESS, null, request.getLocale());
        return ResponseEntity.status(HttpStatus.OK).body(ResponseDto.of(message, ResponseCode.SUCCESS, HttpStatus.OK));

    }



    @PutMapping(PATH_USER_TAG_ADD)
    @PreAuthorize(MethodSecurityDto.ME)
    ResponseEntity<?> addTag(HttpServletRequest request, @CurrentUser User user, @Valid @RequestBody TagDto dto, BindingResult result){
        String message = null;
        if(result.hasErrors()){
            message = messageSource.getMessage(ResponseMessage.MESSAGE_VALIDATION, null, request.getLocale());
            throw new ValidationException(result, message, ResponseCode.VALIDATION);
        }
        if(user!= null){
            userService.addTag(user, dto.getTag());
        }

        message = messageSource.getMessage(ResponseMessage.MESSAGE_SUCCESS, null, request.getLocale());
        return ResponseEntity.status(HttpStatus.OK).body(ResponseDto.of(message, ResponseCode.SUCCESS, HttpStatus.OK));
    }


    @PutMapping(PATH_USER_DEVICE_ADD)
    @PreAuthorize(MethodSecurityDto.ME)
    ResponseEntity<?> addDevice(HttpServletRequest request, @CurrentUser User user, @Valid @RequestBody DeviceDto dto, BindingResult result){
        Util.processValidaton(result, messageSource, request);
        if(user!= null){
            userService.addDevice(user, dto);
        }

        return Util.processSuccess(messageSource, request, HttpStatus.OK);
    }


    @PutMapping(PATH_USER_DEVICE_REMOVE)
    @PreAuthorize(MethodSecurityDto.ME)
    ResponseEntity<?> removeDevice(HttpServletRequest request, @CurrentUser User user, @Valid @RequestBody DeviceDto dto, BindingResult result){
        Util.processValidaton(result, messageSource, request);
        if(user!= null){
            userService.removeDevice(user, dto);
        }

        return Util.processSuccess(messageSource, request, HttpStatus.OK);
    }










}
