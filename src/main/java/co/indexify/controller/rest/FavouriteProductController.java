package co.indexify.controller.rest;

import co.indexify.annotation.CurrentUser;
import co.indexify.domain.model.FavouriteProduct;
import co.indexify.domain.model.User;
import co.indexify.dto.*;
import co.indexify.event.PaginatedResultsRetrievedEvent;
import co.indexify.event.ResourceCreatedEvent;
import co.indexify.exception.NotFoundException;
import co.indexify.exception.ValidationException;
import co.indexify.service.IFavouriteProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
public class FavouriteProductController {

    @Autowired
    private IFavouriteProductService favouriteProductService;

    @Autowired
    private MessageSource messageSource;


    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    public static final String PATH_FAVOURITE_PRODUCT_CREATE = "/api/favourite-products";

    public static final String PATH_FAVOURITE_PRODUCT_ORGANIZATIONS_ID_NAME_GET = "/api/favourite-products/organizations";

    public static final String RESOURCE_ID = "id";
    public static final String QUERY_PARAM_SIZE = "size";
    public static final String QUERY_PARAM_PAGE = "page";
    public static final String QUERY_PARAM_SIZE_DEFAULT = "20";
    public static final String QUERY_PARAM_PAGE_DEFAULT = "0";

    public static final String PATH_FAVOURITE_PRODUCT_DELETE = "/api/favourite-products/{id}";
    public static final String PATH_FAVOURITE_PRODUCT_DELETE_SECURITY = "/api/favourite-products/*";





    @PostMapping(PATH_FAVOURITE_PRODUCT_CREATE)
    @PreAuthorize(MethodSecurityDto.ME_AND_NOT_MY_ORGANIZATION)
    ResponseEntity<?> create(HttpServletResponse response, HttpServletRequest request, @CurrentUser User user, @Valid @RequestBody FavouriteProductDto dto, BindingResult result) {
        String message = null;
        if (result.hasErrors()) {
            message = messageSource.getMessage(ResponseMessage.MESSAGE_VALIDATION, null, request.getLocale());
            throw new ValidationException(result, message, ResponseCode.VALIDATION);
        }
        FavouriteProduct product = favouriteProductService.create(dto, user, request.getLocale());
        applicationEventPublisher.publishEvent(new ResourceCreatedEvent(product,response, product.getId()));
        message = messageSource.getMessage(ResponseMessage.MESSAGE_FAVOURITE_PRODUCT_CREATE_SUCCESS, null, request.getLocale());
        return ResponseEntity.status(HttpStatus.CREATED).body(ResponseDto.of(message, ResponseCode.SUCCESS, HttpStatus.CREATED).addExtra(RESOURCE_ID, product.getId()));
    }


    @GetMapping(PATH_FAVOURITE_PRODUCT_ORGANIZATIONS_ID_NAME_GET)
    @PreAuthorize(MethodSecurityDto.ME)
    List<OrganizationIdNameDto> organizationsIdName(HttpServletResponse response, HttpServletRequest request, @CurrentUser User user, @RequestParam(name = QUERY_PARAM_PAGE, required = false, defaultValue = QUERY_PARAM_PAGE_DEFAULT) int page, @RequestParam(name = QUERY_PARAM_SIZE, required = false, defaultValue = QUERY_PARAM_SIZE_DEFAULT) int size) {
        Page<OrganizationIdNameDto> resultPage = favouriteProductService.getOrganizationIdName(PageRequest.of(page, size));
        if (page > resultPage.getTotalPages()-1)
            throw new NotFoundException(Page.class.getSimpleName(), ResponseCode.PAGE_NOT_FOUND);
        applicationEventPublisher.publishEvent(new PaginatedResultsRetrievedEvent<OrganizationIdNameDto>(resultPage, response) );
        return resultPage.getContent();
    }


    @DeleteMapping(PATH_FAVOURITE_PRODUCT_DELETE)
    @PreAuthorize(MethodSecurityDto.ME)
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteProduct(@PathVariable(value = PATH_PARAM_ID, required = true) String id){
        favouriteProductService.deleteById(id);
    }

    public static final String PATH_PARAM_ID = "id";




}
