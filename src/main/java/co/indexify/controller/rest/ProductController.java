package co.indexify.controller.rest;

import co.indexify.annotation.CurrentUser;
import co.indexify.domain.model.Organization;
import co.indexify.domain.model.Product;
import co.indexify.domain.model.User;
import co.indexify.dto.*;
import co.indexify.exception.ValidationException;
import co.indexify.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
public class ProductController  {

    public static final String PATH_PRODUCT_CREATE ="/api/product";
    public static final String PATH_PRODUCT_CREATE_CSV ="/api/product/csv";

    public static final String PATH_PRODUCT_UPDATE= "/api/product/{id}";
    public static final String PATH_PRODUCT_UPDATE_SECURITY= "/api/product/*";
    public static final String PATH_PRODUCT_NODE_LINK = "/api/product/node/link";
    public static final String PATH_PRODUCT_NODE_UNLINK = "/api/product/node/unlink";
    
    @Autowired
    private MessageSource messageSource;

    @Autowired
    private IProductService productService;


    @PostMapping(PATH_PRODUCT_CREATE)
    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    public ResponseEntity<?> create(HttpServletRequest request, @CurrentUser User user, @Validated ProductCreateDto productCreateDto, BindingResult result){
        String message = null;
        if(result.hasErrors()){
            message = messageSource.getMessage(ResponseMessage.MESSAGE_VALIDATION, null, request.getLocale());
            throw new ValidationException(result, message, ResponseCode.VALIDATION);
        }
        Product product = productService.create(productCreateDto, user);
        message = messageSource.getMessage(ResponseMessage.MESSAGE_PRODUCT_CREATE_SUCCESS, null, request.getLocale());
        return ResponseEntity.status(HttpStatus.CREATED).body(ResponseDto.of(message, ResponseCode.SUCCESS, HttpStatus.CREATED));
    }



    @PostMapping(PATH_PRODUCT_CREATE_CSV)
    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    public ResponseEntity<?> createByCsv(HttpServletRequest request, @CurrentUser User user, @Validated ProductCreateCsvDto dto, BindingResult result){
        Util.processValidaton(result, messageSource, request);
        if(user!= null && user.getOrganization() != null)
            productService.createByCsv(dto, user);
        return Util.processSuccess(messageSource, request, HttpStatus.CREATED);
    }





    @PostMapping(PATH_PRODUCT_UPDATE)
    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    public ResponseEntity<?> update(HttpServletRequest request,@CurrentUser User user, @PathVariable(required = true) String id, ProductUpdateDto productUpdateDto, BindingResult result){
        String message = null;
        if(result.hasErrors()){
            message = messageSource.getMessage(ResponseMessage.MESSAGE_VALIDATION, null, request.getLocale());
            throw new ValidationException(result, message, ResponseCode.VALIDATION);
        }
        Product product = productService.update(id, productUpdateDto, user);
        message = messageSource.getMessage(ResponseMessage.MESSAGE_PRODUCT_UPDATE_SUCCESS, null, request.getLocale());
        return ResponseEntity.status(HttpStatus.OK).body(ResponseDto.of(message, ResponseCode.SUCCESS, HttpStatus.OK));
    }


    @PostMapping(PATH_PRODUCT_NODE_LINK)
    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    public ResponseEntity<?> linkToNode(HttpServletRequest request, @CurrentUser User user,@Valid @RequestBody ProductNodeLinkDto productNodeLinkDto, BindingResult result){
        String message = null;
        if(result.hasErrors()){
            message = messageSource.getMessage(ResponseMessage.MESSAGE_VALIDATION, null, request.getLocale());
            throw new ValidationException(result, message, ResponseCode.VALIDATION);
        }
        productService.linkToNode(productNodeLinkDto, user);
        message = messageSource.getMessage(ResponseMessage.MESSAGE_PRODUCT_NODE_LINK_SUCCESS, null, request.getLocale());
        return ResponseEntity.status(HttpStatus.OK).body(ResponseDto.of(message, ResponseCode.SUCCESS, HttpStatus.OK));
    }
    
    @PostMapping(PATH_PRODUCT_NODE_UNLINK)
    @PreAuthorize(MethodSecurityDto.MY_ORGANIZATION)
    public ResponseEntity<?> unlinkToNode(HttpServletRequest request, @CurrentUser User user,@Valid @RequestBody ProductNodeUnLinkDto productNodeLinkDto, BindingResult result){
        String message = null;
        if(result.hasErrors()){
            message = messageSource.getMessage(ResponseMessage.MESSAGE_VALIDATION, null, request.getLocale());
            throw new ValidationException(result, message, ResponseCode.VALIDATION);
        }
        productService.unlinkToNode(productNodeLinkDto, user);
        message = messageSource.getMessage(ResponseMessage.MESSAGE_PRODUCT_NODE_UNLINK_SUCCESS, null, request.getLocale());
        return ResponseEntity.status(HttpStatus.OK).body(ResponseDto.of(message, ResponseCode.SUCCESS, HttpStatus.OK));
    }

}
