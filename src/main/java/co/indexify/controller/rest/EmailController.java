package co.indexify.controller.rest;


import co.indexify.annotation.CurrentUser;
import co.indexify.domain.model.Organization;
import co.indexify.domain.model.User;
import co.indexify.dto.*;
import co.indexify.exception.ValidationException;
import co.indexify.service.IEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;


@RestController
public class EmailController {

    public static final String PATH_EMAILS = "/api/emails";

    @Autowired
    private MessageSource messageSource;


    @Autowired
    private IEmailService emailService;


    @PostMapping(PATH_EMAILS)
    public ResponseEntity<?> createNew(HttpServletRequest request, @RequestBody @Valid EmailPostDto emailPostDto, BindingResult result){
        String message = null;
        if(result.hasErrors()){
            message = messageSource.getMessage(ResponseMessage.MESSAGE_VALIDATION, null, request.getLocale());
            throw new ValidationException(result, message, ResponseCode.VALIDATION);
        }
        emailService.send(emailPostDto);
        message = messageSource.getMessage(ResponseMessage.MESSAGE_SUCCESS, null, request.getLocale());
        return ResponseEntity.status(HttpStatus.CREATED).body(ResponseDto.of(message, ResponseCode.SUCCESS, HttpStatus.OK));
    }

}

