package co.indexify.controller.rest;

import co.indexify.dto.ResponseCode;
import co.indexify.dto.ResponseDto;
import co.indexify.dto.ResponseMessage;
import co.indexify.exception.ValidationException;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import javax.servlet.http.HttpServletRequest;

public class Util {

    public static void processValidaton(BindingResult result, MessageSource messageSource, HttpServletRequest request){
        if(result.hasErrors()){
            String message = messageSource.getMessage(ResponseMessage.MESSAGE_VALIDATION, null, request.getLocale());
            throw new ValidationException(result, message, ResponseCode.VALIDATION);
        }
    }


    public static ResponseEntity<ResponseDto> processSuccess(MessageSource messageSource, HttpServletRequest request, HttpStatus code){
        String message = messageSource.getMessage(ResponseMessage.MESSAGE_SUCCESS, null, request.getLocale());
        return ResponseEntity.ok().body(ResponseDto.of(message, ResponseCode.SUCCESS, code));
    }
}
