package co.indexify.controller.rest;


import co.indexify.annotation.CurrentUserContext;
import co.indexify.annotation.CurrentUserDetails;
import co.indexify.config.JwtSettings;
import co.indexify.domain.model.Otp;
import co.indexify.dto.LoginOtpDto;
import co.indexify.dto.PasswordLoginDto;
import co.indexify.dto.UserContext;
import co.indexify.service.IUserService;
import co.indexify.token.Token;
import co.indexify.token.jwt.TokenFactory;
import co.indexify.util.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
public class LoginController {

    @Autowired
    private PasswordEncoder encoder;

    private final String MESSAGE_USER_DISABLED_TEMP="Authentication failed. User is not enabled.";
    private final String MESSAGE_USER_INVALID_CREDENTIAL_TEMP ="Authentication Failed. Username or Password not valid.";
    private final String MESSAGE_USER_NO_ROLE_ASSIGNED_TEMP ="Authentication Failed. User is not assigned any role.";

    @Autowired private TokenFactory tokenFactory;
    @Autowired private JwtSettings jwtSettings;
    @Autowired private UserDetailsService userDetailsService;
    @Autowired private IUserService userService;


    public static final String PATH_AUTHENTICATION_PASSWORD ="/api/auth/password";


    public static final String PATH_AUTHENTICATION_OTP="/api/auth/otp";

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;



    @PostMapping(PATH_AUTHENTICATION_PASSWORD)
    public Map<String, String> loginWithPassword(HttpServletRequest request, @CurrentUserContext UserContext userContext, @RequestBody PasswordLoginDto dto){
        if(userContext.getUserDetails()!= null && userContext.getUserDetails().getUser()!= null && !userContext.getUserDetails().getUser().getEnabled()){
            throw new DisabledException(MESSAGE_USER_DISABLED_TEMP);
        }

        if (dto.getPassword()!= null && userContext.getUserDetails()!= null && userContext.getUserDetails().getUser()!= null && !encoder.matches(dto.getPassword(), userContext.getUserDetails().getUser().getPassword())) {
            throw new BadCredentialsException(MESSAGE_USER_INVALID_CREDENTIAL_TEMP);
        }
        if(SecurityUtil.getAuthentication().isPresent()){
            applicationEventPublisher.publishEvent(new AuthenticationSuccessEvent(SecurityUtil.getAuthentication().get()));
        }

        UserContext userContextOut = UserContext.create(userContext.getUserDetails().getUsername(),userContext.getUserDetails().getUser().getRoles(), userContext.getAuthorities(), userContext.getUserDetails());


        Token accessToken = tokenFactory.createAccessJwtToken(userContextOut);
        Token refreshToken = tokenFactory.createRefreshToken(userContextOut);

        Map<String, String> tokenMap = new HashMap<String, String>();
        tokenMap.put("token", accessToken.getToken());
        tokenMap.put("refreshToken", refreshToken.getToken());

        return tokenMap;
    }

    @PostMapping(PATH_AUTHENTICATION_OTP)
    public Map<String, String> loginWithOpt(HttpServletRequest request, @CurrentUserContext UserContext userContext, @RequestBody LoginOtpDto dto){

        if (dto.getOtp()!= null && userContext.getUserDetails()!= null && userContext.getUserDetails().getUser()!= null && !userService.validateLoginOtp(userContext.getUserDetails().getUser(), dto.getOtp()).equals(Otp.OtpStatus.VALID)) {
            throw new BadCredentialsException(MESSAGE_USER_INVALID_CREDENTIAL_TEMP);
        }

        /*

        if(userContext.getUserDetails()!= null && userContext.getUserDetails().getUser()!= null &&!userContext.getUserDetails().getUser().getEnabled()){
           // userContext.getUserDetails().getUser().setEnabled(true);
        }

        */

        if(SecurityUtil.getAuthentication().isPresent()){
            applicationEventPublisher.publishEvent(new AuthenticationSuccessEvent(SecurityUtil.getAuthentication().get()));
        }



        UserContext userContextOut = UserContext.create(userContext.getUserDetails().getUsername(),userContext.getUserDetails().getUser().getRoles(), userContext.getAuthorities(), userContext.getUserDetails());

        Token accessToken = tokenFactory.createAccessJwtToken(userContextOut);
        Token refreshToken = tokenFactory.createRefreshToken(userContextOut);

        Map<String, String> tokenMap = new HashMap<String, String>();
        tokenMap.put("token", accessToken.getToken());
        tokenMap.put("refreshToken", refreshToken.getToken());

        // return tokenFactory.createAccessJwtToken(userContext);
        return tokenMap;
    }

}
