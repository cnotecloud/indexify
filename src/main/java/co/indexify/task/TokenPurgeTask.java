package co.indexify.task;

import co.indexify.service.IUserService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;

@Service
public class TokenPurgeTask {

    private final IUserService userService;

    public TokenPurgeTask(IUserService userService) {
        this.userService = userService;
    }

    @Scheduled(cron = "${purge.cron.expression}")
    public void purgeExpired() {
        Date now = Date.from(Instant.now());
        userService.deletePasswordResetAllExpiredSince(now);
        userService.deleteVerificationTokenVerified();
    }
}