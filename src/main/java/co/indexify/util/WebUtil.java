package co.indexify.util;

import javax.servlet.http.HttpServletRequest;

import co.indexify.config.WebSecurityConfig;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.web.context.request.WebRequest;

import java.util.Locale;

/**
 * 
 *
 *
 * Aug 3, 2016
 */
public class WebUtil {
    private static final String XML_HTTP_REQUEST = "XMLHttpRequest";
    private static final String X_REQUESTED_WITH = "X-Requested-With";
    private static final String X_TAG = "X-Tag";
    private static final String X_REQUSETED_ORGANIZATION = "X-Requested-Organization";
    private static final String X_REQUSETED_USER = "X-Requested-User";
    private static final String CONTENT_TYPE = "Content-type";
    private static final String CONTENT_TYPE_JSON = "application/json";
    private static final String X_OTP = "X-Otp";

    public static boolean isAjax(HttpServletRequest request) {
        return XML_HTTP_REQUEST.equals(request.getHeader(X_REQUESTED_WITH));
    }

    public static String getJwtAuthorizationHeader(HttpServletRequest request){
        return request.getHeader(WebSecurityConfig.AUTHENTICATION_HEADER_NAME);
    }


    public static boolean isAjax(SavedRequest request) {
        return request.getHeaderValues(X_REQUESTED_WITH).contains(XML_HTTP_REQUEST);
    }

    public static boolean isContentTypeJson(SavedRequest request) {
        return request.getHeaderValues(CONTENT_TYPE).contains(CONTENT_TYPE_JSON);
    }

    public static String getRequestedOrganization(HttpServletRequest request){
        return request.getHeader(X_REQUSETED_ORGANIZATION);
    }

    public static String getTag(HttpServletRequest request){
        return request.getHeader(X_TAG);
    }

    public static String getRequestedUser(HttpServletRequest request){
        return request.getHeader(X_REQUSETED_USER);
    }
    public static String getClientIP(HttpServletRequest request) {
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        return ipAddress;
    }

    public static Boolean getOtp(HttpServletRequest request) {
        return Boolean.valueOf(request.getHeader(X_OTP));
    }

    public static Locale getLocale(HttpServletRequest request) {
        return request.getLocale();
    }
}
