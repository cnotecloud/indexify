package co.indexify.util;

import co.indexify.domain.model.DomainContext;
import co.indexify.domain.model.User;
import co.indexify.dto.UserContext;
import co.indexify.security.JwtAuthenticationToken;
import co.indexify.security.SecurityContextImplExtended;
import co.indexify.security.jwt.JwtTokenAuthenticationProcessingFilter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

public class SecurityUtil {
    public static Optional<User> getMe() {
        return getMe(getAuthentication().orElse(null));
    }

    public static Optional<User> getMe(Authentication authentication){
        Optional<UserContext> userContext = getUserContext(authentication);
        if (userContext.isPresent() && userContext.get().getUserDetails() != null && userContext.get().getUserDetails().getUser() != null) {
            return Optional.of(userContext.get().getUserDetails().getUser());
        }
        return Optional.empty();
    }


    public static Boolean getOpt(DomainContext domainContext){
        if(domainContext!= null  && domainContext.getOtp() != null)
            return domainContext.getOtp();
        else return false;
    }


    public static Boolean getOtp(){
        return getOpt(getDomainContext().orElse(null));
    }


    public static Boolean getOtp(Authentication authentication){
        return getOpt(getDomainContext(authentication).orElse(null));
    }





    public static Optional<String> getTag(DomainContext domainContext){
        if(domainContext!= null  && domainContext.getTag() != null)
            return Optional.of(domainContext.getTag());
        else return Optional.empty();
    }

    public static Optional<String> getTag(){
        return getTag(getDomainContext().orElse(null));
    }



    public static String getRequestedOrganization(){
        if (getDomainContext().isPresent()) {
            return getDomainContext().get().getRequestedOrganization();
        }
        return null;
    }

    public static String getRequestedUser(){
        if (getDomainContext().isPresent()) {
            return getDomainContext().get().getRequestedUser();
        }
        return null;
    }


    public static Optional<DomainContext> getDomainContext() {
        return getDomainContext(getAuthentication().orElse(null));
    }

    public static Optional<DomainContext> getDomainContext(Authentication authentication) {
        if(authentication!= null){
            if(authentication.getDetails() != null && authentication.getDetails() instanceof DomainContext){
                DomainContext domainContext = (DomainContext) authentication.getDetails();
                return Optional.of(domainContext);
            }
        }
        return Optional.empty();
    }


    public static Optional<String> getIp(DomainContext domainContext){
        if(domainContext!= null  && domainContext.getOtp() != null)
            return Optional.of(domainContext.getRemoteAddress());
        else return Optional.empty();
    }


    public static Optional<String> getIp(){
        return getIp(getSecurityContext().isPresent() ?  getSecurityContext().get().getDomainContext() : null);
    }



    public static Optional<Authentication> getAuthentication() {
        return getAuthentication(SecurityContextHolder.getContext().getAuthentication());
    }

    public static Optional<Authentication> getAuthentication(Authentication authentication){
        if (authentication!= null) {
            return Optional.of(authentication);
        }
        return Optional.empty();
    }

    public static Optional<UserContext> getUserContext() {
        return getUserContext(getAuthentication().orElse(null));
    }



    public static Optional<UserContext> getUserContext(Authentication authentication) {
        if(authentication!= null){
            if(authentication.getPrincipal() != null && authentication.getPrincipal() instanceof UserContext){
                UserContext userContext = (UserContext) authentication.getPrincipal();
                return Optional.of(userContext);
            }
        }
        return Optional.empty();
    }


    public static Optional<SecurityContextImplExtended> getSecurityContext() {
        SecurityContext context = SecurityContextHolder.getContext();
        if(context!= null && context instanceof SecurityContextImplExtended){
            return Optional.of((SecurityContextImplExtended) context);
        }
        return Optional.empty();
    }







    public static String getOrganization() {
        String organizationId = getRequestedOrganization();
        if(StringUtils.isBlank(organizationId))
            organizationId = getMeOrganization();
        if(StringUtils.isBlank(organizationId))
            organizationId = "0";
        return organizationId;
    }


    public static String getMeOrganization(){
        Optional<User> me = getMe();
        if(me.isPresent() && me.get().getOrganization() != null)
            return String.valueOf(me.get().getOrganization().getId());
        return null;
    }

    public static boolean isMyOrganization(String myOrganizationId){
        return getOrganization().equals(myOrganizationId);
    }

    public static String getUser(){
        String userId = getRequestedUser();
        if(StringUtils.isBlank(userId)){
            userId = getMeId();
        }
        if(StringUtils.isBlank(userId))
            userId = "0";
        return userId;
    }





    public static Boolean isMe(String meId){
        return getUser().equals(meId);
    }


    public static String getMeId(){
        Optional<User> me = getMe();
        if(me.isPresent()  && me.get().getId()!= null)
            return String.valueOf(me.get().getId());
        else return null;
    }
}
