package co.indexify.util;

import com.google.common.collect.Sets;

import java.util.Set;

public class FileUtil {
    public static final Set<String> docMimes = Sets.newHashSet(
            "application/pdf",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "application/msword",
            "application/vnd.ms-excel",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "application/vnd.ms-powerpoint",
            "application/vnd.openxmlformats-officedocument.presentationml.presentation",
            "text/csv",
            "text/plain"
    );


    public static final Set<String> imageMimes = Sets.newHashSet(
            "image/jpeg",
            "image/png",
            "image/gif",
            "image/bmp"
    );

}
