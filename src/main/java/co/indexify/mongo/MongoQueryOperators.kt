package co.indexify.mongo

class MongoQueryOperators{
    companion object {
        const val eq = "\$eq"
        val gt = "\$gt"
        val gte = "\$gte"
        val In = "\$in"
        val lt = "\$lt"
        val lte = "\$lte"
        val ne = "\$ne"
        val nin = "\$nin"
        const val and = "\$and"
        val not = "\$not"
        val nor = "\$nor"
        const val or = "\$or"
        const val exists = "\$exists"
        val type = "\$type"
        val expr = "\$expr"
        val jsonSchema = "\$jsonSchema"
        val mod = "\$mod"
        const val regex = "\$regex"
        val text = "\$text"
        val where = "\$where"
        val geoIntersects = "\$geoIntersects"
        val geoWithin = "\$geoWithin"
        val near = "\$near"
        val nearSphere = "\$nearSphere"
        val all = "\$all"
        val size = "\$size"
        val bitsAllClear = "\$bitsAllClear"
        val bitsAllSet = "\$bitsAllSet"
        val bitsAnyClear = "\$bitsAnyClear"
        val bitsAnySet = "\$bitsAnySet"
        val comment = "\$comment"
        val meta = "\$meta"
        val slice = "\$slice"
        const val query = "\$query"
        const val orderby = "\$orderby"

        const val objectToArray = "\$objectToArray"

        const val ROOT = "\$\$ROOT"

        const val options = "\$options"

    }

}