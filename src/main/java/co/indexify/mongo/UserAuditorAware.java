package co.indexify.mongo;

import co.indexify.domain.model.User;
import co.indexify.dto.UserContext;
import co.indexify.security.CustomUserDetails;
import co.indexify.security.CustomUserDetailsService;
import co.indexify.security.JwtAuthenticationToken;
import co.indexify.service.IUserService;
import co.indexify.util.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Created by ankur on 13-08-2016.
 */
@Component
public class UserAuditorAware implements AuditorAware<User> {

    @Autowired
    private IUserService userService;
    @Override
    public Optional<User> getCurrentAuditor() {
        return SecurityUtil.getMe().isPresent()? SecurityUtil.getMe(): Optional.of(User.ANONYMOUS_USER);
    }
}
