package co.indexify.csv.dto;

import co.indexify.domain.model.Control;
import com.opencsv.bean.CsvBindAndJoinByName;
import com.opencsv.bean.CsvBindAndSplitByName;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import org.apache.commons.collections4.MultiValuedMap;
import org.springframework.util.CollectionUtils;

import java.util.*;

public class ProductCsvDto {

    @CsvBindByName(column = "name", required = true)
    private String name;


    @CsvBindAndJoinByName(column = ".*", elementType = String.class, required = true)
    private MultiValuedMap<String, String> fields;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MultiValuedMap<String, String> getFields() {
        return fields;
    }

    public void setFields(MultiValuedMap<String, String> fields) {
        this.fields = fields;
    }


    public Map<String, String> fieldsAsMap(){
        if(fields != null && !fields.isEmpty()){
            Map<String, Collection<String>> fieldsMap = fields.asMap();
            Map<String, String> transformedFiledsMap = new HashMap<>();

            fieldsMap.entrySet().forEach(entry -> {
                transformedFiledsMap.put(entry.getKey(), entry.getValue().stream().findFirst().orElse(""));
            });
            return transformedFiledsMap;
        }
        return Collections.emptyMap();
    }


    public List<Control> fieldsAsList(){
        if(fields != null && !fields.isEmpty()){
            Map<String, Collection<String>> fieldsMap = fields.asMap();
            List<Control> transformedFiledsMap = new ArrayList<>();

            fieldsMap.entrySet().forEach(entry -> {
                transformedFiledsMap.add(new Control(entry.getKey(), entry.getValue().stream().findFirst().orElse("")));
            });
            return transformedFiledsMap;
        }
        return Collections.emptyList();
    }



}
