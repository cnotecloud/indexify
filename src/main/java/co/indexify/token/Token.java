package co.indexify.token;

public interface Token {
    String getToken();
}
