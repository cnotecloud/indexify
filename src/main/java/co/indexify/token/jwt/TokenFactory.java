package co.indexify.token.jwt;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;
import java.util.stream.Collectors;

import co.indexify.config.JwtSettings;
import co.indexify.dto.Scopes;
import co.indexify.dto.UserContext;
import co.indexify.token.Token;
import com.google.common.collect.ImmutableMap;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



/**
 * Factory class that should be always used to create {@link Token}.
 * 
 *
 *
 * May 31, 2016
 */
@Component
public class TokenFactory {
    private final JwtSettings settings;

    @Autowired
    public TokenFactory(JwtSettings settings) {
        this.settings = settings;
    }

    public AccessToken createAccessJwtToken(UserContext userContext, Integer tokenExpirationTime) {
        if (StringUtils.isBlank(userContext.getUsername())) 
            throw new IllegalArgumentException("Cannot create JWT Token without username");

        if (userContext.getAuthorities() == null || userContext.getAuthorities().isEmpty()) 
            throw new IllegalArgumentException("User doesn't have any privileges");

        Claims claims = Jwts.claims().setSubject(userContext.getUsername());
       // claims.put("scopes", userContext.getAuthorities().stream().map(s -> s.toString()).collect(Collectors.toList()));
        claims.put("scopes", userContext.getRoles().stream().map(s -> s.toString()).collect(Collectors.toList()));
        if(userContext.getId() != null){
                claims.put("id", userContext.getId());
                if(userContext.getOrganization()!= null){
                    claims.putAll(ImmutableMap.of("organization", userContext.getOrganization()));
                }
        }
        LocalDateTime currentTime = LocalDateTime.now();
        
        String token = Jwts.builder()
          .setClaims(claims)
          .setIssuer(settings.getTokenIssuer())
          .setIssuedAt(Date.from(currentTime.atZone(ZoneId.systemDefault()).toInstant()))
          .setExpiration(Date.from(currentTime
              .plusMinutes(tokenExpirationTime)
              .atZone(ZoneId.systemDefault()).toInstant()))
          .signWith(SignatureAlgorithm.HS512, settings.getTokenSigningKey())
        .compact();

        return new AccessToken(token, claims);
    }


    public AccessToken createAccessJwtToken(UserContext userContext){
        return createAccessJwtToken(userContext, settings.getTokenExpirationTime());
    }




    public Token createRefreshToken(UserContext userContext, Integer refreshTokenExpirationTime) {
        if (StringUtils.isBlank(userContext.getUsername())) {
            throw new IllegalArgumentException("Cannot create JWT Token without username");
        }

        LocalDateTime currentTime = LocalDateTime.now();

        Claims claims = Jwts.claims().setSubject(userContext.getUsername());
        claims.put("scopes", Arrays.asList(Scopes.REFRESH_TOKEN.authority()));
        
        String token = Jwts.builder()
          .setClaims(claims)
          .setIssuer(settings.getTokenIssuer())
          .setId(UUID.randomUUID().toString())
          .setIssuedAt(Date.from(currentTime.atZone(ZoneId.systemDefault()).toInstant()))
          .setExpiration(Date.from(currentTime
              .plusMinutes(refreshTokenExpirationTime)
              .atZone(ZoneId.systemDefault()).toInstant()))
          .signWith(SignatureAlgorithm.HS512, settings.getTokenSigningKey())
        .compact();

        return new AccessToken(token, claims);
    }

    public Token createRefreshToken(UserContext userContext){
        return createRefreshToken(userContext, settings.getRefreshTokenExpirationTime());
    }
}
