package com.ankurpathak.server.validation.constraints.validator;


import co.indexify.dto.PasswordDto;
import co.indexify.dto.RegistrationDto;
import com.ankurpathak.server.validation.constraints.PasswordMatches;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by ankur on 23-10-2016.
 */

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {
    @Override
    public void initialize(PasswordMatches passwordMatches) {

    }

    @Override
    public boolean isValid(Object objectDto, ConstraintValidatorContext constraintValidatorContext) {
        if(objectDto instanceof RegistrationDto){
            RegistrationDto registrationDto = (RegistrationDto) objectDto;
            String password = registrationDto.getPassword() ;
            if(password == null)
                password = "";
            return password.equals(registrationDto.getMatchingPassword());
        }else if(objectDto instanceof PasswordDto){
            PasswordDto passwordDto = (PasswordDto) objectDto;
            String password = passwordDto.getPassword() ;
            if(password == null)
                password = "";
            return password.equals(passwordDto.getMatchingPassword());
        }
        return false;
    }
}


