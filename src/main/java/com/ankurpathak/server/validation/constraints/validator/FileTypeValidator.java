package com.ankurpathak.server.validation.constraints.validator;

import co.indexify.util.FileUtil;
import com.ankurpathak.server.validation.constraints.FileType;
import org.apache.tika.Tika;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FileTypeValidator implements ConstraintValidator<FileType, MultipartFile> {
   private FileType fileType;
   public void initialize(FileType fileType) {
      this.fileType = fileType;
   }

   public boolean isValid(MultipartFile file, ConstraintValidatorContext context) {
      if(file == null)
         return true;
      String fileName = file.getOriginalFilename();
      Tika tika = new Tika();
      String mimeType = tika.detect(fileName);
      if(FileUtil.docMimes.contains(mimeType) || FileUtil.imageMimes.contains(mimeType))
         return true;
      else
         return false;

   }
}
