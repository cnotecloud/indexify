package com.ankurpathak.server.validation.constraints.validator;

import co.indexify.aws.sns.StringUtils;
import com.ankurpathak.server.validation.constraints.Any;
import com.ankurpathak.server.validation.constraints.Email;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Created by ankur on 04-02-2017.
 */
public class AnyValidator implements ConstraintValidator<Any, String>{

    private String[] value;

    @Override
    public void initialize(Any email) {
        this.value = email.value();
    }

    @Override
    public boolean isValid(String any, ConstraintValidatorContext constraintValidatorContext) {
        if(StringUtils.isEmpty(any))
            return true;

        boolean result = Stream.of(value).anyMatch(x -> Objects.equals(x, any));

        return result;

    }

}
