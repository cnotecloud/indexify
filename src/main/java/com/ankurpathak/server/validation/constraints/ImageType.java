package com.ankurpathak.server.validation.constraints;

import com.ankurpathak.server.validation.constraints.validator.FileTypeValidator;
import com.ankurpathak.server.validation.constraints.validator.ImageTypeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RUNTIME)
@Constraint(validatedBy = ImageTypeValidator.class)
@Documented
public @interface ImageType {

    String message() default "{com.ankurpathak.server.validation.constraints.ImageType.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
