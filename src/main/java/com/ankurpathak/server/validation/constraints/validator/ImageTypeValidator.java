package com.ankurpathak.server.validation.constraints.validator;

import co.indexify.util.FileUtil;
import com.ankurpathak.server.validation.constraints.FileType;
import com.ankurpathak.server.validation.constraints.ImageType;
import org.apache.tika.Tika;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ImageTypeValidator implements ConstraintValidator<ImageType, MultipartFile> {
   private ImageType imageType;
   public void initialize(ImageType constraint) {
      this.imageType = constraint;
   }


   public boolean isValid(MultipartFile file, ConstraintValidatorContext context) {
      if(file == null)
         return true;
      String fileName = file.getOriginalFilename();
      Tika tika = new Tika();
      String mimeType = tika.detect(fileName);
      if(FileUtil.imageMimes.contains(mimeType))
         return true;
      else
         return false;

   }

}
